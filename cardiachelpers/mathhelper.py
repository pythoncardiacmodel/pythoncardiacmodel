import numpy as np
import scipy as sp
import math
from cardiachelpers import displayhelper

def pol2cart(theta, rho):
	"""Convert polar (theta, rho) coordinates to cartesian (x, y) coordinates"""
	x = rho * np.cos(theta)
	y = rho * np.sin(theta)
	return ([x, y])

def cart2pol(x, y):
	"""Convert cartesian (x,y) coordinates to polar (theta, rho) coordinates"""
	rho = np.sqrt(np.square(x) + np.square(y))
	theta = np.arctan2(y,x)
	theta = np.where(theta < 0, theta + 2*np.pi, theta)
	return np.array([theta, rho])

def prolate2cart(m, n, p, focus):
	"""Convert passed lambda, mu, theta from prolate to cartesian based on focus."""
	x = focus * np.cosh(m) * np.cos(n)
	y = focus * np.sinh(m) * np.sin(n) * np.cos(p)
	z = focus * np.sinh(m) * np.sin(n) * np.sin(p)
	return([x, y, z])
	
def cart2prolate(x, y, z, focus):
	"""Convert passed x, y, z from cartesian to prolate based on focus."""
	# Checks if the data is a matrix to set the loop.
	matrix_flag = False
	if isinstance(x, np.ndarray):
		# Store the shape of the passed array, then flatten them
		input_shape = x.shape
		x = np.ravel(x)
		y = np.ravel(y)
		z = np.ravel(z)
		#x = np.reshape(x, x.size, 1)
		#y = np.reshape(y, y.size, 1)
		#z = np.reshape(z, z.size, 1)
		len_x = x.size
		matrix_flag = True
	else:
		len_x = 1
	m = np.zeros((len_x, 1))
	n = np.zeros((len_x, 1))
	p = np.zeros((len_x, 1))
	# Loop through the vaues and perform equations to convert to prolate.
	for jz in range(len_x):
		# Pull values from array if arrays were passed
		if matrix_flag:
			x1 = x[jz]
			x2 = y[jz]
			x3 = z[jz]
		else:
			x1 = x
			x2 = y
			x3 = z
		a1 = x1**2 + x2**2 + x3**2 - focus**2
		a2 = math.sqrt((a1**2)+4*(focus**2)*((x2**2)+(x3**2)))
		a3 = 2*(focus**2)
		a4 = max([(a1+a2)/a3, 0])
		a5 = max([(a2-a1)/a3, 0])
		a6 = math.sqrt(a4)
		a7 = min([math.sqrt(a5), 1])
		a8 = math.asin(a7) if abs(a7) <= 1 else 0
		if abs(a7) > 1: print('SLH_CMI_C2P: A8 is zero')
		if x3==0 or a6==0 or a7==0:
			a9 = 0
		else:
			a9 = x3 / (focus*a6*a7) if abs(a6*a7)>0 else 0
		a9 = math.pi/2 if a9 >= 1 else -math.pi/2 if a9 <= -1 else math.asin(a9)
		# Set the prolate values lambda (z1), mu (z2), and theta (z3)
		z1 = math.log(a6 + math.sqrt(a4+1))
		z2 = a8 if x1 >= 0 else math.pi - a8
		z3 = math.fmod(a9+2*math.pi, 2*math.pi) if x2 >= 0 else math.pi-a9
		# Store the singular values into the array
		if matrix_flag:
			m[jz] = z1
			n[jz] = z2
			p[jz] = z3
		else:
			m = z1
			n = z2
			p = z3
	# Reshape the mu, nu, and phi arrays based on the input arrays
	if matrix_flag:
		m = np.reshape(m, input_shape, order='F')
		n = np.reshape(n, input_shape, order='F')
		p = np.reshape(p, input_shape, order='F')
	return([m, n, p])
	
def findMidPt(endo_pins, time_id, septal_slice, endo_x, endo_y):
	"""Calculate the mid-septal point based on the pinpoints already placed
	
	args:
		endo_pins: array containing the rv insertion pinpoints
		time_id: which time point to use for calculation
		septal_slice: which slice to use for calculation
		endo_x: the x values defining the endocardial contour
		endo_y: the y values defining the endocardial contour
	returns:
		array mid_pt: The septal midpoint between the two other pinpoints
	"""
	if endo_pins.ndim < 2:
		endo_pins = np.expand_dims(endo_pins, 0)
	# Get mean point between the 2 pinpoints.
	mean_pt = np.mean(endo_pins, axis=0).reshape([2, 1])

	# Calculate the perpindicular line between the two points (just slope, no intercept)
	slope = (endo_pins[1,1] - endo_pins[0,1])/(endo_pins[1,0] - endo_pins[0,0])
	perp_slope = -1/slope
	
	# Get the current slice and shift it by the mean point
	cur_slice = np.array([endo_x[time_id, septal_slice, :], endo_y[time_id, septal_slice, :]])
	cur_shape = cur_slice.shape
	cur_slice = cur_slice.reshape(cur_shape[0], cur_shape[3])
	cur_slice = cur_slice - mean_pt
	
	# Convert the slice into polar values
	polar_coords = cart2pol(cur_slice[0,:], cur_slice[1,:])[:,1:]
	
	# Get the theta values for the perpindicular line (polar theta)
	perp_dot = np.dot([1, perp_slope], [1, 0])
	calcNorm = lambda arr_in: np.sqrt(np.sum(np.square(arr_in)))
	perp_norm = calcNorm([1, 1*perp_slope])
	th1 = np.arccos(perp_dot/perp_norm)
	th2 = th1 + np.pi
	
	# Calculate the rho values for the two theta values by interpolation
	r_interp = sp.interpolate.interp1d(polar_coords[0,:], polar_coords[1,:])
	r1 = r_interp(th1)
	r2 = r_interp(th2)
	r = r1 if r1<r2 else r2
	theta = th1 if r1<r2 else th2
	
	# Reconvert the interpolated rho and theta to cartesian
	mid_pt = (pol2cart(theta, r) + mean_pt.reshape([1,2])).reshape(2)
	return(mid_pt)

def findConfMidPt(pin_pts, endo_contour):
	if endo_contour.shape[0] < 1:
		return(False)
	mean_pt = np.mean(pin_pts, axis=0)
	
	slope = (pin_pts[1, 1] - pin_pts[0, 1])/(pin_pts[1, 0] - pin_pts[0, 0])
	perp_slope = -1/slope
	
	shifted_endo_contour = endo_contour - mean_pt
	
	polar_endo_contour = np.column_stack(tuple(cart2pol(shifted_endo_contour[:, 0], shifted_endo_contour[:, 1])))
	
	perp_dot = np.dot([1, perp_slope], [1, 0])
	calcNorm = lambda arr_in: np.sqrt(np.sum(np.square(arr_in)))
	perp_norm = calcNorm([1, 1*perp_slope])
	
	th1 = np.arccos(perp_dot/perp_norm)
	th2 = th1 + np.pi
	
	r_interp = sp.interpolate.interp1d(polar_endo_contour[:, 0], polar_endo_contour[:, 1], kind='linear', fill_value='extrapolate')
	r1 = r_interp(th1)
	r2 = r_interp(th2)
	r = r1 if abs(r1)<abs(r2) else r2
	theta = th1 if th1<th2 else th2
	
	mid_pt = pol2cart(theta, r) + mean_pt
	
	septal_pts = np.vstack((pin_pts, mid_pt))
	return(septal_pts)

def getContourCenter(points):
	'''Identify the visual center point of a list of points.
	'''
	if type(points) is list:
		points = np.asarray(points)
	shifted_points = np.roll(points, 1, axis=0)
	side_lengths = np.sum((points - shifted_points)**2, axis=1)**0.5
	m = (points + shifted_points)/2
	sum_lengths = np.sum(side_lengths)
	center_point = np.round(np.dot(side_lengths, m) / sum_lengths).astype(int)
	return(center_point)

def rotateProlatePhi(input_angles, slice_assignment, pinpt_arr, target_angle):
	if input_angles.shape[0] != slice_assignment.shape[0]:
		return(input_angles)
	shift_amount = np.array([target_angle - pinpt_arr[slice_assignment[point_ind]*3 + 2, -1] for point_ind in range(slice_assignment.shape[0])])
	return_angles = np.squeeze(np.array([(input_angles[angle_ind] + shift_amount[angle_ind]) % (2*math.pi) for angle_ind in range(input_angles.shape[0])]))
	#return_angles = np.array([return_angle % 2*math.pi for return_angle in return_angles])
	return(return_angles)

def adjustContourWidth(input_points, goal_points=np.array([]), radius_ratio=False):
	if radius_ratio:
		input_center = getContourCenter(input_points)
		adjusted_points = np.column_stack((input_point[:, 0], input_points[:, 1]))
	elif goal_points.size:
		input_center = getContourCenter(input_points)
		print(input_center)
		goal_center = getContourCenter(adjusted_points)
		adjusted_points = input_points
	else:
		adjusted_points = input_points
	return(adjusted_points)

def getBinValues(values_in, bin_edges):
	'''Return a histogram of values_in based on increments defined in bin_edges.
	
	Returns:
	bin_counts: List of number of items in each bin.
	bin_index: The respective bin for each value in bin_counts when zipped.
	'''
	bin_index = np.full((len(values_in)), np.nan) if isinstance(values_in, list) else np.full((values_in.size), np.nan)
	bin_counts = [None]*(len(bin_edges) - 1)
	value_list = values_in if isinstance(values_in, list) else values_in.flatten()
	for cur_bin in range(len(bin_edges) - 1):
		bin_bot = bin_edges[cur_bin]
		bin_top = bin_edges[cur_bin + 1]
		items_in_bin = np.where(np.logical_and(np.greater_equal(value_list, bin_bot), np.less(value_list, bin_top)))[0]
		#print(items_in_bin)
		bin_counts[cur_bin] = len(items_in_bin)
		bin_index[items_in_bin] = cur_bin
	#if bin_index.size:
		#bin_index[np.where(np.greater(value_list, max(bin_edges)))[0]] = max(bin_index)
		#bin_index[np.where(np.less(value_list, min(bin_edges)))[0]] = min(bin_index)
	return(bin_counts, list(bin_index.astype(int)))
		
def getAngleRange(angles):
	"""Get the leftmost and rightmost values from a passed series of angles.
	
	The angles should be circular, with a total possible range of 2*pi. The purpose of this function
	is to allow finding the circular extent of angles that cross the origin point.
	
	args:
		angles (float arr-like): The angles for which ranges are being determined.
	returns:
		angle_min (float): The "minimum" angle (clockwise)
		angle_max (float): The "maximum" angle (clockwise)
		direction (bool): True if the scar does not pass through the origin (angle flip point)
	"""
	# Get maximum and minimum angle values and subtract
	angle_max = np.max(angles)
	angle_min = np.min(angles)
	init_range = angle_max - angle_min
	# If the range is less than 2*pi, you don't cross the origin
	if init_range < 6:
		direction = True
		return([angle_min, angle_max, direction])
	else:
		# Sort angles from minimum -> maximum, append initial value to the end, increased by 2*pi
		angles_sorted = np.sort(angles)
		angles_sorted = np.append(angles_sorted, angles_sorted[0] + 2*math.pi)
		# Calculate the moving differential
		angles_diff = [angles_sorted[i+1] - angles_sorted[i] for i in range(len(angles))]
		# The "true minimum" (most counterclockwise angle) is immediately after the largest gap
		angle_min = angles_sorted[np.argmax(angles_diff, axis=0) + 1]
		angle_max = angles_sorted[np.argmax(angles_diff, axis=0)]
		# Determine directionality to ensure that scar values are between the minimum and maximum appropriately
		direction = np.all(np.bitwise_and(angles >= angle_min, angles <= angle_max))
	return([angle_min, angle_max, direction])
	
def twoxcrmNorm2gd(myocardium_signal, blood_signal, baseline_index, data_struct, pd_struct):
	time_val = data_struct['TimeVector'][-1] - data_struct['TimeVector'][0]
	#x0 = 100
	#lb = 1
	#ub = 2000
	tr = data_struct['RepetitionTime']
	sat_time = data_struct['InversionTime'][0] + 5
	flip_angle = data_struct['FlipAngle']
	pd_flip_angle = pd_struct['FlipAngle']
	aif = 1
	f = 0
	params = [tr, sat_time, aif, flip_angle, pd_flip_angle, f]
	t1 = [sp.optimize.curve_fit(__Axel, params, [blood_sig], p0=100, bounds=(1,2000))[0][0]/1000 for blood_sig in blood_signal]
	r1 = [1/t1_ind for t1_ind in t1]
	#r1_baseline = np.mean(r1[baseline_index])
	r1_baseline = 1/1.5
	aifgdconc = [(r1_ind - r1_baseline)/0.1393 for r1_ind in r1]
	t1_tf = [sp.optimize.curve_fit(__Axel, params, [myo_sig], p0=500, bounds=(1,2000))[0][0]/1000 for myo_sig in myocardium_signal]
	r1_tf = [1/t1_tf_ind for t1_tf_ind in t1_tf]
	#r1_tf_baseline = np.mean(r1_tf[baseline_index])
	r1_tf_baseline = 1/1.45
	tfgdconc = [(r1_tf_ind - r1_tf_baseline)/0.1393 for r1_tf_ind in r1_tf]
	return(t1, aifgdconc, t1_tf, tfgdconc)

def _createFitNitroxide(fit_time, fit_tf, fit_type="exp"):
	if fit_type == "lin":
		fit_tf_log = [np.log(fit_tf_ind) if fit_tf_ind > 0 else np.log(-fit_tf_ind) for fit_tf_ind in fit_tf] # Identify better log(<=0) solution.
		#fit_tf_log = [np.log(fit_tf_ind) for fit_tf_ind in fit_tf]
		#fit_tf_log = []
		#fit_time_log = []
		#for fit_tf_ind in range(len(fit_tf)):
			#if fit_tf[fit_tf_ind] > 0:
				#fit_tf_log.append(np.log(fit_tf[fit_tf_ind]))
				#fit_time_log.append(fit_time[fit_tf_ind])
		model_results = sp.optimize.curve_fit(lambda x, p1, p2: p1*x+p2, fit_time, fit_tf_log)[0]
	elif fit_type == "exp":
		model_results = sp.optimize.curve_fit(lambda x, a, b, c: a*np.exp(-x/b)+c, fit_time, fit_tf, p0=[10, 10, 1])[0]
	else:
		print("Incompatible fit type. Defaulting to exponential.")
		model_results = sp.optimize.curve_fit(lambda x, a, b, c: a*np.exp(-x/b)+c, fit_time, fit_tf, p0=[10, 10, 1])[0]
	return(model_results)

def getDecayRates(input_conc, time_vals, num_points):
	max_ind = input_conc.index(max(input_conc[5:]))
	fit_tf = input_conc[max_ind:]
	fit_time = (time_vals[max_ind:-1] - time_vals[max_ind])/60
	fit_result = _createFitNitroxide(fit_time, fit_tf, fit_type="exp")
	decayrate = fit_result[1]
	fit_result_log = _createFitNitroxide(fit_time, fit_tf, fit_type="lin")
	slope = -fit_result_log[0]
	return(decayrate, slope)
	
def __Axel(params, x):
	"""Axel function to feed into norm2gd.
	
	Inputs:
	x = value to assess
	tr = Repetition Time
	td = Saturation time for AIF
	n = Ordering parameter.
	alpha_t1 = flip angle (degrees)
	alpha_pd = flip angle (degrees) in proton density
	"""
	tr, td, n, alpha_t1, alpha_pd, f = params
	try:
		e1 = math.exp(-tr/x)
		e2 = math.exp(-td/x)
		cos_at1 = math.cos(math.radians(alpha_t1))
		cos_apd = math.cos(math.radians(alpha_pd))
		t1_term = (1-(e1*cos_at1)**(n-1))/(1-(e1-cos_at1))
		pd_term = (1-(e1*cos_apd)**(n-1))/(1-(e1-cos_apd))
		norm_factor = math.sin(math.radians(alpha_t1))/math.sin(math.radians(alpha_pd))
		m = ((1-(1-f)*e2)*(e1*cos_at1)**(n-1) + (1-e1)*t1_term)/((e1*cos_apd)**(n-1) + (1-e1)*pd_term)
	except ZeroDivisionError:
		m = 1
		print('Division by 0. Returning default value.')
	return(m)
	
def modToftsKetyModel(time_val_in, aif, tf, arrival_ind=4):
	# Slice inputs for timing and fitting
	time_val_in = time_val_in[:len(aif)]
	time_vals = [(time_val-time_val_in[5])/60 for time_val in time_val_in[5:]]
	aif_x = aif[5:]
	tf_x = tf[5:]
	num_points = len(time_vals)
	time_trim = time_vals[:num_points]
	aif_trim = aif_x[:num_points]
	# Calculate precontrast AIF
	avg_precontrast_aif = np.mean(aif_trim[:arrival_ind])
	aif_trim -= avg_precontrast_aif
	aif_trim[:arrival_ind+1] = 0
	# Calculate input lists
	switch_ind = arrival_ind + 3
	arrival_t = time_vals[arrival_ind]
	aif_trim_preswitch = aif_trim[:switch_ind]
	time_trim_preswitch = time_trim[:switch_ind]
	aif_trim_postswitch = aif_trim[switch_ind:]
	time_trim_postswitch = time_trim[switch_ind:]
	fit_pos = [int(time_trim_preswitch_val > arrival_t) for time_trim_preswitch_val in time_trim_preswitch]
	# Calculate model for aif over time based on input data.
	aif_model_fit = __aifModelFit(time_trim_preswitch, time_trim_postswitch, aif_trim_preswitch, aif_trim_postswitch, fit_pos, arrival_t)
	# Perform tf calculations
	tf_trim = tf_x[:len(aif_model_fit)]
	avg_precontrast_tf = np.mean(tf_trim[2:arrival_ind])
	tf_trim -= avg_precontrast_tf
	tf_trim[:arrival_ind+1] = 0
	tf_conv_fit, tf_conv_params = _convolutionModelFit(time_trim, aif_model_fit, tf_trim, arrival_ind)
	# Check fit reasonableness
	r2_x = 1 - np.sum([(tf_conv_fit[ind] - tf_trim[ind])**2 for ind in range(len(tf_trim))])/np.sum([tf_conv_fit_ind**2 for tf_conv_fit_ind in tf_conv_fit])
	model_results = [tf_conv_params[0], tf_conv_params[1], tf_conv_params[2], tf_conv_params[0]/tf_conv_params[2], r2_x]
	trimmed_model_results = np.column_stack((time_trim, aif_trim, aif_model_fit, tf_trim, tf_conv_fit))
	return(model_results, trimmed_model_results)

def __aifModelFit(time_preswitch, time_postswitch, aif_preswitch, aif_postswitch, fit_pos, arrival_t):
	# Set initial parameter estimations.
	alpha = 0.1
	beta = 0.8
	amp = 90
	a = 0.1
	b = 0
	e = 0
	# Define models to fit
	def gamma_aif_model(x, a_t, alph, bet, amp_in):
		comp_results = [fit_pos[ind]*amp_in*(complex((x[ind]-a_t))**alph)*math.exp(-(x[ind]-a_t)/bet) for ind in range(len(fit_pos))]
		return([comp_result.real for comp_result in comp_results])
	def exp_aif_model(x, a, b, e):
		comp_results = [a*math.exp(b*(x[ind] - x[0]))+e for ind in range(len(x))]
		return([comp_result.real for comp_result in comp_results])
	# Combine parameters and fit gamma model
	gamma_params = [arrival_t, alpha, beta, amp]
	gamma_results = sp.optimize.curve_fit(gamma_aif_model, time_preswitch, aif_preswitch, p0=gamma_params, bounds=(-100, 100))[0]
	# Combine parameters and fit exponential model
	exp_params = [a, b, e]
	exp_results = sp.optimize.curve_fit(exp_aif_model, time_postswitch, aif_postswitch, p0=exp_params)[0]
	# Calculate with fitted model parameters
	gamma_fit = gamma_aif_model(time_preswitch, gamma_results[0], gamma_results[1], gamma_results[2], gamma_results[3])
	exp_fit = exp_aif_model(time_postswitch, exp_results[0], exp_results[1], exp_results[2])
	# Combine fitted results and return
	aif_model_fit = gamma_fit + exp_fit
	return(aif_model_fit)
	
def _convolutionModelFit(time_trim, aif_model, tf_trim, arrival_ind):
	# Set up initial parameters
	k_norm = 1
	k_red = 0
	ve = 0.05
	clearance = 0
	params = [k_norm, k_red, ve, clearance]
	w1 = np.ones(len(time_trim))
	w1[arrival_ind+1] = 1
	w1[arrival_ind+2] = 10
	w1[arrival_ind+3] = 1
	w1[arrival_ind+6:] = 1
	pred_vals = np.column_stack((time_trim, aif_model))
	conv_results = sp.optimize.curve_fit(__convolutionModel, pred_vals, tf_trim, p0=params, sigma=w1, bounds=(0, 5))[0]
	tf_conv_fit = __convolutionModel(pred_vals, conv_results[0], conv_results[1], conv_results[2], conv_results[3])
	return(tf_conv_fit, conv_results)
	#convolution_model_results = __convolutionModel(time_trim, aif_model, k_norm, k_red, ve, clearance)
	#print(convolution_model_results)
	
def __convolutionModel(pred_in, k_norm, k_red, ve, clearance):
	fp = 0.05
	time_in = pred_in[:, 0]
	aif_in = pred_in[:, 1]
	t_step = max(time_in)/(len(time_in)-1)
	ev = k_norm*np.convolve([math.exp(-(k_norm/ve)*time_i) for time_i in time_in], aif_in)
	ev_trim = ev[:len(aif_in)]
	red = k_red*np.convolve([math.exp(-(k_norm/ve)*time_i) for time_i in time_in], 1)
	red_trim = red[:len(aif_in)]
	b = [0 if aif_in[ind] == 0 else clearance for ind in range(len(aif_in))]
	tf_out = [fp*aif_in[ind] + (1-fp)*t_step*(ev[ind]-red[ind]) + b[ind] for ind in range(len(aif_in))]
	return(tf_out)