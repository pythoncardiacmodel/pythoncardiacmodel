# -*- coding: utf-8 -*-
"""
Contains information necessary for import and alignment of confocal microscopy images.

Created on Mon Jan 27 12:22:15 2018

@author: cdw2be

Planar Image Brightness Correction based on: https://imagej.nih.gov/ij/plugins/plane-brightness/2010_Michalek_Biosignal.pdf
"""

# Imports
import math
import tkinter as tk
from tkinter import filedialog
import scipy as sp
import numpy as np
import matplotlib.pyplot as mplt
import glob
from PIL import Image, ImageDraw
from PIL.TiffTags import TAGS
import tifffile
import skimage.io as skio
import skimage.transform as sktransform
import os
import re
import pickle
from natsort import natsorted, ns
from cardiachelpers import importhelper
from cardiachelpers import confocalhelper
from cardiachelpers import mathhelper
from cardiachelpers import displayhelper

class ConfocalModel():
	"""Model class to hold confocal microscopy images and format them to generate a mesh to align with MRI data.
	"""

	def __init__(self, top_data_dir, slice_width = 0.25):
		"""Initialize the model made to import confocal microscopy data.
		
		args:
			top_data_dir (string): The path to the directory containing the metadata file and the subfolders
			slice_width (float): The height of the slices in mm
		"""
		self.model_dir = top_data_dir
		self.slice_folders = [f.path for f in os.scandir(top_data_dir) if f.is_dir()]
		comd_files = [f.path for f in os.scandir(top_data_dir) if ".comd" in f.path]
		# Import data if a .comd file is found in the model directory. Otherwise, use default values.
		try:
			self.comd_file = comd_files[0]
			self.importCOMDPickle()
			self.data_imported = True
		except:
			self.data_imported = False
			self.comd_file = False
			print('No metadata file, using default values.')
		try:
			if not self.slice_widths: self.slice_widths = [slice_width]*len(self.slice_folders)
		except AttributeError:
			self.slice_widths = [slice_width]*len(self.slice_folders)
		# Either truncate or expand the slice_widths variable to match the number of slices (subfolders) found.
		if len(self.slice_folders) > len(self.slice_widths):
			while len(self.slice_folders) > len(self.slice_widths):
				self.slice_widths.append(slice_width)
		elif len(self.slice_folders) < len(self.slice_widths):
			self.slice_widths = self.slice_widths[0:len(self.slice_positions)]
		# Either truncate or expand the slice_positions variable to match the number of slices (subfolders) found
		try:
			if not self.slice_positions: self.slice_positions = [0 for slice_idx in range(len(self.slice_folders))]
		except AttributeError:
			print('Created new slice positions data.')
			self.slice_positions = [0 for slice_idx in range(len(self.slice_folders))]
		if len(self.slice_folders) > len(self.slice_positions):
			while len(self.slice_folders) > len(self.slice_positions):
				self.slice_positions.append(0)
		elif len(self.slice_folders) < len(self.slice_positions):
			self.slice_positions = self.slice_positions[0:len(self.slice_folders)]
		# Instantiate a ConfocalSlice object for each subfolder, using the slice_position and slice_width settings.
		self.confocal_slices = [ConfocalSlice(self.slice_folders[slice_idx], self.slice_widths[slice_idx]) for slice_idx in range(len(self.slice_folders))]
		self.slices_imported = [confocal_slice.data_imported for confocal_slice in self.confocal_slices]
		#self.stitching_images = False
		#self.slice_width = slice_width
		#self.slice_positions = None
		#self.data_files = False
		#self.fluo_images = {}
		#self.model_name = ""
		#self.slices = False
		#if top_stitching_dir:
		#	self.stitching_images = self.setTopStitchingDir(top_stitching_dir)
	
	def importCOMData(self):
		'''Imports from a human-readable COMD file. Not recommended.
		'''
		model_dict = {}
		with open(self.comd_file, 'r') as comd_file:
			for file_line in comd_file.readlines():
				if ':' in file_line:
					split_line = file_line.split(':')
					data_key = split_line[0].strip()
					data_input = split_line[1].strip()
					if data_key == 'slice_positions':
						slice_pos_strs = data_input.split(',')
						model_dict[data_key] = [float(slice_pos_str) for slice_pos_str in slice_pos_strs]
					if data_key == 'slice_widths':
						slice_width_strs = data_input.split(',')
						model_dict[data_key] = [float(slice_width_str) for slice_width_str in slice_width_strs]
		self.slice_positions = model_dict['slice_positions']
		self.slice_widths = model_dict['slice_widths'] if 'slice_widths' in model_dict.keys() else False
		self.data_imported = True
		return(True)
	
	def importCOMDPickle(self):
		'''Reads in data from a .comd file that was exported using the pickle built-in library.
		'''
		with open(self.comd_file, 'rb') as import_file:
			model_dict = pickle.load(import_file)
		self.slice_positions = model_dict['slice_positions']
		self.slice_widths = model_dict['slice_widths']
		self.data_imported = True
		return(True)
	
	def exportCOMData(self, comd_file = False):
		'''Creates a .comd file containing model metadata using Pickle.
		Output file is not human-readable.
		'''
		if not comd_file:
			if self.comd_file:
				comd_file = self.comd_file
			else:
				comd_file = os.path.join(self.model_dir, '0.comd')
		export_dict = {
			'slice_positions' : self.slice_positions,
			'slice_widths' : self.slice_widths
			}
		with open(comd_file, 'wb') as output_file:
			pickle.dump(export_dict, output_file, protocol=pickle.HIGHEST_PROTOCOL)
		return(True)
	
	def exportSlices(self, slice_num = -1):
		'''Exports data from ConfocalSlice objects. If a specific slice is selected, only that slice is exported.
		'''
		if slice_num < 0:
			for confocal_slice in self.confocal_slices:
				confocal_slice.exportCSDataPickle()
		else:
			self.confocal_slices[slice_num].exportCSDataPickle()
	
	def setTopStitchingDir(self, top_dir):
		# Get all TIFF files in the directory
		dirs = natsorted([(top_dir + '/' + sub_dir) for sub_dir in os.listdir(top_dir) if os.path.isdir(top_dir + '/' + sub_dir)], alg=ns.IC)
		self.top_dir = top_dir
		self.model_name = os.path.basename(top_dir)
		if not hasattr(self, 'slices') or not self.slices:
			self.slices = [ConfocalSlice() for im_dir in dirs]
			self.slice_positions = [i*self.slice_width for i in range(len(dirs))]
		slice_success = [False]*len(self.slices)
		for i in range(len(self.slices)):
			slice_success[i] = self.slices[i].setStitchingDirectory(dirs[i])
		self.slices = [self.slices[i] for i in range(len(self.slices)) if slice_success[i]]
		self.slice_names = [confocal_slice.slice_name for confocal_slice in self.slices]
		self.model_image = None
		self.slice_image_list = None
		self.endo_contours = None
		self.epi_contours = None
		return(True)
		
	def setStitchedDir(self, stitched_dir):
		self.stitched_files = natsorted(glob.glob(os.path.join(stitched_dir, '*.tif')).copy(), alg=ns.IC)
		if not hasattr(self, 'slices') or not self.slices:
			self.slices = [ConfocalSlice() for stitched_file in self.stitched_files]
			self.slice_positions = [i*self.slice_width for i in range(len(self.stitched_files))]
		for i in range(len(self.stitched_files)):
			self.slices[i].setStitchedImage(self.stitched_files[i])
			
	def setImportDirectory(self, data_dir):
		if not os.path.exists(data_dir):
			print('Directory not found.')
			return(False)
		self.data_files = natsorted(glob.glob(os.path.join(data_dir, '*.cod')).copy(), alg=ns.IC)
		if not hasattr(self, 'slices') or not self.slices:
			self.slices = [ConfocalSlice() for data_file in self.data_files]
			self.slice_positions = [i*self.slice_width for i in range(len(self.data_files))]
		
	def importModelData(self):
		if self.data_files:
			for i in range(len(self.data_files)):
				self.slices[i].importModelData(self.data_files[i])
			return(True)
		else:
			print('No data files available!')
			return(False)
			
	def setExportDirectory(self, data_dir):
		if not hasattr(self, 'slices') or not self.slices:
			return(False)
		for conf_slice in self.slices:
			file_name = conf_slice.slice_name + '.cod'
			full_file_path = os.path.join(data_dir, file_name)
			conf_slice.exportModelData(full_file_path)
	
	def generateStitchedImages(self, slices, sub_slices, channel_list, overlap=0.1, compress_ratio=1, force_file=False):
		"""Adjust image intensity based on edge intensity of adacent images.
		"""
		# Set subslices list to mid-slice if none is indicated.
		for slice_ind, slice_num in enumerate(slices):
			# Determine if the sub_slices list is per-slice, or a general list.
			cur_sub_slices = sub_slices[slice_ind] if isinstance(sub_slices[slice_ind], list) else sub_slices
			for sub_slice in cur_sub_slices:
				for channel_val in channel_list[slice_ind]:
					#file_name = self.top_dir + '/' + self.slice_names[slice_num] + 'Channel' + self.getChannelList(slice_num)[channel_val] + 'Frame' + str(sub_slice) + 'Stitched.tif'
					file_name = self.top_dir + '/' + slice_num + 'Channel' + self.getChannelList(slice_ind)[channel_val] + 'Frame' + str(sub_slice) + 'Stitched.tif'
					if (not os.path.isfile(file_name)) or force_file:
						#self.slices[slice_num].createStitchedImage(channel_num=channel_val, overlap=overlap, compress_ratio=compress_ratio, frame=sub_slice, stitched_file=file_name, force_file=force_file)
						self.slices[slice_ind].createStitchedImage(channel_num=channel_val, overlap=overlap, compress_ratio=compress_ratio, frame=sub_slice, stitched_file=file_name, force_file=force_file)
		
	def generateImageGridFiles(self, slices):
		for slice_ind, slice_num in enumerate(slices):
			#self.slices[slice_num].createImageGridFile()
			self.slices[slice_ind].createImageGridFile()
		
	def getSubsliceList(self, top_slice):
		"""Gather information about which sub-slices are present within each image.
		"""
		return(list(range(self.slices[top_slice].num_slices)))
		
	def getChannelList(self, top_slice):
		"""Gather information about which channels are present within each image.
		"""
		return(list(self.slices[top_slice].channels))
	
	def importModelImage(self, model_image_file):
		self.model_image = model_image_file
		self.slice_image_list = confocalhelper.openModelImage(model_image_file)
	
	def generateContours(self):
		self.endo_contours = [None]*len(self.slice_image_list)
		self.epi_contours = [None]*len(self.slice_image_list)
		for im_num, im_slice in enumerate(self.slice_image_list):
			edge_image = confocalhelper.contourMaskImage(im_slice)
			endo_path, epi_path, labelled_arr = confocalhelper.splitImageObjects(edge_image)
			self.endo_contours[im_num] = confocalhelper.orderPathTrace(endo_path)
			self.epi_contours[im_num] = confocalhelper.orderPathTrace(epi_path)
	
	def prepDataForMesh(self):
		# Calculate slice heights for each slice and 
		slice_heights = []
		endo_contours = []
		epi_contours = []
		pixel_mm_ratios = []
		midpoint_endo_contour = []
		midpoint_pinpoints = []
		for slice_ind, slice_pos in enumerate(self.slice_positions):
			slice_heights.extend([subslice_height+slice_pos for subslice_height in self.slices[slice_ind].slice_positions])
			endo_contours.extend([endo_contours_subslice for endo_contours_subslice in self.slices[slice_ind].contours['endo_contours']])
			epi_contours.extend([epi_contours_subslice for epi_contours_subslice in self.slices[slice_ind].contours['epi_contours']])
			midpoint_epi_contour = np.array(self.slices[slice_ind].contours['epi_contours'][0])
			center_epi_point = np.mean(midpoint_epi_contour, axis=0)
			cur_midpoint_contour = np.subtract(np.array(self.slices[slice_ind].contours['endo_contours'][0]), center_epi_point)
			midpoint_endo_contour.append(cur_midpoint_contour)
			cur_midpoint_pinpoints = np.subtract(np.array(self.slices[slice_ind].contours['pinpoints']), center_epi_point)
			midpoint_pinpoints.append(cur_midpoint_pinpoints)
			pixel_mm_ratios.extend([self.slices[slice_ind].contours['image_resolution']]*self.slices[slice_ind].num_slices)
		# Shift slices to align the contours in X and Y (in-slice should be similar, across slices will vary).
		shifted_epi_contours = [[] for i in range(len(epi_contours))]
		shifted_endo_contours = [[] for i in range(len(endo_contours))]
		for contour_ind in range(len(epi_contours)):
			epi_contour_x = [epi_point[0] for epi_point in epi_contours[contour_ind]]
			epi_contour_y = [epi_point[1] for epi_point in epi_contours[contour_ind]]
			center_epi_point = [np.mean(epi_contour_x), np.mean(epi_contour_y)]
			shifted_epi_x = [epi_contour_x_val - center_epi_point[0] for epi_contour_x_val in epi_contour_x]
			shifted_epi_y = [epi_contour_y_val - center_epi_point[1] for epi_contour_y_val in epi_contour_y]
			shifted_epi_contours[contour_ind] = [(shifted_epi_x[i], shifted_epi_y[i]) for i in range(len(shifted_epi_x))]
			endo_contour_x = [endo_point[0] for endo_point in endo_contours[contour_ind]]
			endo_contour_y = [endo_point[1] for endo_point in endo_contours[contour_ind]]
			shifted_endo_x = [endo_contour_x_val - center_epi_point[0] for endo_contour_x_val in endo_contour_x]
			shifted_endo_y = [endo_contour_y_val - center_epi_point[1] for endo_contour_y_val in endo_contour_y]
			shifted_endo_contours[contour_ind] = [(shifted_endo_x[i], shifted_endo_y[i]) for i in range(len(shifted_endo_x))]
		# Calculate the mid-septal point from a selected slice.
		self.septal_points = []
		for slice_ind in range(len(self.slice_positions)):
			self.septal_points.extend([mathhelper.findConfMidPt(midpoint_pinpoints[slice_ind], midpoint_endo_contour[slice_ind])]*self.slices[slice_ind].num_slices)
		# Convert contours into real (mm) values and append slice height information.
		self.full_endo_contour = np.empty([0, 3])
		self.full_epi_contour = np.empty([0, 3])
		self.septal_pinpoints = []
		temp_disp_axes = False
		for subslice_ind in range(len(slice_heights)):
			endo_contour_arr = np.column_stack(([slice_heights[subslice_ind]]*len(endo_contours[subslice_ind]), np.array(shifted_endo_contours[subslice_ind])))
			endo_contour_arr[:, 1:] *= pixel_mm_ratios[subslice_ind]
			if endo_contour_arr.shape[1] == 3:
				self.full_endo_contour = np.vstack((self.full_endo_contour, endo_contour_arr))
			epi_contour_arr = np.column_stack(([slice_heights[subslice_ind]]*len(epi_contours[subslice_ind]), np.array(shifted_epi_contours[subslice_ind])))
			epi_contour_arr[:, 1:] *= pixel_mm_ratios[subslice_ind]
			if epi_contour_arr.shape[1] == 3:
				self.full_epi_contour = np.vstack((self.full_epi_contour, epi_contour_arr))
			pinpoint_arr = np.column_stack(([slice_heights[subslice_ind]]*len(self.septal_points[subslice_ind]), self.septal_points[subslice_ind]))
			pinpoint_arr[:, 1:] *= pixel_mm_ratios[subslice_ind]
			self.septal_pinpoints.append(pinpoint_arr)
			
		# Generate Apex-Base points for use in vertical alignment.
		self.basal_pt = np.array([np.min(slice_heights), 0, 0])
		self.apex_pt = np.array([np.max(slice_heights), 0, 0])
		prior_to_rot_points = [np.copy(pinpoint_val) for pinpoint_val in self.septal_pinpoints]
		for slice_ind in range(0, len(slice_heights)):
			self.full_epi_contour, self.full_endo_contour, self.septal_pinpoints[slice_ind] = confocalhelper.rotateContours(self.full_epi_contour, self.full_endo_contour, self.septal_pinpoints[slice_ind], self.septal_pinpoints[0], slice_heights[slice_ind])
		
		return(self.full_endo_contour, self.full_epi_contour, self.apex_pt, self.basal_pt, self.septal_pinpoints, prior_to_rot_points)
	
	def importSliceFluorescences(self, image_dir, channel_name):
		self.fluo_images[channel_name] = natsorted(glob.glob(os.path.join(image_dir, '*.tif')).copy(), alg=ns.IC)
		full_slice_arrs = [None for i in range(len(self.slices))]
		for slice_ind, conf_slice in enumerate(self.slices):
			full_slice_arrs[slice_ind] = conf_slice.importFluorescenceData(channel_name, self.fluo_images[channel_name][slice_ind])
		return(full_slice_arrs)
	
class ConfocalSlice():
	"""Class to hold information for a single biological slice of tissue.
	
	Biological slices are stored in individual directories.
	"""
	def __init__(self, data_folder, slice_width = 0.25, slice_separation=0.05, image_resolution=0.0008791):
		self.slice_width = slice_width
		self.data_folder = data_folder
		# Assume that each tif file is a channel with multiple pages, one per subslice
		self.tif_files = [f.path for f in os.scandir(data_folder) if ".tif" in f.path]
		try:
			with tifffile.TiffFile(self.tif_files[0]) as img_file:
				resolution_tuple = img_file.pages[0].tags['XResolution'].value
				self.image_resolution = resolution_tuple[0]/(resolution_tuple[1]*1000)
		except:
			self.image_resolution = image_resolution
		csd_files = [f.path for f in os.scandir(data_folder) if ".csd" in f.path]
		# Import images using MultiImage for multi-page tiff files.
		#	Number of images = len(object), image_shape = shape of each element in list.
		#	Different list item for each page of the image.
		self.raw_images = [[image_frame for image_frame in skio.MultiImage(tif_file)] for tif_file in self.tif_files]
		self.num_slices = len(self.raw_images[0])
		self.compression_ratio = -1
		self.final_resolution = (-1, -1)
		if len(csd_files):
			self.csd_file = csd_files[0]
			self.data_imported = self.importCSDataPickle()
			self.slice_positions = [np.round(self.slice_separation*slice_idx, 3) for slice_idx in range(self.num_slices)]
			if self.slice_positions[-1] > self.slice_width:
				print('Slice positions and number exceed slice width, please note.')
		else:
			self.channels = list(range(len(self.tif_files)))
			self.data_imported = False
			self.slice_separation = slice_separation # Defaults to 0.05 mm slice separation within z-stack
			self.slice_positions = [np.round(self.slice_separation*slice_idx, 3) for slice_idx in range(self.num_slices)]
			if self.slice_positions[-1] > self.slice_width:
				print('Slice positions and number exceed slice width, please note.')
			if np.any([len(raw_image) != self.num_slices for raw_image in self.raw_images]):
				print('Image files have different number of slices, cannot continue.')
				return(False)
			self.endo_contour = [[] for contour_idx in range(self.num_slices)]
			self.septal_pts = np.zeros((2,3))
			#np.empty((2, 0, len(self.raw_images[0])))
			self.epi_contour = [[] for contour_idx in range(self.num_slices)]
			tif_file_name = self.tif_files[0].split('.')[0]
			csd_filename = tif_file_name + '.csd'
			self.csd_file = os.path.join(self.data_folder, csd_filename)
			#np.empty((2, 0, len(self.raw_images[0])))
		''' Values below this comment are deprecated.
		#self.stitching_dir = stitching_dir
		#self.stitching_images = None
		#self.num_slices = 0
		#self.contours = {}
		#self.stitched_file = None
		#self.slice_name = ''
		#self.stitched_image = None
		#self.compressed_images = None
		#self.image_grid_file = None
		#self.model_images = None
		#self.ome_files = False
		#self.keys_set = False
		#if stitching_dir:
		#	self.stitching_images = self.setStitchingDirectory(stitching_dir)'''
	
	def compressImages(self, compression_ratio = False, final_resolution = False):
		'''Resizes the raw images based on either a compression factor (single floating point) or a final image size (2-D array of integers).
		'''
		if compression_ratio:
			if self.compression_ratio == compression_ratio:
				return(True)
			self.compression_ratio = compression_ratio
			self.compressed_images = [[255 * sktransform.rescale(image_pane, compression_ratio, anti_aliasing = False) for image_pane in raw_image] for raw_image in self.raw_images]
			self.compressed_epi_contour = [[(epi_contour_frame_pts[0]*compression_ratio, epi_contour_frame_pts[1]*compression_ratio) for epi_contour_frame_pts in epi_contour_frame] for epi_contour_frame in self.epi_contour]
			self.compressed_endo_contour = [[(endo_contour_frame_pts[0]*compression_ratio, endo_contour_frame_pts[1]*compression_ratio) for endo_contour_frame_pts in endo_contour_frame] for endo_contour_frame in self.endo_contour]
			self.compressed_septal_pts = self.septal_pts*compression_ratio
			self.compressed_image_resolution = self.image_resolution / compression_ratio
		elif final_resolution:
			if self.final_resolution == final_resolution:
				return(True)
			self.compressed_images = [[255 * sktransform.resize(image_pane, final_resolution, anti_aliasing = True) for image_pane in raw_image] for raw_image in self.raw_images]
			self.final_resolution = final_resolution
			self.image_ratios = [tuple([final_resolution[dim_i]/raw_image[0].shape[dim_i] for dim_i in range(len(final_resolution))]) for raw_image in self.raw_images]
		else:
			print('Need either compression ratio or a final resolution to compress to.')
			return(False)
		return(True)
	
	def addContourPoint(self, contour, slice, point):
		'''Add a point to the end of a selected contour at a given slice.
		'''
		if contour == 'endo':
			self.endo_contour[slice].append(point)
			return(True)
		elif contour == 'epi':
			self.epi_contour[slice].append(point)
			return(True)
		else:
			print('No contour selected, point ignored.')
	
	def closeContour(self, contour, slice):
		if contour == 'endo':
			self.endo_contour[slice].append(self.endo_contour[slice][0])
			return(True)
		elif contour == 'epi':
			self.epi_contour[slice].append(self.epi_contour[slice][0])
			return(True)
		else:
			print('No contour selected.')
			return(False)
	
	def deleteContourPoint(self, contour, slice, point_idx=-1):
		if contour == 'endo':
			del self.endo_contour[slice][point_idx]
			return(True)
		if contour == 'epi':
			del self.epi_contour[slice][point_idx]
			return(True)
		else:
			print('No contour selected, point ignored.')
	
	def addSeptalPoint(self, point, replace=0):
		if self.septal_pts[0, 0] == 0:
			self.septal_pts[0, 0] = point[0]
			self.septal_pts[1, 0] = point[1]
		elif self.septal_pts[0, 1] == 0:
			self.septal_pts[0, 1] = point[0]
			self.septal_pts[1, 1] = point[1]
		else:
			if replace > 1:
				return(False)
			self.septal_pts[replace, 0] = point[0]
			self.septal_pts[replace, 1] = point[1]
	
	def getPixelData(self, frame, epi_contour = np.empty(0), endo_contour = np.empty(0), compressed=False):
		if not compressed:
			if not epi_contour.size:
				epi_contour = self.epi_contour[frame]
			if not endo_contour.size:
				endo_contour = self.endo_contour[frame]
		pixel_arr = confocalhelper.getPixelsInContour(epi_contour, endo_contour)
		query_images = [raw_image[frame] for raw_image in self.raw_images] if not compressed else [raw_image[frame] for raw_image in self.compressed_images]
		pixel_vals = [[query_image[pixel_arr[pixel_ind, 1], pixel_arr[pixel_ind, 0]] for pixel_ind in range(pixel_arr.shape[0])] for query_image in query_images]
		return([pixel_arr, pixel_vals])
	
	def setStitchingDirectory(self, stitching_dir):
		# Store the directory of images used for this model and parse out the folder name.
		self.stitching_dir = stitching_dir
		self.slice_name = os.path.split(stitching_dir)[1]
		
		# Record filenames for all tiff image files in the directory.
		self.tif_files = natsorted(glob.glob(os.path.join(stitching_dir, '*.tif')).copy(), alg=ns.IC)
		if len(self.tif_files) == 0:
			self.tif_files = natsorted(glob.glob(os.path.join(stitching_dir, '*.tiff')).copy(), alg=ns.IC)
			if len(self.tif_files) > 0:
				self.ome_files = True
			else:
				return(False)
		self.raw_images = [None]*len(self.tif_files)
		
		# Iterate through and create image objects for each tiff file
		for file_num, image_file in enumerate(self.tif_files):
			#self.raw_images[file_num] = skio.MultiImage(image_file)
			self.raw_images[file_num] = Image.open(image_file)
		
		# Get number of z-slices and channels based on either raw image information or OME XML Data
		if self.ome_files:
			self.channels, self.num_slices = confocalhelper.getOMEData(self.tif_files[0])
		else:
			self.num_slices = self.raw_images[0].n_frames
			#self.num_slices = len(self.raw_images[0])
			self.channels = self.raw_images[0].getbands()
			#self.channels = self.raw_images[0].shape[2] if len(self.raw_images[0].shape) > 2 else 1
			
		# Define how many sub-slices (confocal slices) are contained within the overall slice
		self.slice_positions = [slice_num*(self.slice_width / self.num_slices) for slice_num in range(self.num_slices)]
		
		# Set up a list to hold compressed versions of the images, formatted as slices at top level
		#		This is due to compression only working on a single slice
		self.compressed_images = [None]*self.num_slices
		
		# Assign a file to contain positional information for the images for stitching purposes
		#		This file may or may not exist already in the directory, but is either referenced or created during stitching
		self.image_grid_file = self.stitching_dir + '/stitch_grid.txt'
		return(True)
	
	def setStitchedImage(self, stitched_image):
		self.stitched_file = stitched_image
		self.slice_name = os.path.split(stitched_image)[-1].split('.')[0]
		self.stitched_image = Image.open(self.stitched_file)
		#self.stitched_image = skio.MultiImage(self.stitched_file)
		#self.num_slices = len(self.stitched_image)
		self.num_slices = self.stitched_image.n_frames
		self.slice_positions = [slice_num*(self.slice_width / self.num_slices) for slice_num in range(self.num_slices)]
		self.__setDefaultKeyValues()
		return(True)
	
	def getStitchedSubslice(self, subslice_ind):
		try:
			image_frames = confocalhelper.splitImageFrames(self.stitched_image)
			selected_frame = image_frames[subslice_ind] if subslice_ind < self.num_slices else image_frames[0]
			#return(True)
			return(selected_frame)
		except:
			return(False)
	
	def getRGBSlice(self, frame_ind):
		return(self.raw_images[0][frame_ind])
	
	def createStitchedImage(self, channel_num=0, overlap=0.1, compress_ratio=1, frame=0, stitched_file=False, force_file=False):
		"""Stitch images together in a grid.
		"""
		# Either read or create the image grid file
		if os.path.isfile(self.image_grid_file):
			self.im_grid, self.im_resolutions = confocalhelper.readImageGrid(self.image_grid_file)
		else:
			# If the image grid file does not exist, create it for future use.
			self.im_locs, im_locs_dict = confocalhelper.getImagePositions(self.tif_files)
			self.im_grid, self.im_resolutions = confocalhelper.getImageGrid(self.tif_files, self.im_locs, im_locs_dict)
			confocalhelper.writeImageGrid(self.im_grid, self.im_resolutions, self.image_grid_file)
		
		# Define a name for the file to use to store the stitched image
		if not stitched_file:
			stitched_file = self.confocal_dir + '/StitchedImages/' + self.slice_name + 'slice' + str(frame) + 'chan' + str(channel_num) + '.tif'
		if os.path.isfile(stitched_file) and not force_file:
			# If the file exists and overwriting isn't forced, don't bother stitching
			print('File exists! No need to overwrite.')
			return(True)
		
		# If there is nothing in the compressed images (i.e. it hasn't already been compressed), instantiate it
		if not self.compressed_images[frame]:
			self.compressed_images[frame] = [None]*len(self.raw_images)
		
		# Iterate through the images to generate compressed versions
		image_desc = False
		for image_num, raw_image in enumerate(self.raw_images):
			# Pull the desired frame
			if self.ome_files:
				# Adjust for the frame-channel shift with OME Tiff files
				image_frame_channel = confocalhelper.splitImageFrames(raw_image)[channel_num*self.num_slices+frame]
				if not image_desc:
					image_desc = confocalhelper.changeDescriptionScales(confocalhelper.getImageDescription(self.tif_files[image_num]), self.im_resolutions, compress_ratio, ome_file=True)
			else:
				image_frame = confocalhelper.splitImageFrames(raw_image)[frame] if frame < raw_image.n_frames else confocalhelper.splitImageFrames(raw_image)[0]
				# Split the frame into the desired channel
				image_frame_channel = image_frame.split()[channel_num]
				if not image_desc:
					image_desc = confocalhelper.changeDescriptionScales(confocalhelper.getImageDescription(self.tif_files[image_num]), self.im_resolutions, compress_ratio, ome_file=False)
			# If being told to compress the images, compress to desired ratio
			compressed_frame = confocalhelper.compressImages(image_frame_channel, image_scale=compress_ratio)
			self.compressed_images[frame][image_num] = compressed_frame

		# Pass the compressed images, image grid information, and stitched file to save to the image stitching function
		stitched_success = confocalhelper.stitchImages(self.compressed_images[frame], self.im_grid[:, 0], self.im_grid[:, 1], save_pos=stitched_file, description=image_desc)
		
	def createImageGridFile(self):
		im_locs, im_locs_dict = confocalhelper.getImagePositions(self.tif_files)
		im_grid = confocalhelper.getImageGrid(self.tif_files, im_locs, im_locs_dict)
		confocalhelper.writeImageGrid(im_grid, self.image_grid_file)
	
	def importStitchedImage(self, image_file):
		self.model_images = confocalhelper.openModelImage(image_file)
	
	def importModelData(self, data_file):
		'''Deprecated
		'''
		model_dict = {}
		self.slice_name = os.path.splitext(os.path.basename(data_file))[0]
		with open(data_file, 'r') as import_file:
			for file_line in import_file.readlines():
				if ':' in file_line:
					split_line = file_line.split(':')
					data_input = split_line[1].strip()
					try:
						model_dict[split_line[0]] = float(data_input)
					except:
						arr_depth = np.min([substr.start() for substr in re.finditer('[^[({]', data_input)])
						string_arr = confocalhelper.stringToArr(data_input, arr_depth)
						model_dict[split_line[0]] = string_arr
		self.contours = model_dict
		self.num_slices = len(self.contours['endo_contours'])
		self.slice_positions = [slice_num*(self.slice_width / self.num_slices) for slice_num in range(self.num_slices)]
		self.__setDefaultKeyValues()
		self.data_imported = True
		return(model_dict)
	
	def importCSDataPickle(self, csd_file = False):
		if not csd_file:
			csd_file = self.csd_file
		with open(csd_file, 'rb') as slice_file:
			model_dict = pickle.load(slice_file)
		self.endo_contour = model_dict['endo_contour']
		self.epi_contour = model_dict['epi_contour']
		self.channels = model_dict['channels']
		self.slice_separation = model_dict['slice_separation']
		#self.image_resolution = model_dict['image_resolution']
		insertion_pts = model_dict['insertion_pts']
		#if insertion_pts.shape[0] > 2:
		self.septal_pts = insertion_pts
		#else:
		#	self.septal_pts = mathhelper.findConfMidPt(np.transpose(insertion_pts[:2, :2]), np.array(self.endo_contour[0]))
		return(True)
	
	def exportCSDataPickle(self, csd_file = False):
		if not csd_file:
			csd_file = self.csd_file
		model_dict = {
			'endo_contour' : self.endo_contour,
			'epi_contour' : self.epi_contour,
			'channels' : self.channels,
			'slice_separation' : self.slice_separation,
			'insertion_pts' : self.septal_pts
			}
		#'image_resolution' : self.image_resolution,
		with open(csd_file, 'wb') as slice_file:
			pickle.dump(model_dict, slice_file, protocol=pickle.HIGHEST_PROTOCOL)
		return(True)
	
	def importCSData(self):
		'''Function to import data from the CSD file in the data directory of the confocal slice.
		'''
		# Instantiate dict structure to store data pulled as text from file.
		model_dict = {}
		with open(self.csd_file, 'r') as csd_file:
			for file_line in csd_file.readlines():
				if ':' in file_line:
					split_line = file_line.split(':')
					data_key = split_line[0].strip()
					data_input = split_line[1].strip()
					if data_key == 'slice_separation':
						model_dict[data_key] = float(data_input.strip())
					if data_key == 'channel_list':
						channel_list_strs = split_line[1].split(',')
						model_dict[data_key] = [int(channel_list_str.strip()) for channel_list_str in channel_list_strs]
					if data_key == 'septal_pts_x':
						septal_x_strs = split_line[1].split(',')
						model_dict[data_key] = [float(septal_x_str.strip()) for septal_x_str in septal_x_strs]
					if data_key == 'septal_pts_y':
						septal_y_strs = split_line[1].split(',')
						model_dict[data_key] = [float(septal_y_str.strip()) for septal_y_str in septal_y_strs]
					if data_key == 'image_resolution':
						model_dict[data_key] = float(data_input.strip())
					if data_key == 'epi_contour_y':
						epi_y_slice_strs = [epi_y_slice_str.strip().split(',') for epi_y_slice_str in split_line[1].split(';')]
						model_dict[data_key] = [[float(epi_y_str.strip()) for epi_y_str in epi_y_strs] for epi_y_strs in epi_y_slice_strs]
					if data_key == 'epi_contour_x':
						epi_x_slice_strs = [epi_x_slice_str.strip().split(',') for epi_x_slice_str in split_line[1].split(';')]
						model_dict[data_key] = [[float(epi_x_str.strip()) for epi_x_str in epi_x_strs] for epi_x_strs in epi_x_slice_strs]
					if data_key == 'endo_contour_y':
						endo_y_slice_strs = [endo_y_slice_str.strip().split(',') for endo_y_slice_str in split_line[1].split(';')]
						model_dict[data_key] = [[float(endo_y_str.strip()) for endo_y_str in endo_y_strs] for endo_y_strs in endo_y_slice_strs]
					if data_key == 'endo_contour_x':
						endo_x_slice_strs = [endo_x_slice_str.strip().split(',') for endo_x_slice_str in split_line[1].split(';')]
						model_dict[data_key] = [[float(endo_x_str.strip()) for endo_x_str in endo_x_strs] for endo_x_strs in endo_x_slice_strs]
		# Slice separation and channel listing do not require additional processing.
		self.slice_separation = model_dict['slice_separation']
		self.channels = model_dict['channel_list']
		self.image_resolution = model_dict['image_resolution']
		# Iterate through the contours to generate a finalized 80-point contour to align with the standard from the MR model
		endo_contour = [None]*len(model_dict['endo_contour_x'])
		epi_contour = [None]*len(model_dict['epi_contour_x'])
		if len(endo_contour) != len(epi_contour): return(False)
		for subslice_idx in range(len(endo_contour)):
			endo_contour_subslice = np.column_stack((model_dict['endo_contour_x'][subslice_idx], model_dict['endo_contour_y'][subslice_idx]))
			epi_contour_subslice = np.column_stack((model_dict['epi_contour_x'][subslice_idx], model_dict['epi_contour_y'][subslice_idx]))
			# If there are 80 points in the contour, no need to interpolate to 80 points
			if endo_contour_subslice.shape[0] == 80:
				endo_contour[subslice_idx] = endo_contour_subslice
			else:
				endo_interp_func = sp.interpolate.interp1d(np.linspace(0, 1, endo_contour_subslice.shape[0]), endo_contour_subslice, axis=0, kind='cubic')
				endo_contour[subslice_idx] = endo_interp_func(np.linspace(0, 1, 80))
			if epi_contour_subslice.shape[0] == 80:
				epi_contour[subslice_idx] = epi_contour_subslice
			else:
				epi_interp_func = sp.interpolate.interp1d(np.linspace(0, 1, epi_contour_subslice.shape[0]), epi_contour_subslice, axis=0, kind='cubic')
				epi_contour[subslice_idx] = epi_interp_func(np.linspace(0, 1, 80))
		# Convert the contours into an array of 3 dimensions: [subslice, point, x/y]
		self.endo_contour = np.array(endo_contour)
		self.epi_contour = np.array(epi_contour)
		# Pull insertion points and process as in MR processing
		insertion_pts = np.column_stack((model_dict['septal_pts_x'], model_dict['septal_pts_y'])).transpose().squeeze()
		if insertion_pts.shape[1] > 2:
			self.septal_pts = insertion_pts
		else:
			self.septal_pts = mathhelper.findConfMidPt(insertion_pts, self.endo_contour[0, :, :])
		self.data_imported = True
		return(True)
	
	def importFluorescenceData(self, channel_name, image_file):
		'''Deprecated.
		'''
		if not self.data_imported:
			return(False)
		fluorescence_image = Image.open(image_file)
		arr_list = [None for i in range(fluorescence_image.n_frames)]
		mask_locs_list = [None for i in range(fluorescence_image.n_frames)]
		for slice_num in range(fluorescence_image.n_frames):
			fluorescence_image.seek(slice_num)
			slice_endo_contour = self.contours['endo_contours'][slice_num]
			slice_epi_contour = self.contours['epi_contours'][slice_num]
			mask_image = Image.new(mode='L', size=fluorescence_image.size)
			mask_image_draw = ImageDraw.Draw(mask_image)
			if len(slice_epi_contour):
				mask_image_draw.polygon(slice_epi_contour, fill=255)
			if len(slice_endo_contour):
				mask_image_draw.polygon(slice_endo_contour, fill=0)
			mask_arr = np.array(mask_image)
			mask_locs_list[slice_num] = np.where(mask_arr)
			arr_list[slice_num] = np.array(fluorescence_image)[mask_locs_list[slice_num]]
		return(mask_locs_list, arr_list)
	
	def __setDefaultKeyValues(self):
		if self.keys_set:
			return(True)
		contour_keys = ['endo_contours', 'epi_contours', 'endo_resized_contours', 'epi_resized_contours']
		for contour_key in contour_keys:
			if not contour_key in self.contours:
				self.contours[contour_key] = [[] for i in range(self.num_slices)]
		pinpt_keys = ['pinpoints', 'resized_pinpoints']
		for pinpt_key in pinpt_keys:
			if not pinpt_key in self.contours:
				self.contours[pinpt_key] = []
		meta_keys = ['image_resolution']
		for meta_key in meta_keys:
			if not meta_key in self.contours:
				self.contours[meta_key] = 1
		self.keys_set = True
		return(True)
		
	def exportModelData(self, file_name):
		export_dict = {}
		export_dict['slice_separation'] = str(self.slice_separation)
		channel_list_str = ''
		for channel in self.channels:
			channel_list_str = channel_list_str + str(channel) + ','
		export_dict['channel_list'] = channel_list_str[:-1]
		septal_x_str = ''
		septal_y_str = ''
		for septal_point_idx in range(self.septal_pts.shape[0]):
			septal_x_str = septal_x_str + str(self.septal_pts[septal_point_idx, 0]) + ','
			septal_y_str = septal_y_str + str(self.septal_pts[septal_point_idx, 1]) + ','
		export_dict['septal_pts_x'] = septal_x_str[:-1]
		export_dict['septal_pts_y'] = septal_y_str[:-1]
		export_dict['image_resolution'] = str(self.image_resolution)
		endo_x_str = ''
		endo_y_str = ''
		for subslice_idx in range(self.endo_contour.shape[0]):
			endo_x_subslice_str = ''
			endo_y_subslice_str = ''
			for point_idx in range(self.endo_contour.shape[1]):
				endo_x_subslice_str = endo_x_subslice_str + str(self.endo_contour[subslice_idx, point_idx, 0]) + ','
				endo_y_subslice_str = endo_y_subslice_str + str(self.endo_contour[subslice_idx, point_idx, 1]) + ','
			endo_x_str = endo_x_str + endo_x_subslice_str[:-1] + ';'
			endo_y_str = endo_y_str + endo_y_subslice_str[:-1] + ';'
		export_dict['endo_contour_x'] = endo_x_str[:-1]
		export_dict['endo_contour_y'] = endo_y_str[:-1]
		epi_x_str = ''
		epi_y_str = ''
		for subslice_idx in range(self.epi_contour.shape[0]):
			epi_x_subslice_str = ''
			epi_y_subslice_str = ''
			for point_idx in range(self.epi_contour.shape[1]):
				epi_x_subslice_str = epi_x_subslice_str + str(self.epi_contour[subslice_idx, point_idx, 0]) + ','
				epi_y_subslice_str = epi_y_subslice_str + str(self.epi_contour[subslice_idx, point_idx, 1]) + ','
			epi_x_str = epi_x_str + epi_x_subslice_str[:-1] + ';'
			epi_y_str = epi_y_str + epi_y_subslice_str[:-1] + ';'
		export_dict['epi_contour_x'] = epi_x_str[:-1]
		export_dict['epi_contour_y'] = epi_y_str[:-1]
		with open(file_name, 'w+') as export_file:
			export_file.writelines([var_name + ':' + var_value + '\n' for var_name, var_value in export_dict.items()])