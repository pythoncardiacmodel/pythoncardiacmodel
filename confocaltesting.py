from cardiachelpers import confocalhelper, displayhelper
#import numpy as np
#import matplotlib.pyplot as mplt
#from mpl_toolkits.mplot3d import Axes3D
import mrimodel
import mesh
from PIL import Image
import numpy as np

sax_file = 'C:/Users/cwsoc/OneDrive/Grad School Research/CreMouseStudy/CardiacCre-Female3/Day28/CardiacCre-Female3-SAx-Day28.mat'
lax_file = 'C:/Users/cwsoc/OneDrive/Grad School Research/CreMouseStudy/CardiacCre-Female3/Day28/CardiacCre-Female3-LAx-Day28.mat'
lge_file = 'C:/Users/cwsoc/OneDrive/Grad School Research/CreMouseStudy/CardiacCre-Female3/Day28/CardiacCre-Female3-LGE-Day28.mat'
dense_files = ['C:/Users/cwsoc/OneDrive/Grad School Research/CreMouseStudy/CardiacCre-Female3/Day28/CardiacCre-Female3-DENSESlice1-Day28.mat', 'C:/Users/cwsoc/OneDrive/Grad School Research/CreMouseStudy/CardiacCre-Female3/Day28/CardiacCre-Female3-DENSESlice2-Day28.mat', 'C:/Users/cwsoc/OneDrive/Grad School Research/CreMouseStudy/CardiacCre-Female3/Day28/CardiacCre-Female3-DENSESlice3-Day28.mat']
conf_slice_dir = 'C:/Users/cwsoc/OneDrive/Grad School Research/CreMouseStudy/CardiacCre-Female3/Confocal'
conf_out_file = 'C:/Users/cwsoc/OneDrive/Documents/pythoncardiacmodel/pythoncardiacmodel/Test Data/NewConfModel/NewConfSlice1/NewConfSlice1v2.csd'
mri_model = mrimodel.MRIModel(sax_file, lax_file, sa_scar_file=lge_file, dense_file=dense_files, confocal_directory=conf_slice_dir)
mri_model.importCine()
mri_model.importLGE()
mri_model.importDense()
mri_model.importConfocal(compressed='dense')
mri_model.convertDataProlate()
mri_model.alignScar()
mri_model.alignDense()
mri_model.alignConfocal()
num_rings = 28
elem_per_ring = 48
elem_in_wall = 5
time_point = 0
conf_mesh = mesh.Mesh(num_rings, elem_per_ring, elem_in_wall)
conf_mesh.fitContours(mri_model.cine_endo_rotate[time_point][:, :3], mri_model.cine_epi_rotate[time_point][:, :3], mri_model.cine_apex_pt, mri_model.cine_basal_pt, mri_model.cine_septal_pts, '4x8')
conf_mesh.feMeshRender()
conf_mesh.nodeNum(conf_mesh.meshCart[0], conf_mesh.meshCart[1], conf_mesh.meshCart[2])
conf_mesh.getElemConMatrix()
conf_mesh.interpScarData(mri_model.interp_scar, trans_smooth=1, depth_smooth=0.5)
conf_mesh.assignDenseElems(mri_model.dense_aligned_pts, mri_model.dense_slices, mri_model.dense_aligned_displacement, mri_model.radial_strain, mri_model.circumferential_strain)
conf_mesh.assignConfElems(mri_model.conf_aligned_pts, mri_model.conf_aligned_vals)
scar_conf_channels = [conf_mesh.getElemData(conf_mesh.elems_in_scar, 'confocal', channel=channel_ind) for channel_ind in range(4)]
border_conf_channels = [conf_mesh.getElemData(conf_mesh.border_elems, 'confocal', channel=channel_ind) for channel_ind in range(4)]
nonscar_conf_channels = [conf_mesh.getElemData(conf_mesh.elems_out_scar, 'confocal', channel=channel_ind) for channel_ind in range(4)]
print(scar_conf_channels)
print(border_conf_channels)
print(nonscar_conf_channels)

#print(mri_model.conf_aligned_pts.shape)
#mri_model.confocal_model.confocal_slices[0].addContourPoint('endo', 0, (155, 123))
#mri_model.confocal_model.confocal_slices[0].exportModelData(conf_out_file)

#test_confocal_model = confocalmodel.ConfocalModel()

#test_confocal_model.setImportDirectory(import_data_dir)
#test_confocal_model.importModelData()

#test_fluo_locs = test_confocal_model.importSliceFluorescences(import_data_dir, '488')

#endo_contours, epi_contours, apex_pinpoint, basal_pinpoint, septal_pinpoints, unrot_septal_pinpoints = test_confocal_model.prepDataForMesh()
#conf_test_mesh = mesh.Mesh()
#conf_test_mesh.fitContours(endo_contours, epi_contours, apex_pinpoint, basal_pinpoint, septal_pinpoints[0], '4x8')
#conf_test_mesh.feMeshRender()
#conf_test_mesh.nodeNum(conf_test_mesh.meshCart[0], conf_test_mesh.meshCart[1], conf_test_mesh.meshCart[2])
#conf_test_mesh.getElemConMatrix()