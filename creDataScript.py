import mrimodel
import confocalmodel
import mesh
import glob, os
import csv
from natsort import natsorted, ns
import numpy as np

data_dir = 'C:/Users/cwsoc/OneDrive/Grad School Research/CreMouseStudy'
data_file_loc = 'C:/Users/cwsoc/OneDrive/Grad School Research/CreMouseStudy/ConfData.csv'
all_folders = [f.path for f in os.scandir(data_dir) if f.is_dir()]
cardiac_folders = [dir_path for dir_path in all_folders if "CardiacCre" in dir_path]
mac_folders = [dir_path for dir_path in all_folders if "MacCre" in dir_path]
inos_folders = [dir_path for dir_path in all_folders if "iNOS" in dir_path]
num_rings = 28
elem_per_ring = 48
elem_thru_wall = 5
mesh_timepoint = 0
mesh_type = '4x8'
border_size = 3

with open(data_file_loc, 'w') as data_file:
	strain_writer = csv.writer(data_file)
	for folder_path in cardiac_folders:
		# Pull Day-specific subfolders from each mouse folder iteratively
		cur_mouse = os.path.basename(os.path.normpath(folder_path))
		day_folders = [f.path for f in os.scandir(folder_path) if f.is_dir() and not "Dicom" in f.path and not "Confocal" in f.path and not "Micro" in f.path]
		if len(day_folders) == 4:
			day_pref = 'Day28'
		elif len(day_folders) == 3:
			day_pref = 'Day7'
		else:
			day_pref = 'Day2'
		conf_folder = os.path.join(folder_path, "Confocal")
		mouse_conf_data = os.path.isdir(conf_folder)
		print(cur_mouse)
		for day_folder in day_folders:
			cur_day = os.path.basename(os.path.normpath(day_folder))
			run_conf = (cur_day == day_pref) & mouse_conf_data
			print(run_conf)
			all_files = [f.path for f in os.scandir(day_folder) if ".mat" in f.path]
			# Select short-axis, long-axis, lge (if any), and DENSE files from the subfolder using standard naming conventions
			sax_file = next((filename for filename in all_files if "SAx" in filename), None)
			lax_file = next((filename for filename in all_files if "LAx" in filename), None)
			lge_file = next((filename for filename in all_files if "LGE" in filename and not "LGE4ch" in filename and not "LGELAx" in filename and not "4chLGE" in filename), None)
			dense_files = [filename for filename in all_files if "dense" in str.lower(filename)]
			print(cur_day)
			# Instantiate the MRI model using the selected files.
			mri_model = mrimodel.MRIModel(sax_file, lax_file, sa_scar_file=lge_file, dense_file=dense_files, confocal_directory=conf_folder) if run_conf else mrimodel.MRIModel(sax_file, lax_file, sa_scar_file=lge_file, dense_file=dense_files)
			mri_model.importCine(timepoint=0)
			# Only attempt to fit scar values if there is an LGE file
			if mri_model.scar:
				mri_model.importLGE()
				mri_model.convertDataProlate()
				interp_scar = mri_model.alignScar()
			# Import DENSE and align to the cine baseline at end diastole.
			mri_model.importDense()
			mri_model.convertDataProlate()
			mri_model.alignDense(cine_timepoint=0)
			# Only pull confocal data if 
			if run_conf:
				mri_model.importConfocal(compressed=0.1)
				mri_model.convertDataProlate()
				mri_model.alignConfocal()
			# Instantiate the finite-element mesh using the values defined above.
			mri_mesh = mesh.Mesh(num_rings, elem_per_ring, elem_thru_wall)
			# Fit the mesh to the contours defined in the multiparametric model
			mri_mesh.fitContours(mri_model.cine_endo_rotate[mesh_timepoint][:, :3], mri_model.cine_epi_rotate[mesh_timepoint][:, :3], mri_model.cine_apex_pt, mri_model.cine_basal_pt, mri_model.cine_septal_pts, mesh_type)
			# Standard mesh instantiation steps to define the connectivity matrix between elements and define nodal geometry.
			mri_mesh.feMeshRender()
			mri_mesh.nodeNum(mri_mesh.meshCart[0], mri_mesh.meshCart[1], mri_mesh.meshCart[2])
			mri_mesh.getElemConMatrix()
			# Update the prolate spheroid coordinate conversion in the model based on the focus from the mesh
			mri_model.convertDataProlate(mri_mesh.focus)
			mri_mesh.rotateNodesProlate()
			# Interpolate scar within the mesh based on the traces in the MRI model
			if mri_model.scar:
				mri_mesh.interpScarData(mri_model.interp_scar, trans_smooth=1, depth_smooth=0.5, border_size=border_size)
			else:
				mri_mesh.randomizeScarElems()
			# Interpolate DENSE across the possible elements.
			#try:
			mri_mesh.assignDenseElems(mri_model.dense_aligned_pts, mri_model.dense_slices, mri_model.dense_aligned_displacement, mri_model.radial_strain, mri_model.circumferential_strain)
			if run_conf:
				mri_mesh.assignConfElems(mri_model.conf_aligned_pts, mri_model.conf_aligned_vals)
				scar_conf_channels = [mri_mesh.getElemData(mri_mesh.elems_in_scar, 'confocal', channel=channel_ind) for channel_ind in range(4)]
				border_conf_channels = [mri_mesh.getElemData(mri_mesh.border_elems, 'confocal', channel=channel_ind) for channel_ind in range(4)]
				nonscar_conf_channels = [mri_mesh.getElemData(mri_mesh.elems_out_scar, 'confocal', channel=channel_ind) for channel_ind in range(4)]
				data_row = [cur_mouse, cur_day, scar_conf_channels[0], scar_conf_channels[1], scar_conf_channels[2], scar_conf_channels[3]]
				strain_writer.writerow(data_row)
				data_row = [cur_mouse, cur_day, border_conf_channels[0], border_conf_channels[1], border_conf_channels[2], border_conf_channels[3]]
				strain_writer.writerow(data_row)
				data_row = [cur_mouse, cur_day, nonscar_conf_channels[0], nonscar_conf_channels[1], nonscar_conf_channels[2], nonscar_conf_channels[3]]
				strain_writer.writerow(data_row)
			# Calculate average DENSE path in infarct, borderzone, and remote regions
			'''scar_average_dense = [None]*mri_mesh.dense_radial_strain.shape[1]
			border_average_dense = [None]*mri_mesh.dense_radial_strain.shape[1]
			remote_average_dense = [None]*mri_mesh.dense_radial_strain.shape[1]
			for time_point in range(mri_mesh.dense_radial_strain.shape[1]):
				scar_average_dense[time_point] = mri_mesh.getElemData(mri_mesh.elems_in_scar, 'dense', format="Average", timepoint=time_point).tolist()
				border_average_dense[time_point] = mri_mesh.getElemData(mri_mesh.border_elems, 'dense', format="Average", timepoint=time_point).tolist()
				remote_average_dense[time_point] = mri_mesh.getElemData(mri_mesh.elems_out_scar, 'dense', format="Average", timepoint=time_point).tolist()
			scar_radial = [scar_dense[0] for scar_dense in scar_average_dense]
			scar_circ = [scar_dense[1] for scar_dense in scar_average_dense]
			border_radial = [border_dense[0] for border_dense in border_average_dense]
			border_circ = [border_dense[1] for border_dense in border_average_dense]
			remote_radial = [remote_dense[0] for remote_dense in remote_average_dense]
			remote_circ = [remote_dense[1] for remote_dense in remote_average_dense]
			data_row = [cur_mouse, cur_day, max(scar_radial), max(border_radial), max(remote_radial), min(scar_circ), min(border_circ), min(remote_circ)]
			'''
			#data_row = [cur_mouse, cur_day, np.nanmean(mri_mesh.radial_piecewise_errs), np.nanmean(mri_mesh.radial_linear_errs), np.nanmean(mri_mesh.circ_piecewise_errs), np.nanmean(mri_mesh.circ_linear_errs)]
			#strain_writer.writerow(data_row)
			#print('[Scar Radial, Border Radial, Remote Radial]')
			#print([max(scar_radial), max(border_radial), max(remote_radial)])
			#print(['Scar Circ, Border Circ, Remote Circ'])
			#print([min(scar_circ), min(border_circ), min(remote_circ)])
			'''if mri_model.scar:
				print('Randomized elements for control.')
				mri_mesh.randomizeScarElems()
				scar_average_dense = [None]*mri_mesh.dense_radial_strain.shape[1]
				border_average_dense = [None]*mri_mesh.dense_radial_strain.shape[1]
				remote_average_dense = [None]*mri_mesh.dense_radial_strain.shape[1]
				for time_point in range(mri_mesh.dense_radial_strain.shape[1]):
					scar_average_dense[time_point] = mri_mesh.getElemData(mri_mesh.elems_in_scar, 'dense', format="Average", timepoint=time_point).tolist()
					border_average_dense[time_point] = mri_mesh.getElemData(mri_mesh.border_elems, 'dense', format="Average", timepoint=time_point).tolist()
					remote_average_dense[time_point] = mri_mesh.getElemData(mri_mesh.elems_out_scar, 'dense', format="Average", timepoint=time_point).tolist()
				scar_radial = [scar_dense[0] for scar_dense in scar_average_dense]
				scar_circ = [scar_dense[1] for scar_dense in scar_average_dense]
				border_radial = [border_dense[0] for border_dense in border_average_dense]
				border_circ = [border_dense[1] for border_dense in border_average_dense]
				remote_radial = [remote_dense[0] for remote_dense in remote_average_dense]
				remote_circ = [remote_dense[1] for remote_dense in remote_average_dense]
				#print('[Scar Radial, Border Radial, Remote Radial]')
				#print([max(scar_radial), max(border_radial), max(remote_radial)])
				#print(['Scar Circ, Border Circ, Remote Circ'])
				#print([min(scar_circ), min(border_circ), min(remote_circ)])'''
			#except:
			#	print('Error\n\n\n\n\n')
			#	pass
			
	for folder_path in mac_folders:
		cur_mouse = os.path.basename(os.path.normpath(folder_path))
		day_folders = [f.path for f in os.scandir(folder_path) if f.is_dir() and not "Dicom" in f.path and not "Confocal" in f.path and not "Micro" in f.path]
		if len(day_folders) == 4:
			day_pref = 'Day28'
		elif len(day_folders) == 3:
			day_pref = 'Day7'
		else:
			day_pref = 'Day2'
		conf_folder = os.path.join(folder_path, "Confocal")
		mouse_conf_data = os.path.isdir(conf_folder)
		print(cur_mouse)
		for day_folder in day_folders:
			cur_day = os.path.basename(os.path.normpath(day_folder))
			run_conf = (cur_day == day_pref) & mouse_conf_data
			print(run_conf)
			all_files = [f.path for f in os.scandir(day_folder) if ".mat" in f.path]
			sax_file = next((filename for filename in all_files if "SAx" in filename), None)
			lax_file = next((filename for filename in all_files if "LAx" in filename), None)
			lge_file = next((filename for filename in all_files if "LGE" in filename and not "LGE4ch" in filename and not "LGELAx" in filename and not "4chLGE" in filename), None)
			dense_files = [filename for filename in all_files if "dense" in str.lower(filename)]
			print(cur_day)
			mri_model = mrimodel.MRIModel(sax_file, lax_file, sa_scar_file=lge_file, dense_file=dense_files, confocal_directory=conf_folder) if run_conf else mrimodel.MRIModel(sax_file, lax_file, sa_scar_file=lge_file, dense_file=dense_files)
			mri_model.importCine(timepoint=0)
			if mri_model.scar:
				mri_model.importLGE()
				mri_model.convertDataProlate()
				interp_scar = mri_model.alignScar()
			mri_model.importDense()
			mri_model.convertDataProlate()
			mri_model.alignDense(cine_timepoint=0)
			if run_conf:
				mri_model.importConfocal(compressed=0.1)
				mri_model.convertDataProlate()
				mri_model.alignConfocal()
			# Instantiate the finite-element mesh using the values defined above.
			mri_mesh = mesh.Mesh(num_rings, elem_per_ring, elem_thru_wall)
			# Fit the mesh to the contours defined in the multiparametric model
			mri_mesh.fitContours(mri_model.cine_endo_rotate[mesh_timepoint][:, :3], mri_model.cine_epi_rotate[mesh_timepoint][:, :3], mri_model.cine_apex_pt, mri_model.cine_basal_pt, mri_model.cine_septal_pts, mesh_type)
			# Standard mesh instantiation steps to define the connectivity matrix between elements and define nodal geometry.
			mri_mesh.feMeshRender()
			mri_mesh.nodeNum(mri_mesh.meshCart[0], mri_mesh.meshCart[1], mri_mesh.meshCart[2])
			mri_mesh.getElemConMatrix()
			# Update the prolate spheroid coordinate conversion in the model based on the focus from the mesh
			mri_model.convertDataProlate(mri_mesh.focus)
			mri_mesh.rotateNodesProlate()
			# Interpolate scar within the mesh based on the traces in the MRI model
			if mri_model.scar:
				mri_mesh.interpScarData(mri_model.interp_scar, trans_smooth=1, depth_smooth=0.5, border_size=border_size)
			else:
				mri_mesh.randomizeScarElems()
			# Interpolate DENSE across the possible elements.
			try:
				mri_mesh.assignDenseElems(mri_model.dense_aligned_pts, mri_model.dense_slices, mri_model.dense_aligned_displacement, mri_model.radial_strain, mri_model.circumferential_strain)
				if run_conf:
					mri_mesh.assignConfElems(mri_model.conf_aligned_pts, mri_model.conf_aligned_vals)
					scar_conf_channels = [mri_mesh.getElemData(mri_mesh.elems_in_scar, 'confocal', channel=channel_ind) for channel_ind in range(4)]
					border_conf_channels = [mri_mesh.getElemData(mri_mesh.border_elems, 'confocal', channel=channel_ind) for channel_ind in range(4)]
					nonscar_conf_channels = [mri_mesh.getElemData(mri_mesh.elems_out_scar, 'confocal', channel=channel_ind) for channel_ind in range(4)]
					data_row = [cur_mouse, cur_day, scar_conf_channels[0], scar_conf_channels[1], scar_conf_channels[2], scar_conf_channels[3]]
					strain_writer.writerow(data_row)
					data_row = [cur_mouse, cur_day, border_conf_channels[0], border_conf_channels[1], border_conf_channels[2], border_conf_channels[3]]
					strain_writer.writerow(data_row)
					data_row = [cur_mouse, cur_day, nonscar_conf_channels[0], nonscar_conf_channels[1], nonscar_conf_channels[2], nonscar_conf_channels[3]]
					strain_writer.writerow(data_row)
				# Calculate average DENSE path in infarct, borderzone, and remote regions
				'''scar_average_dense = [None]*mri_mesh.dense_radial_strain.shape[1]
				border_average_dense = [None]*mri_mesh.dense_radial_strain.shape[1]
				remote_average_dense = [None]*mri_mesh.dense_radial_strain.shape[1]
				for time_point in range(mri_mesh.dense_radial_strain.shape[1]):
					scar_average_dense[time_point] = mri_mesh.getElemData(mri_mesh.elems_in_scar, 'dense', format="Average", timepoint=time_point).tolist()
					border_average_dense[time_point] = mri_mesh.getElemData(mri_mesh.border_elems, 'dense', format="Average", timepoint=time_point).tolist()
					remote_average_dense[time_point] = mri_mesh.getElemData(mri_mesh.elems_out_scar, 'dense', format="Average", timepoint=time_point).tolist()
				scar_radial = [scar_dense[0] for scar_dense in scar_average_dense]
				scar_circ = [scar_dense[1] for scar_dense in scar_average_dense]
				border_radial = [border_dense[0] for border_dense in border_average_dense]
				border_circ = [border_dense[1] for border_dense in border_average_dense]
				remote_radial = [remote_dense[0] for remote_dense in remote_average_dense]
				remote_circ = [remote_dense[1] for remote_dense in remote_average_dense]
				data_row = [cur_mouse, cur_day, max(scar_radial), max(border_radial), max(remote_radial), min(scar_circ), min(border_circ), min(remote_circ)]
				#data_row = [cur_mouse, cur_day, np.nanmean(mri_mesh.radial_piecewise_errs), np.nanmean(mri_mesh.radial_linear_errs), np.nanmean(mri_mesh.circ_piecewise_errs), np.nanmean(mri_mesh.circ_linear_errs)]
				strain_writer.writerow(data_row)'''
				'''if mri_model.scar:
					print('Randomized elements for control.')
					mri_mesh.randomizeScarElems()
					scar_average_dense = [None]*mri_mesh.dense_radial_strain.shape[1]
					border_average_dense = [None]*mri_mesh.dense_radial_strain.shape[1]
					remote_average_dense = [None]*mri_mesh.dense_radial_strain.shape[1]
					for time_point in range(mri_mesh.dense_radial_strain.shape[1]):
						scar_average_dense[time_point] = mri_mesh.getElemData(mri_mesh.elems_in_scar, 'dense', format="Average", timepoint=time_point).tolist()
						border_average_dense[time_point] = mri_mesh.getElemData(mri_mesh.border_elems, 'dense', format="Average", timepoint=time_point).tolist()
						remote_average_dense[time_point] = mri_mesh.getElemData(mri_mesh.elems_out_scar, 'dense', format="Average", timepoint=time_point).tolist()
					scar_radial = [scar_dense[0] for scar_dense in scar_average_dense]
					scar_circ = [scar_dense[1] for scar_dense in scar_average_dense]
					border_radial = [border_dense[0] for border_dense in border_average_dense]
					border_circ = [border_dense[1] for border_dense in border_average_dense]
					remote_radial = [remote_dense[0] for remote_dense in remote_average_dense]
					remote_circ = [remote_dense[1] for remote_dense in remote_average_dense]
					#print('[Scar Radial, Border Radial, Remote Radial]')
					#print([max(scar_radial), max(border_radial), max(remote_radial)])
					#print(['Scar Circ, Border Circ, Remote Circ'])
					#print([min(scar_circ), min(border_circ), min(remote_circ)])'''
			except:
				print('Error\n\n\n\n\n')
				pass
			
	for folder_path in inos_folders:
		cur_mouse = os.path.basename(os.path.normpath(folder_path))
		day_folders = [f.path for f in os.scandir(folder_path) if f.is_dir() and not "Dicom" in f.path and not "Confocal" in f.path and not "Micro" in f.path]
		if len(day_folders) == 4:
			day_pref = 'Day28'
		elif len(day_folders) == 3:
			day_pref = 'Day7'
		else:
			day_pref = 'Day2'
		conf_folder = os.path.join(folder_path, "Confocal")
		mouse_conf_data = os.path.isdir(conf_folder)
		print(cur_mouse)
		for day_folder in day_folders:
			cur_day = os.path.basename(os.path.normpath(day_folder))
			run_conf = (cur_day == day_pref) & mouse_conf_data
			print(run_conf)
			all_files = [f.path for f in os.scandir(day_folder) if ".mat" in f.path]
			sax_file = next((filename for filename in all_files if "SAx" in filename), None)
			lax_file = next((filename for filename in all_files if "LAx" in filename), None)
			lge_file = next((filename for filename in all_files if "LGE" in filename and not "LGE4ch" in filename and not "LGELAx" in filename and not "4chLGE" in filename), None)
			dense_files = [filename for filename in all_files if "dense" in str.lower(filename)]
			print(cur_day)
			mri_model = mrimodel.MRIModel(sax_file, lax_file, sa_scar_file=lge_file, dense_file=dense_files, confocal_directory=conf_folder) if run_conf else mrimodel.MRIModel(sax_file, lax_file, sa_scar_file=lge_file, dense_file=dense_files)
			mri_model.importCine(timepoint=0)
			if mri_model.scar:
				mri_model.importLGE()
				mri_model.convertDataProlate()
				interp_scar = mri_model.alignScar()
			mri_model.importDense()
			mri_model.convertDataProlate()
			mri_model.alignDense(cine_timepoint=0)
			if run_conf:
				mri_model.importConfocal(compressed=0.1)
				mri_model.convertDataProlate()
				mri_model.alignConfocal()
			# Instantiate the finite-element mesh using the values defined above.
			mri_mesh = mesh.Mesh(num_rings, elem_per_ring, elem_thru_wall)
			# Fit the mesh to the contours defined in the multiparametric model
			mri_mesh.fitContours(mri_model.cine_endo_rotate[mesh_timepoint][:, :3], mri_model.cine_epi_rotate[mesh_timepoint][:, :3], mri_model.cine_apex_pt, mri_model.cine_basal_pt, mri_model.cine_septal_pts, mesh_type)
			# Standard mesh instantiation steps to define the connectivity matrix between elements and define nodal geometry.
			mri_mesh.feMeshRender()
			mri_mesh.nodeNum(mri_mesh.meshCart[0], mri_mesh.meshCart[1], mri_mesh.meshCart[2])
			mri_mesh.getElemConMatrix()
			# Update the prolate spheroid coordinate conversion in the model based on the focus from the mesh
			mri_model.convertDataProlate(mri_mesh.focus)
			mri_mesh.rotateNodesProlate()
			# Interpolate scar within the mesh based on the traces in the MRI model
			if mri_model.scar:
				mri_mesh.interpScarData(mri_model.interp_scar, trans_smooth=1, depth_smooth=0.5, border_size=border_size)
			else:
				mri_mesh.randomizeScarElems()
			# Interpolate DENSE across the possible elements.
			try:
				mri_mesh.assignDenseElems(mri_model.dense_aligned_pts, mri_model.dense_slices, mri_model.dense_aligned_displacement, mri_model.radial_strain, mri_model.circumferential_strain)
				if run_conf:
					mri_mesh.assignConfElems(mri_model.conf_aligned_pts, mri_model.conf_aligned_vals)
					scar_conf_channels = [mri_mesh.getElemData(mri_mesh.elems_in_scar, 'confocal', channel=channel_ind) for channel_ind in range(4)]
					border_conf_channels = [mri_mesh.getElemData(mri_mesh.border_elems, 'confocal', channel=channel_ind) for channel_ind in range(4)]
					nonscar_conf_channels = [mri_mesh.getElemData(mri_mesh.elems_out_scar, 'confocal', channel=channel_ind) for channel_ind in range(4)]
					data_row = [cur_mouse, cur_day, scar_conf_channels[0], scar_conf_channels[1], scar_conf_channels[2], scar_conf_channels[3]]
					strain_writer.writerow(data_row)
					data_row = [cur_mouse, cur_day, border_conf_channels[0], border_conf_channels[1], border_conf_channels[2], border_conf_channels[3]]
					strain_writer.writerow(data_row)
					data_row = [cur_mouse, cur_day, nonscar_conf_channels[0], nonscar_conf_channels[1], nonscar_conf_channels[2], nonscar_conf_channels[3]]
					strain_writer.writerow(data_row)
				# Calculate average DENSE path in infarct, borderzone, and remote regions
				'''scar_average_dense = [None]*mri_mesh.dense_radial_strain.shape[1]
				border_average_dense = [None]*mri_mesh.dense_radial_strain.shape[1]
				remote_average_dense = [None]*mri_mesh.dense_radial_strain.shape[1]
				for time_point in range(mri_mesh.dense_radial_strain.shape[1]):
					scar_average_dense[time_point] = mri_mesh.getElemData(mri_mesh.elems_in_scar, 'dense', format="Average", timepoint=time_point).tolist()
					border_average_dense[time_point] = mri_mesh.getElemData(mri_mesh.border_elems, 'dense', format="Average", timepoint=time_point).tolist()
					remote_average_dense[time_point] = mri_mesh.getElemData(mri_mesh.elems_out_scar, 'dense', format="Average", timepoint=time_point).tolist()
				scar_radial = [scar_dense[0] for scar_dense in scar_average_dense]
				scar_circ = [scar_dense[1] for scar_dense in scar_average_dense]
				border_radial = [border_dense[0] for border_dense in border_average_dense]
				border_circ = [border_dense[1] for border_dense in border_average_dense]
				remote_radial = [remote_dense[0] for remote_dense in remote_average_dense]
				remote_circ = [remote_dense[1] for remote_dense in remote_average_dense]
				data_row = [cur_mouse, cur_day, max(scar_radial), max(border_radial), max(remote_radial), min(scar_circ), min(border_circ), min(remote_circ)]
				#data_row = [cur_mouse, cur_day, np.nanmean(mri_mesh.radial_piecewise_errs), np.nanmean(mri_mesh.radial_linear_errs), np.nanmean(mri_mesh.circ_piecewise_errs), np.nanmean(mri_mesh.circ_linear_errs)]
				strain_writer.writerow(data_row)'''
				#print('[Scar Radial, Border Radial, Remote Radial]')
				#print([max(scar_radial), max(border_radial), max(remote_radial)])
				#print(['Scar Circ, Border Circ, Remote Circ'])
				#print([min(scar_circ), min(border_circ), min(remote_circ)])
				'''if mri_model.scar:
					print('Randomized elements for control.')
					mri_mesh.randomizeScarElems()
					scar_average_dense = [None]*mri_mesh.dense_radial_strain.shape[1]
					border_average_dense = [None]*mri_mesh.dense_radial_strain.shape[1]
					remote_average_dense = [None]*mri_mesh.dense_radial_strain.shape[1]
					for time_point in range(mri_mesh.dense_radial_strain.shape[1]):
						scar_average_dense[time_point] = mri_mesh.getElemData(mri_mesh.elems_in_scar, 'dense', format="Average", timepoint=time_point).tolist()
						border_average_dense[time_point] = mri_mesh.getElemData(mri_mesh.border_elems, 'dense', format="Average", timepoint=time_point).tolist()
						remote_average_dense[time_point] = mri_mesh.getElemData(mri_mesh.elems_out_scar, 'dense', format="Average", timepoint=time_point).tolist()
					scar_radial = [scar_dense[0] for scar_dense in scar_average_dense]
					scar_circ = [scar_dense[1] for scar_dense in scar_average_dense]
					border_radial = [border_dense[0] for border_dense in border_average_dense]
					border_circ = [border_dense[1] for border_dense in border_average_dense]
					remote_radial = [remote_dense[0] for remote_dense in remote_average_dense]
					remote_circ = [remote_dense[1] for remote_dense in remote_average_dense]'''
			except:
				print('Error\n\n\n\n\n')
				pass