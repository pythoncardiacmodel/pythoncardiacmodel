# -*- coding: utf-8 -*-
"""
Created on Fri Feb 9 12:48:42 2017

@author: cdw2be
"""
import warnings
warnings.simplefilter('ignore', UserWarning)
import tkinter as tk
from tkinter import filedialog
from tkinter import ttk
from tkinter import font
from tkinter import messagebox
import platform
import vispy
if platform.system() == 'Linux':
	vispy.use("pyqt5")
import mrimodel
import confocalmodel
import mesh
import numpy as np
from cardiachelpers import displayhelper
from cardiachelpers import mathhelper
import math
try:
	import winreg
except ImportError:
	import wslwinreg as winreg
import glob, os
from natsort import natsorted, ns
from PIL import ImageDraw, ImageTk, Image
# Temporary Import
from copy import deepcopy
warnings.simplefilter('default', UserWarning)

class modelGUI(tk.Frame):
	"""Generates a GUI to control the Python-based cardiac modeling toolbox.
	"""
	
	def __init__(self, master=None):
		tk.Frame.__init__(self, master)
		self.grid()
		self.identifyPostView()
		self.createWidgets()
		self.scar_assign = False
		self.dense_assign = False
		self.mri_model = False
		self.mri_mesh = False
		
		# Set row and column paddings
		master.rowconfigure(0, pad=5)
		master.rowconfigure(7, pad=0)
		master.rowconfigure(11, pad=0)
		master.rowconfigure(13, pad=0)
		master.columnconfigure(8, pad=5)
		master.columnconfigure(9, pad=5)
		master.columnconfigure(11, pad=5)
		
		# On window close by user
		master.protocol('WM_DELETE_WINDOW', self.destroy())
		
		self.master = master

	def identifyPostView(self):
		try:
			postview_key = winreg.OpenKey(winreg.HKEY_LOCAL_MACHINE, "SOFTWARE\\University of Utah\\PostView")
			postview_folder = winreg.QueryValueEx(postview_key, "Location")[0]
			os.chdir(postview_folder)
			postview_exe_file = glob.glob('P*.exe')[0]
			self.postview_exe = os.path.join(postview_folder, postview_exe_file)
		except FileNotFoundError:
			self.postview_exe = ''
		
	def createWidgets(self):
		"""Place widgets throughout GUI frame and assign functionality.
		"""
		
		# Establish tk variables
		sa_filename = tk.StringVar()
		la_filename = tk.StringVar()
		lge_filename = tk.StringVar()
		la_lge_filenames = tk.StringVar()
		dense_filenames = tk.StringVar()
		twoxcrm_contrast_filename = tk.StringVar()
		twoxcrm_pd_filename = tk.StringVar()
		premade_mesh_filename = tk.StringVar()
		self.slice_position = tk.StringVar()
		self.slice_width = tk.StringVar()
		conf_export_dir = tk.StringVar()
		conf_import_dir = tk.StringVar()
		self.scar_plot_bool = tk.IntVar(value=0)
		self.dense_plot_bool = tk.IntVar(value=0)
		self.nodes_plot_bool = tk.IntVar(value=0)
		self.conf_scar_plot_bool = tk.IntVar(value=0)
		self.conf_nodes_plot_bool = tk.IntVar(value=0)

		# Import Settings
		#	Place labels
		import_label = ttk.Label(text='MRI Model Options')
		import_label.grid(row=0, column=0, columnspan=9, pady=2)
		ttk.Label(text='Short-Axis File:').grid(row=1, sticky='W', pady=2, padx=5)
		ttk.Label(text='Long-Axis File:').grid(row=2, sticky='W', pady=2, padx=5)
		ttk.Label(text='SA LGE File:').grid(row=3, sticky='W', pady=2, padx=5)
		ttk.Label(text='LA LGE Files:').grid(row=4, sticky='W', pady=2, padx=5)
		ttk.Label(text='DENSE Files:').grid(row=5, sticky='W', pady=2, padx=5)
		ttk.Label(text='2XCRM Contrast:').grid(row=6, sticky='W', pady=2, padx=5)
		ttk.Label(text='2XCRM PD:').grid(row=7, sticky='W', pady=2, padx=5)
		ttk.Label(text='Premade Mesh File:').grid(row=8, sticky='W', pady=(2, 1), padx=5)
		
		# 	Create entry objects
		sa_file_entry = ttk.Entry(width=80, textvariable=sa_filename)
		la_file_entry = ttk.Entry(width=80, textvariable=la_filename)
		lge_file_entry = ttk.Entry(width=80, textvariable=lge_filename)
		la_lge_file_entry = ttk.Entry(width=80, textvariable=la_lge_filenames)
		dense_file_entry = ttk.Entry(width=80, textvariable=dense_filenames)
		premade_mesh_entry = ttk.Entry(width=80, textvariable=premade_mesh_filename)
		twoxcrm_contrast_file_entry = ttk.Entry(width=80, textvariable=twoxcrm_contrast_filename)
		twoxcrm_pd_file_entry = ttk.Entry(width=80, textvariable=twoxcrm_pd_filename)
		# 	Place entry objects
		sa_file_entry.grid(row=1, column=1, columnspan=5, pady=2)
		la_file_entry.grid(row=2, column=1, columnspan=5, pady=2)
		lge_file_entry.grid(row=3, column=1, columnspan=5, pady=2)
		la_lge_file_entry.grid(row=4, column=1, columnspan=5, pady=2)
		dense_file_entry.grid(row=5, column=1, columnspan=5, pady=2)
		twoxcrm_contrast_file_entry.grid(row=6, column=1, columnspan=5, pady=2)
		twoxcrm_pd_file_entry.grid(row=7, column=1, columnspan=5, pady=2)
		premade_mesh_entry.grid(row=8, column=1, columnspan=5, pady=(2, 1))
		
		#	Place "Browse" buttons
		ttk.Button(text='Browse', command= lambda: self.openFileBrowser(sa_file_entry)).grid(row=1, column=6, pady=2)
		ttk.Button(text='Browse', command= lambda: self.openFileBrowser(la_file_entry)).grid(row=2, column=6, pady=2)
		ttk.Button(text='Browse', command= lambda: self.openFileBrowser(lge_file_entry)).grid(row=3, column=6, pady=2)
		ttk.Button(text='Browse', command= lambda: self.openFileBrowser(la_lge_file_entry, multi='True')).grid(row=4, column=6, pady=2)
		ttk.Button(text='Browse', command= lambda: self.openFileBrowser(dense_file_entry, multi='True')).grid(row=5, column=6, pady=2)
		ttk.Button(text='Browse', command= lambda: self.openFileBrowser(twoxcrm_contrast_file_entry)).grid(row=6, column=6, pady=2)
		ttk.Button(text='Browse', command= lambda: self.openFileBrowser(twoxcrm_pd_file_entry)).grid(row=7, column=6, pady=2)
		ttk.Button(text='Browse', command= lambda: self.openFileBrowser(premade_mesh_entry)).grid(row=8, column=6, pady=(2, 1))
		
		# Model Options
		#	Place labels
		ttk.Label(text='Primary Cine Timepoint:').grid(row=1, column=7, sticky='W')
		#	Create options comboboxes
		self.cine_timepoint_cbox = ttk.Combobox(state='disabled', width=5)
		self.cine_timepoint_cbox.bind('<<ComboboxSelected>>', lambda _ : self.cineTimeChanged())
		self.cine_timepoint_cbox.grid(row=1, column=8)
		#	Buttons to generate models
		#ttk.Button(text='Draw 2XCRM ROI', command= lambda: self.set2XCRM(twoxcrm_pd_filename)).grid(row=7, column=7, columnspan=2)
		ttk.Button(text='Generate MRI Model', command= lambda: self.createMRIModel(sa_filename, la_filename, lge_filename, la_lge_filenames, dense_filenames, twoxcrm_pd_filename, twoxcrm_contrast_filename, conf_import_dir)).grid(row=2, column=7, columnspan=2)
		
		# Confocal Model Options
		#	Place labels
		confocal_label = ttk.Label(text='Confocal Model Options')
		confocal_label.grid(row=10, column=0, columnspan=9, pady=(1, 2))
		ttk.Label(text='Model Data:').grid(row=11, sticky='W', pady=2)
		ttk.Label(text='Export Directory:').grid(row=12, sticky='W', pady=(2, 1))
		ttk.Label(text='Subslice Data:').grid(row=14, sticky='W', pady=(1, 2))
		ttk.Label(text='Slice Position:').grid(row=14, column=2, pady=(1, 2))
		ttk.Label(text='Slice Width:').grid(row=14, column=4, pady=(1, 2))
		#ttk.Label(text='Stitching Directory:').grid(row=15, sticky='W', pady=(2, 1))
		#	Create entry objects
		self.slice_width_entry = ttk.Entry(width=15, textvariable=self.slice_width, state='disabled')
		self.slice_position_entry = ttk.Entry(width=15, textvariable=self.slice_position, state='disabled')
		conf_import_entry = ttk.Entry(width=80, textvariable=conf_import_dir)
		conf_export_entry = ttk.Entry(width=80, textvariable=conf_export_dir)
		#	Place slice-selection combobox
		self.slice_combobox = ttk.Combobox(state='disabled')
		self.slice_combobox.bind('<<ComboboxSelected>>', lambda _ : self.subsliceChanged())
		self.slice_combobox.grid(row=14, column=1, sticky='W', pady=(1, 2))
		#	Place entry objects
		conf_import_entry.grid(row=11, column=1, columnspan=5, pady=2)
		conf_export_entry.grid(row=12, column=1, columnspan=5, pady=(2, 1))
		self.slice_position_entry.grid(row=14, column=3, pady=(1, 2))
		self.slice_width_entry.grid(row=14, column=5, pady=(1, 2))
		#stitching_dir_entry.grid(row=15, column=1, columnspan=5, pady=(2, 1))
		#	Place "Browse" buttons
		ttk.Button(text='Browse', command= lambda: self.openFileBrowser(conf_import_entry, multi='Dir')).grid(row=11, column=6, pady=2)
		ttk.Button(text='Browse', command= lambda: self.openFileBrowser(conf_export_entry, multi='Dir')).grid(row=12, column=6, pady=(2, 1))
		#ttk.Button(text='Browse', command= lambda: self.openFileBrowser(stitched_dir_entry, multi='Dir')).grid(row=14, column=6, pady=(1, 2))
		#ttk.Button(text='Browse', command= lambda: self.openFileBrowser(stitching_dir_entry, multi='Dir')).grid(row=15, column=6, pady=(2, 1))
		#	Buttons to generate models
		self.conf_contour_button = ttk.Button(text='Launch Contouring Window', command= lambda: self.launchConfocalContouringWindows(), state='disabled')
		self.conf_contour_button.grid(row=11, column=7, columnspan=2, pady=2)
		#ttk.Button(text='Create Confocal Model', command= lambda: self.setConfocalImportFiles(conf_import_entry)).grid(row=11, column=7, columnspan=2, pady=2)
		self.conf_export_button = ttk.Button(text='Export Data', command= lambda: self.exportConfData(), state='disabled')
		self.conf_export_button.grid(row=12, column=7, columnspan=2, pady=(2, 1))
		#self.stitched_image_button = ttk.Button(text='Import Stitched Images', command= lambda: self.setConfocalStitchedImages(stitched_dir_entry))
		#self.stitched_image_button.grid(row=14, column=7, columnspan=2, pady=(1, 2))
		#ttk.Button(text='Stitch Images', command= lambda: self.launchStitchingWindow(stitching_dir_entry)).grid(row=15, column=7, columnspan=2, pady=(2, 1))
		
		# Mesh Options / Creation
		#	Place labels
		mesh_label = ttk.Label(text='MRI Mesh Settings')
		mesh_label.grid(row=0, column=10, columnspan=2, pady=2)
		ttk.Label(text='Number of Rings:').grid(row=1, column=10, sticky='W', pady=2)
		ttk.Label(text='Elements per Ring:').grid(row=2, column=10, sticky='W', pady=2)
		ttk.Label(text='Elements through Wall:').grid(row=3, column=10, sticky='W', pady=2)
		ttk.Label(text='Mesh Type:').grid(row=4, column=10, sticky='W', pady=2)
		ttk.Label(text='Select conn matrix:').grid(row=5, column=10, sticky='W', pady=2)
		#	Create mesh option entry boxes
		num_rings_entry = ttk.Entry(width=10)
		elem_per_ring_entry = ttk.Entry(width=10)
		elem_thru_wall_entry = ttk.Entry(width=10)
		mesh_type_cbox = ttk.Combobox(values=['4x2', '4x4', '4x8'], state='readonly', width=10)
		self.conn_mat_cbox = ttk.Combobox(state='disabled', values=['hex', 'pent'], width=10)
		#	Place mesh option entry boxes
		num_rings_entry.grid(row=1, column=11, sticky='W', pady=2)
		elem_per_ring_entry.grid(row=2, column=11, sticky='W', pady=2)
		elem_thru_wall_entry.grid(row=3, column=11, sticky='W', pady=2)
		mesh_type_cbox.grid(row=4, column=11, sticky='W', pady=2)
		self.conn_mat_cbox.grid(row=5, column=11, sticky='W', pady=2)
		#	Mesh option entry boxes default text and input validation
		num_rings_entry.insert(0, '28')
		elem_per_ring_entry.insert(0, '48')
		elem_thru_wall_entry.insert(0, '5')
		num_rings_entry.configure(validate='key', validatecommand=(num_rings_entry.register(self.intValidate), '%P'))
		elem_per_ring_entry.configure(validate='key', validatecommand=(elem_per_ring_entry.register(self.intValidate), '%P'))
		elem_thru_wall_entry.configure(validate='key', validatecommand=(elem_thru_wall_entry.register(self.intValidate), '%P'))
		mesh_type_cbox.current(2)
		self.conn_mat_cbox.current(0)
		#	Create mesh option buttons
		self.premadeMeshButton = ttk.Button(text='Use Premade Mesh', state='enabled', command= lambda: self.createMRIMesh(num_rings_entry, elem_per_ring_entry, elem_thru_wall_entry, premade_mesh_file=premade_mesh_filename))
		self.meshButton = ttk.Button(text='Generate Model First', state='disabled', command= lambda: self.createMRIMesh(num_rings_entry, elem_per_ring_entry, elem_thru_wall_entry, mesh_type_cbox=mesh_type_cbox))
		self.scar_fe_button = ttk.Button(text='Identify scar nodes', state='disabled', command= lambda: self.scarElem())
		self.dense_fe_button = ttk.Button(text='Assign element displacements', state='disabled', command= lambda: self.denseElem())
		self.scar_dense_button = ttk.Button(text='Get scar region DENSE average', state='disabled', command= lambda: self.scarDense())
		#	Place mesh option buttons
		self.premadeMeshButton.grid(row=8, column=7, columnspan=2, pady=(2, 1))
		self.meshButton.grid(row=8, column=10, columnspan=2, pady=(2, 1))
		self.scar_fe_button.grid(row=10, column=10, columnspan=2, pady=(1, 2))
		self.dense_fe_button.grid(row=11, column=10, columnspan=2, pady=2)
		self.scar_dense_button.grid(row=12, column=10, columnspan=2, pady=(2, 1))
		
		# Confocal Mesh Options / Creation
		#	Place labels
		conf_mesh_label = ttk.Label(text='Confocal Mesh Settings')
		conf_mesh_label.grid(row=0, column=13, columnspan=2, pady=2)
		ttk.Label(text='Number of Rings:').grid(row=1, column=13, sticky='W', pady=2)
		ttk.Label(text='Elements per Ring:').grid(row=2, column=13, sticky='W', pady=2)
		ttk.Label(text='Elements through Wall:').grid(row=3, column=13, sticky='W', pady=2)
		ttk.Label(text='Mesh Type:').grid(row=4, column=13, sticky='W', pady=2)
		ttk.Label(text='Select conn matrix:').grid(row=5, column=13, sticky='W', pady=2)
		#	Create mesh option entry boxes
		conf_num_rings_entry = ttk.Entry(width=10)
		conf_elem_per_ring_entry = ttk.Entry(width=10)
		conf_elem_thru_wall_entry = ttk.Entry(width=10)
		conf_mesh_type_cbox = ttk.Combobox(values=['4x2', '4x4', '4x8'], state='readonly', width=10)
		self.conf_conn_mat_cbox = ttk.Combobox(state='disabled', values=['hex', 'pent'], width=10)
		#	Place mesh option entry boxes
		conf_num_rings_entry.grid(row=1, column=14, sticky='W', pady=2)
		conf_elem_per_ring_entry.grid(row=2, column=14, sticky='W', pady=2)
		conf_elem_thru_wall_entry.grid(row=3, column=14, sticky='W', pady=2)
		conf_mesh_type_cbox.grid(row=4, column=14, sticky='W', pady=2)
		self.conf_conn_mat_cbox.grid(row=5, column=14, sticky='W', pady=2)
		#	Mesh options entry boxes default text and input validation
		conf_num_rings_entry.insert(0, '28')
		conf_elem_per_ring_entry.insert(0, '48')
		conf_elem_thru_wall_entry.insert(0, '5')
		conf_num_rings_entry.configure(validate='key', validatecommand=(conf_num_rings_entry.register(self.intValidate), '%P'))
		conf_elem_per_ring_entry.configure(validate='key', validatecommand=(conf_elem_per_ring_entry.register(self.intValidate), '%P'))
		conf_elem_thru_wall_entry.configure(validate='key', validatecommand=(conf_elem_thru_wall_entry.register(self.intValidate), '%P'))
		conf_mesh_type_cbox.current(2)
		self.conf_conn_mat_cbox.current(0)
		#	Create mesh option buttons
		self.confMeshButton = ttk.Button(text='Generate Model First', state='disabled', command= lambda: self.createConfocalMesh(conf_num_rings_entry, conf_elem_per_ring_entry, conf_elem_thru_wall_entry, mesh_type_cbox=conf_mesh_type_cbox))
		self.confAlignButton = ttk.Button(text='Align Meshes', state='disabled', command= lambda: self.alignConfMRI(mesh_type_cbox=conf_mesh_type_cbox))
		#	Place mesh option buttons
		self.confMeshButton.grid(row=8, column=13, columnspan=2, pady=(2, 1))
		self.confAlignButton.grid(row=10, column=13, columnspan=2, pady=(1, 2))
		
		# FEBio File Creation
		#	Place labels
		postview_label = ttk.Label(text='Postview Options')
		postview_label.grid(row=17, column=0, columnspan=9, pady=(1, 2))
		ttk.Label(text='Postview filename:').grid(row=18, column=0, sticky='W', pady=2)
		ttk.Label(text='Postview installation:').grid(row=19, column=0, sticky='W', pady=2)
		#	Create entry objects
		self.postview_file_entry = ttk.Entry()
		self.postview_exe_entry = ttk.Entry()
		self.postview_file_entry.grid(row=18, column=1, columnspan=5, sticky='WE', pady=2)
		self.postview_exe_entry.grid(row=19, column=1, columnspan=5, sticky='WE', pady=2)
		self.postview_exe_entry.insert(0, self.postview_exe)
		#	Buttons to create and open files
		self.feb_file_button = ttk.Button(text='Generate FEBio File', state='disabled', command= lambda: self.genFebFile())
		if self.postview_exe == '':
			self.postview_open_button = ttk.Button(text='Launch PostView', state='disabled', command= lambda: self.openPostview())
		else:
			self.postview_open_button = ttk.Button(text='Launch PostView', state='enabled', command= lambda: self.openPostview())
		self.feb_file_button.grid(row=18, column=7, columnspan=2, pady=2)
		self.postview_open_button.grid(row=19, column=7, columnspan=2, pady=2)
		#	Create "Browse" button
		ttk.Button(text='Browse', command= lambda: self.openFileBrowser(self.postview_file_entry, multi='Feb')).grid(row=18, column=6, sticky='W', pady=2)
		ttk.Button(text='Browse', command= lambda: self.openFileBrowser(self.postview_exe_entry, multi='Exe')).grid(row=19, column=6, sticky='W', pady=2)
		
		# Plot Options - MRI
		#	Place labels
		plot_label = ttk.Label(text='Plotting')
		plot_label.grid(row=14, column=10, columnspan=2, pady=(2, 1))
		ttk.Label(text='Plot nodes:').grid(row=15, column=10, sticky='E', pady=(2, 1))
		ttk.Label(text='Plot scar:').grid(row=17, column=10, sticky='E', pady=(1, 2))
		ttk.Label(text='Plot DENSE:').grid(row=18, column=10, sticky='E', pady=2)
		ttk.Label(text='DENSE Timepoint:').grid(row=19, column=10, sticky='E', pady=2)
		#	DENSE Timepoint combobox
		self.dense_timepoint_cbox = ttk.Combobox(state='disabled', width=5)
		self.dense_timepoint_cbox.grid(row=19, column=11, sticky='W', pady=2)
		#	Options checkboxes
		self.scar_cbutton = ttk.Checkbutton(variable = self.scar_plot_bool, state='disabled')
		self.dense_cbutton = ttk.Checkbutton(variable = self.dense_plot_bool, state='disabled')
		self.nodes_cbutton = ttk.Checkbutton(variable = self.nodes_plot_bool, state='disabled')
		self.nodes_cbutton.grid(row=15, column=11, sticky='W', pady=(2, 1))
		self.scar_cbutton.grid(row=17, column=11, sticky='W', pady=(1, 2))
		self.dense_cbutton.grid(row=18, column=11, sticky='W', pady=2)
		#	Buttons to plot MRI Models or Meshes
		self.plot_mri_button = ttk.Button(text='Plot MRI Model', command= lambda: self.plotMRIModel(), state='disabled')
		self.plot_mesh_button = ttk.Button(text='Plot MRI Mesh', command= lambda: self.plotMRIMesh(), state='disabled')
		self.plot_mri_button.grid(row=20, column=10, sticky='W', pady=2)
		self.plot_mesh_button.grid(row=20, column=11, sticky='W', pady=2)
		
		# Plot Options - Confocal
		#   Place labels
		conf_plot_label = ttk.Label(text='Plotting')
		conf_plot_label.grid(row=14, column=13, columnspan=2, pady=(2, 1))
		ttk.Label(text='Plot nodes:').grid(row=15, column=13, sticky='E', pady=(2, 1))
		ttk.Label(text='Plot scar:').grid(row=17, column=13, sticky='E', pady=(1, 2))
		#   Options checkboxes
		self.conf_nodes_cbutton = ttk.Checkbutton(variable = self.conf_nodes_plot_bool, state='disabled')
		self.conf_scar_cbutton = ttk.Checkbutton(variable = self.conf_scar_plot_bool, state='disabled')
		self.conf_nodes_cbutton.grid(row=15, column=14, sticky='W', pady=(2, 1))
		self.conf_scar_cbutton.grid(row=17, column=14, sticky='W', pady=(1, 2))
		#   Buttons to plot confocal mesh
		self.plot_conf_mesh_button = ttk.Button(text='Plot Confocal Mesh', command= lambda: self.plotConfocalMesh(), state='disabled')
		self.plot_conf_model_button = ttk.Button(text='Plot Confocal Model', command= lambda: self.plotConfocalModel(), state='disabled')
		self.plot_conf_mesh_button.grid(row=20, column=14, pady=2)
		self.plot_conf_model_button.grid(row=20, column=13, pady=2)
		
		# Separators
		ttk.Separator(orient='vertical').grid(column=9, row=0, rowspan=21, sticky='NESW')
		ttk.Separator(orient='horizontal').grid(column=0, row=16, columnspan=9, sticky='EW')
		ttk.Separator(orient='horizontal').grid(column=0, row=9, columnspan=9, sticky='EW')
		ttk.Separator(orient='horizontal').grid(column=9, row=13, columnspan=6, sticky='WE')
		ttk.Separator(orient='vertical').grid(column=12, row=0, rowspan=21, sticky='NESW')
		#ttk.Separator(orient='horizontal').grid(column=13, row=11, columnspan=3, sticky='WE')
		
		# Set specific label fonts
		f = font.Font(mesh_label, mesh_label.cget('font'))
		f.configure(underline=True)
		mesh_label.configure(font=f)
		import_label.configure(font=f)
		confocal_label.configure(font=f)
		conf_mesh_label.configure(font=f)
		postview_label.configure(font=f)
		plot_label.configure(font=f)
		conf_plot_label.configure(font=f)

	def openFileBrowser(self, entry_box, multi='False'):
		"""Open a file browser window and assign the file name to the passed entry box.
		Allows various options for type of file browser to be launched.
		"""
		if multi == 'True':
			file_name = filedialog.askopenfilenames(title='Select Files')
		elif multi == 'Dir':
			file_name = filedialog.askdirectory(title='Select Folder')
		elif multi == 'Feb':
			file_name = filedialog.asksaveasfilename(title='Save File', filetypes=(('FEBio files','*.feb'), ('All files', '*')))
			if not (file_name.split('.')[-1] == 'feb'):
				file_name += '.feb'
		else:
			file_name = filedialog.askopenfilename(title='Select File')
		if not file_name == '':
			entry_box.delete(0, 'end')
			entry_box.insert(0, file_name)
		return(file_name)
		
	def createMRIModel(self, sa_filename, la_filename, lge_filename, la_lge_filenames, dense_filenames, twoxcrm_pd_filename, twoxcrm_data_filename, confocal_directory):
		"""Function run to instantiate MRI model based on input files.
		"""
		# Check that required files are present
		if sa_filename.get() == '' or la_filename.get() == '':
			if sa_filename.get() == '':
				messagebox.showinfo('File Error', 'Need Short-Axis file.')
			elif la_filename.get() == '':
				messagebox.showinfo('File Error', 'Need Long-Axis file.')
			return(False)
		
		# Parse DENSE Filenames
		if not(dense_filenames.get() == ''):
			dense_filenames_replaced = list(self.master.tk.splitlist(dense_filenames.get()))
		else:
			dense_filenames_replaced = dense_filenames.get()
			
		if not(la_lge_filenames.get() == ''):
			la_lge_filenames_replaced = list(self.master.tk.splitlist(la_lge_filenames.get()))
		else:
			la_lge_filenames_replaced = la_lge_filenames.get()
			
		if (twoxcrm_pd_filename.get() == '') ^ (twoxcrm_data_filename.get() == ''):
			messagebox.showinfo('2XCRM Error', 'Need both PD and Contrast Files for 2XCRM Analysis. Data will be ignored.')
			twoxcrm_pd_filename_replaced = ''
			twoxcrm_data_filename_replaced = ''
		else:
			twoxcrm_pd_filename_replaced = twoxcrm_pd_filename.get()
			twoxcrm_data_filename_replaced = twoxcrm_data_filename.get()
		
		# Instantiate MRI model object and import cine stack (at default timepoint)
		self.mri_model = mrimodel.MRIModel(sa_filename.get(), la_filename.get(), sa_scar_file=lge_filename.get(), la_scar_files=la_lge_filenames_replaced, dense_file=dense_filenames_replaced, twoxcrm_pd_file=twoxcrm_pd_filename_replaced, twoxcrm_data_file=twoxcrm_data_filename_replaced, confocal_directory=confocal_directory.get())
		self.mri_model.importCine(timepoint=0)
		
		# Import LGE, if included, and generate full alignment array
		if self.mri_model.scar:
			self.scar_cbutton.configure(state='normal')
			self.mri_model.importLGE()
			if not(la_lge_filenames_replaced == ''):
				self.mri_model.importScarLA()
			self.mri_model.convertDataProlate()
			interp_scar = self.mri_model.alignScar()
		else:
			# Only occurs if scar is removed from MRI model on later instantiation
			self.scar_cbutton.configure(state='disabled')
			self.scar_fe_button.configure(state='disabled')
		
		# Import DENSE, if included
		if self.mri_model.dense:
			self.dense_cbutton.configure(state='normal')
			self.mri_model.importDense()
		else:
			# Only occurs if DENSE is removed from MRI model on later instantiation
			self.dense_cbutton.configure(state='disabled')
			self.dense_timepoint_cbox.configure(values=[], state='disabled')
			self.dense_fe_button.configure(state='disabled')
		
		if self.mri_model.confocal:
			self.mri_model.importConfocal(compressed=0.1)
			self.slice_combobox.configure(values=[os.path.basename(confocal_slice.data_folder) for confocal_slice in self.mri_model.confocal_model.confocal_slices], state='readyonly')
			self.conf_contour_button.configure(state='enabled')
			self.conf_export_button.configure(state='enabled')
			self.slice_position_entry.configure(state='enabled')
			self.slice_width_entry.configure(state='enabled')
			self.slice_combobox.current(0)
			self.subsliceChanged()
			self.slice_position.trace_add('write', lambda name, index, mode, dv=self.slice_position: self.positionValidate(dv))
			self.slice_width.trace_add('write', lambda name, index, mode, dv=self.slice_width: self.widthValidate(dv))
		print('Confocal Imported')
		self.mri_model.convertDataProlate()
		if self.mri_model.dense:
			self.mri_model.alignDense(cine_timepoint=0)
			self.dense_timepoint_cbox.configure(values=list(range(len(self.mri_model.circumferential_strain))), state='readonly')
			self.dense_timepoint_cbox.current(0)
		
		if self.mri_model.twoxcrm:
			self.mri_model.import2XCRM()
		
		if self.mri_model.confocal and self.mri_model.confocal_model.data_imported:
			self.mri_model.alignConfocal(cine_timepoint=0)
		
		# Update GUI elements
		#self.cine_timepoint_cbox.configure(values=list(range(len(self.mri_model.cine_endo))), state='readonly')
		self.cine_timepoint_cbox.configure(values=self.mri_model.cine_timepts, state='readonly')
		self.cine_timepoint_cbox.current(0)
		if not self.mri_mesh:
			self.meshButton.configure(state='normal', text='Generate MRI Mesh')
		else:
			self.mri_mesh.assignInsertionPts(self.mri_model.cine_apex_pt, self.mri_model.cine_basal_pt, self.mri_model.cine_septal_pts)
			if self.mri_model.scar:
				self.scar_fe_button.configure(state='enabled')
			if self.mri_model.dense:
				self.dense_fe_button.configure(state='enabled')
		
	def createMRIMesh(self, num_rings_entry, elem_per_ring_entry, elem_thru_wall_entry, mesh_type_cbox=False, premade_mesh_file=False):
		"""Function to generate base-level mesh from MRI model object
		"""
		# Pull variables from GUI entry fields
		if not (num_rings_entry.get() == '' or elem_per_ring_entry.get() == '' or elem_thru_wall_entry.get() == ''):
			num_rings = int(num_rings_entry.get())
			elem_per_ring = int(elem_per_ring_entry.get())
			elem_in_wall = int(elem_thru_wall_entry.get())
		else:
			messagebox.showinfo('Mesh Settings', 'Mesh option left blank. Correct and try again.')
			return(False)
			
		if mesh_type_cbox:
			time_point = int(self.cine_timepoint_cbox.current())
		
			# Create base mesh
			self.mri_mesh = mesh.Mesh(num_rings, elem_per_ring, elem_in_wall)
		
			# Fit mesh to MRI model data
			self.mri_mesh.fitContours(self.mri_model.cine_endo_rotate[time_point][:, :3], self.mri_model.cine_epi_rotate[time_point][:, :3], self.mri_model.cine_apex_pt, self.mri_model.cine_basal_pt, self.mri_model.cine_septal_pts, mesh_type_cbox.get())
			self.mri_mesh.feMeshRender()
			self.mri_mesh.nodeNum(self.mri_mesh.meshCart[0], self.mri_mesh.meshCart[1], self.mri_mesh.meshCart[2])
			self.mri_mesh.getElemConMatrix()
			# Update GUI elements as needed
			self.plot_mri_button.configure(state='normal')
			self.plot_mesh_button.configure(state='normal')
			self.feb_file_button.configure(state='normal')
			self.nodes_cbutton.configure(state='normal')
			self.conn_mat_cbox.configure(state='readonly')
			if self.mri_model.scar:
				self.scar_fe_button.configure(state='normal')
			else:
				self.scar_fe_button.configure(state='disabled')
			if self.mri_model.dense:
				self.dense_fe_button.configure(state='normal')
			else:
				self.dense_fe_button.configure(state='disabled')
		elif premade_mesh_file:
			self.mri_mesh = mesh.Mesh(num_rings, elem_per_ring, elem_in_wall)
			import_success = self.mri_mesh.importPremadeMesh(premade_mesh_file.get())
			# Update GUI elements as needed
			if import_success:
				self.plot_mri_button.configure(state='normal')
				self.plot_mesh_button.configure(state='normal')
				self.feb_file_button.configure(state='normal')
				self.nodes_cbutton.configure(state='normal')
				self.meshButton.configure(state='disabled', text='Using Premade Mesh')
				if self.mri_model:
					self.mri_mesh.assignInsertionPts(self.mri_model.cine_apex_pt, self.mri_model.cine_basal_pt, self.mri_model.cine_septal_pts)
					if self.mri_model.scar:
						self.scar_fe_button.configure(state='normal')
					else:
						self.scar_fe_button.configure(state='disabled')
					if self.mri_model.dense:
						self.dense_fe_button.configure(state='normal')
					else:
						self.dense_fe_button.configure(state='disabled')
			else:
				self.mri_mesh = None
				messagebox.showwarning('No Mesh File', 'Mesh file not found.')
	
	def launchConfocalContouringWindows(self):
		'''Creates a window to select a confocal slice for contour drawing.
		'''
		if not self.mri_model.confocal:
			messagebox.showinfo('No Confocal', 'Please select a Confocal Model folder.')
			return(False)
		slice_folders = [os.path.basename(confocal_slice.data_folder) for confocal_slice in self.mri_model.confocal_model.confocal_slices]
		slice_positions = self.mri_model.confocal_model.slice_positions
		slice_widths = self.mri_model.confocal_model.slice_widths
		self._createStitchedProcWindow(slice_folders, slice_positions, slice_widths)
		
	def launchStitchingWindow(self, confocal_dir_entry):
		"""Set up a new confocalModel object of stitched images.
		
		Models contain ConfocalSlice objects, which are defined within the confocalModel file.
		"""
		# Establish the directory of interest in the model.
		confocal_dir = confocal_dir_entry.get()
		if confocal_dir == '':
			messagebox.showinfo('No Directory', 'Please select a directory for confocal images.')
			return(False)
		# Instantiate a new model object
		if not hasattr(self, 'confocal_model'):
			self.confocal_model = confocalmodel.ConfocalModel()
			
		def getBottomDirs(top_dir):
			# Local function to find every terminal directory below the selected directory that contains tif(f) files.
			sub_dir_isdir = [os.path.isdir(top_dir + '/' + sub_dir) for sub_dir in os.listdir(top_dir)]
			if any(sub_dir_isdir):
				# If not a terminal directory, recurse for all subdirectories.
				sub_dirs = [getBottomDirs(sub_dir) for sub_dir in natsorted([(top_dir + '/' + sub_dir) for sub_dir in os.listdir(top_dir) if os.path.isdir(top_dir + '/' + sub_dir)], alg=ns.IC)]
				# Flatten any lists that may have been returned.
				if any([isinstance(sub_dir, list) for sub_dir in sub_dirs]):
					bottom_dirs = []
					for sub_dir in sub_dirs:
						bottom_dirs += sub_dir
					# Filtering is done in another case. It is unnecessary here.
					return(bottom_dirs)
				else:
					# Filter out any "None" values from the subdirectories.
					return(list(filter(None, sub_dirs)))
			else:
				# If the directory is terminal, determine if it contains tif(f) files.
				tif_list = natsorted(glob.glob(os.path.join(top_dir, '*.tif')).copy(), alg=ns.IC)
				tif_list += natsorted(glob.glob(os.path.join(top_dir, '*.tiff')).copy(), alg=ns.IC)
				# If tif(f) files are present, return the directory as a directory of interest. Otherwise, return None, so filtering may be used.
				if len(tif_list):
					return(top_dir)
				else:
					return(None)
		
		def getModelDirs(bottom_dirs):
			# Local function to get the unique parents directories for each terminal directory containing tif(f) files.
			model_dirs = []
			for bottom_dir in bottom_dirs:
				# Get parent directory for each terminal tif(f) directory, check for uniqueness.
				upper_dir = os.path.dirname(bottom_dir)
				if not upper_dir in model_dirs:
					model_dirs.append(upper_dir)
			return(model_dirs)
		
		# Gather all directory at any point below this one that acts as a slice directory.		
		bottom_dirs = getBottomDirs(confocal_dir)
		model_dirs = getModelDirs(bottom_dirs)
		# Generate a new confocal model for each model directory identified.
		self.confocal_model_list = []
		self.confocal_slice_selections_test = {}
		stitch_slices_list = []
		sub_slices_list = []
		channels_list = []
		for model_ind, model_dir in enumerate(model_dirs):
			# For each model directory, instantiate a new model.
			self.confocal_model_list.append(confocalmodel.ConfocalModel())
			self.confocal_model_list[model_ind].setTopStitchingDir(model_dir)
			cur_stitch_slices = self.confocal_model_list[model_ind].slice_names
			cur_sub_slices = [None]*len(cur_stitch_slices)
			# Get the z-stacks from each model.
			for slice_num, cur_slice in enumerate(cur_stitch_slices):
				cur_sub_slices[slice_num] = self.confocal_model_list[model_ind].getSubsliceList(slice_num)
			# Get the channels from each model.
			cur_channels = [None]*len(cur_stitch_slices)
			for slice_num, cur_slice in enumerate(cur_stitch_slices):
				cur_channels[slice_num] = self.confocal_model_list[model_ind].getChannelList(slice_num)
			stitch_slices_list.append(cur_stitch_slices)
			sub_slices_list.append(cur_sub_slices)
			channels_list.append(cur_channels)
		self._createSubsliceWindow(stitch_slices_list, sub_slices_list, channels_list, self.confocal_model_list)
	
	def cineTimeChanged(self):
		"""Function to respond to timepoint adjustments in the base cine mesh / model
		"""
		# Insure timepoint is an integer (should always succeed, in place for possible errors)
		try:
			new_timepoint = int(self.cine_timepoint_cbox.current())
		except:
			return(False)
		# Import the cine model at the selected timepoint (updates landmarks)
		self.mri_model.importCine(timepoint = new_timepoint)
		# If necessary, align DENSE to new cine timepoint (most important aspect)
		if self.mri_model.dense:
			self.mri_model.alignDense(cine_timepoint = new_timepoint)
	
	def subsliceChanged(self):
		subslice_ind = self.slice_combobox.current()
		self.slice_position.set(self.mri_model.confocal_model.slice_positions[subslice_ind])
		self.slice_width.set(self.mri_model.confocal_model.confocal_slices[subslice_ind].slice_width)
		#self.slice_position_entry.insert(self.slice_position)
		#self.slice_width_entry.insert(self.slice_position)
		return(True)
	
	def exportConfData(self):
		self.mri_model.confocal_model.exportCOMData()
		self.mri_model.confocal_model.exportSlices()
		return(True)
	
	def positionValidate(self, dv):
		try:
			dv_float = float(dv.get())
			self.mri_model.confocal_model.slice_positions[self.slice_combobox.current()] = dv_float
			return(True)
		except:
			if len(str(dv.get())) == 0:
				return(True)
			self.slice_position.set(self.mri_model.confocal_model.slice_positions[self.slice_combobox.current()])
			return(False)
	
	def widthValidate(self, dv):
		try:
			dv_float = float(dv.get())
			self.mri_model.confocal_model.confocal_slices[self.slice_combobox.current()].slice_width = dv_float
			return(True)
		except:
			if len(str(dv.get())) == 0:
				return(True)
			self.slice_width.set(self.mri_model.confocal_model.confocal_slices[self.slice_combobox.current()].slice_width)
			return(False)
	
	def plotMRIModel(self):
		"""Plots MRI Model based on raw data (slice contours, scar traces, etc.)
		"""
		# Pull timepoint from timepoint selection combobox
		time_point = int(self.cine_timepoint_cbox.current())
		# Plot overall cine segmentation data
		mri_axes = displayhelper.segmentRender(self.mri_model.cine_endo_rotate[time_point], self.mri_model.cine_epi_rotate[time_point], self.mri_model.cine_apex_pt, self.mri_model.cine_basal_pt, self.mri_model.cine_septal_pts, self.mri_mesh.origin, self.mri_mesh.transform)
		# If desired, plot scar data
		if self.scar_plot_bool.get() and self.mri_model.scar:
			mri_axes = displayhelper.displayScarTrace(self.mri_model.interp_scar_trace, self.mri_mesh.origin, self.mri_mesh.transform, ax=mri_axes)
			if hasattr(self.mri_model, 'interp_scar_la_trace'):
				mri_axes = displayhelper.displayScarTrace(self.mri_model.interp_scar_la_trace, self.mri_mesh.origin, self.mri_mesh.transform, ax=mri_axes)
		# If desired, plot DENSE data
		if self.dense_plot_bool.get() and self.mri_model.dense:
			mri_axes = displayhelper.displayDensePts(self.mri_model.dense_aligned_pts, self.mri_mesh.origin, self.mri_mesh.transform, dense_displacement_all=False, dense_plot_quiver=1, timepoint=int(self.dense_timepoint_cbox.get()), ax=mri_axes)
		if self.mri_model.confocal:
			conf_epi_polar = np.column_stack(tuple(mathhelper.cart2pol(self.mri_model.conf_aligned_epi[:, 1], self.mri_model.conf_aligned_epi[:, 2])))
			conf_pts_polar = np.column_stack(tuple(mathhelper.cart2pol(self.mri_model.conf_aligned_pts[:, 1], self.mri_model.conf_aligned_pts[:, 2])))
			#mri_axes = displayhelper.displayDensePts(self.mri_model.conf_aligned_pts, self.mri_mesh.origin, self.mri_mesh.transform, dense_displacement_all=False, dense_plot_quiver=1, ax=mri_axes)
			mri_axes = displayhelper.displayDensePts(self.mri_model.conf_pinpts_shifted, self.mri_mesh.origin, self.mri_mesh.transform, dense_displacement_all=False, dense_plot_quiver=1, ax=mri_axes)
			mri_axes = displayhelper.displayDensePts(self.mri_model.conf_aligned_epi, self.mri_mesh.origin, self.mri_mesh.transform, dense_displacement_all=False, dense_plot_quiver=1, ax=mri_axes, set_color='green')
			mri_axes = displayhelper.displayDensePts(self.mri_model.conf_aligned_endo, self.mri_mesh.origin, self.mri_mesh.transform, dense_displacement_all=False, dense_plot_quiver=1, ax=mri_axes, set_color='blue')
	
	def plotConfocalModel(self):
		conf_axes = displayhelper.segmentRender(self.confocal_model.full_endo_contour, self.confocal_model.full_epi_contour, self.confocal_model.apex_pt, self.confocal_model.basal_pt, self.confocal_model.septal_pinpoints, self.confocal_mesh.origin, self.confocal_mesh.transform)
	
	def plotMRIMesh(self):
		"""Plots the mesh data as a surface plot, with display options
		"""
		# Plot surface contours of endocardium and epicardium
		mesh_axes = displayhelper.surfaceRender(self.mri_mesh.endo_node_matrix, self.mri_mesh.focus)
		mesh_axes = displayhelper.surfaceRender(self.mri_mesh.epi_node_matrix, self.mri_mesh.focus, ax=mesh_axes)
		# Display node positions, if selected
		plot_nodes = np.array([4845, 4845])
		mesh_axes = displayhelper.nodeRender(self.mri_mesh.nodes[plot_nodes, :], ax=mesh_axes, color='red')
		if self.nodes_plot_bool.get():
			mesh_axes = displayhelper.nodeRender(self.mri_mesh.nodes, ax=mesh_axes)
		# Display scar locations, if available and desired
		if self.scar_plot_bool.get() and self.mri_mesh.nodes_in_scar.size:
			mesh_axes = displayhelper.nodeRender(self.mri_mesh.nodes[self.mri_mesh.nodes_in_scar, :], ax=mesh_axes)
			#mesh_axes = displayhelper.nodeRender(self.mri_mesh.nodes[self.mri_mesh.border_nodes, :], ax=mesh_axes, color='#00ff00')
		elif self.scar_plot_bool.get() and not self.mri_mesh.nodes_in_scar.size:
			# Warn if scar box selected, but elements unidentified.
			messagebox.showinfo('Warning', 'Identify scar nodes before plotting to view.')
		if self.dense_plot_bool.get() and self.mri_mesh.elems_in_dense.size:
			node_elems = [np.where([node in self.mri_mesh.hex[cur_elem, :] for cur_elem in range(self.mri_mesh.hex.shape[0])])[0] for node in self.mri_mesh.nodes_in_dense]
			node_strains = [np.nanmin([self.mri_mesh.getElemData(cur_node_elems, data_out='dense', format='Average', timepoint=cur_time)[1] for cur_time in range(self.mri_mesh.dense_radial_strain.shape[1])]) for cur_node_elems in node_elems]
			mesh_axes = displayhelper.nodeRender(self.mri_mesh.nodes[self.mri_mesh.nodes_in_dense, :], ax=mesh_axes, colormap_name='hsl', values=node_strains)
	
	def scarElem(self):
		"""Requests mesh to process which elements are in scar
		"""
		time_point = int(self.cine_timepoint_cbox.current())
		self.mri_model.convertDataProlate(self.mri_mesh.focus)
		self.mri_mesh.rotateNodesProlate()
		#self.mri_model.alignScar()
		self.mri_mesh.interpScarData(self.mri_model.interp_scar, trans_smooth=1, depth_smooth=0.5)
		if not self.scar_assign:
			self.scar_assign = True
		if self.dense_assign and self.scar_assign:
			self.scar_dense_button.configure(state='normal')
	
	def denseElem(self):
		"""Requests mesh to assign DENSE information to all applicable elements
		"""
		time_point = int(self.cine_timepoint_cbox.current())
		self.mri_mesh.assignDenseElems(self.mri_model.dense_aligned_pts, self.mri_model.dense_slices, self.mri_model.dense_aligned_displacement, self.mri_model.radial_strain, self.mri_model.circumferential_strain)
		if not self.dense_assign:
			self.dense_assign = True
		if self.dense_assign and self.scar_assign:
			self.scar_dense_button.configure(state='normal')
	
	def scarDense(self):
		"""Function designed to calculate regional DENSE data based on model / mesh scar extent.
		
		Calculates DENSE data in the identified scar and non-scar (remote) regions and reports / plots those values.
		"""
		scar_average_dense = [None]*self.mri_mesh.dense_radial_strain.shape[1]
		border_average_dense = [None]*self.mri_mesh.dense_radial_strain.shape[1]
		remote_average_dense = [None]*self.mri_mesh.dense_radial_strain.shape[1]
		for time_point in range(self.mri_mesh.dense_radial_strain.shape[1]):
			scar_average_dense[time_point] = self.mri_mesh.getElemData(self.mri_mesh.elems_in_scar, 'dense', format="Average", timepoint=time_point).tolist()
			border_average_dense[time_point] = self.mri_mesh.getElemData(self.mri_mesh.border_elems, 'dense', format="Average", timepoint=time_point).tolist()
			remote_average_dense[time_point] = self.mri_mesh.getElemData(self.mri_mesh.elems_out_scar, 'dense', format="Average", timepoint=time_point).tolist()
		scar_radial = [scar_dense[0] for scar_dense in scar_average_dense]
		scar_circ = [scar_dense[1] for scar_dense in scar_average_dense]
		border_radial = [border_dense[0] for border_dense in border_average_dense]
		border_circ = [border_dense[1] for border_dense in border_average_dense]
		remote_radial = [remote_dense[0] for remote_dense in remote_average_dense]
		remote_circ = [remote_dense[1] for remote_dense in remote_average_dense]
		#plot_fig, radial_ax = displayhelper.plotListData(scar_radial, color='r', title='Radial Strain')
		plot_fig, circ_ax = displayhelper.plotListData(scar_circ, color='r')
		displayhelper.plotListData(remote_circ, ax=circ_ax, fig=plot_fig, color='b')
		displayhelper.plotListData(border_circ, ax=circ_ax, fig=plot_fig, color='g')
		radial_plot_fig, radial_ax = displayhelper.plotListData(scar_radial, color='r')
		displayhelper.plotListData(border_radial, ax=radial_ax, fig=radial_plot_fig, color='g')
		displayhelper.plotListData(remote_radial, ax=radial_ax, fig=radial_plot_fig, color='b')
		#_, circ_ax = displayhelper.plotListData(scar_circ, fig=plot_fig, color='r', title='Circumferential Strain')
		#displayhelper.plotListData(remote_circ, ax=circ_ax, color='b')
	
	def genFebFile(self):
		"""Generate FEBio file in indicated location
		"""
		# Perform filename checks to ensure proper filename entered.
		feb_file_name = self.postview_file_entry.get()
		if feb_file_name == '':
			messagebox.showinfo('Filename Error', 'File name is not indicated.')
			return(False)
		elif not (feb_file_name.split('.')[-1] == 'feb'):
			messagebox.showinfo('Filename Error', 'File must be an FEBio file (*.feb).')
			return(False)
		# Generate FEBio file through Mesh function
		self.mri_mesh.generateFEFile(feb_file_name, self.conn_mat_cbox.get())
		# Update GUI Elements
		self.postview_open_button.configure(state='normal')
		
	def openPostview(self):
		"""Launch a PostView instance pointed at the FEBio File
		
		Note for this function to operate as expected, the function displayhelper/displayMeshPostview must point to the FEBio PostView executable on your machine.
		"""
		# Pull FEBio file name
		feb_file_name = self.postview_file_entry.get()
		feb_executable = self.postview_exe_entry.get()
		# Check that file is an accessible file
		try:
			open(feb_file_name)
		except:
			messagebox.showinfo('File Warning', 'FEBio File not found. Check file name and try again.')
			return(False)
		# Ensure that file is an FEBio file
		if not (feb_file_name.split('.')[-1] == 'feb'):
			messagebox.showinfo('File Warning', 'File selected is not an FEBio file. Check file name and try again.')
			return(False)
		# Request PostView Launch
		try:
			open(feb_executable)
			displayhelper.displayMeshPostview(feb_file_name, feb_executable)
		except FileNotFoundError:
			messagebox.showinfo('Executable Warning', 'PostView installation not found. Please identify executable location.')
			return(False)
		except OSError:
			messagebox.showinfo('Executable Warning', 'Invalid file selected. Please identify correct executable location.')
			return(False)

	def intValidate(self, new_value):
		"""Simple validation function to ensure an entry receives only int-able inputs or null
		"""
		# Accept empty entry box to allow clearing the box
		if new_value == '':
			return(True)
		# Attempt integer conversion. If possible, accept new input
		try:
			int(new_value)
			return(True)
		except:
			return(False)
			
	def floatValidate(self, new_value):
		"""Simple validation function to ensure an entry receives only float-able inputs or null
		"""
		# Accept empty entry box to allow clearing the box.
		if new_value == '':
			return(True)
		# Attempt float conversion. If possible, accept new input.
		try:
			float(new_value)
			return(True)
		except:
			return(False)
	
	def setConfocalStitchedImages(self, stitched_dir_entry):
		confocal_stitched_dir = stitched_dir_entry.get()
		if confocal_stitched_dir == '':
			messagebox.showinfo('No Directory', 'Please select a directory for images.')
			return(False)
		if not hasattr(self, 'confocal_model'):
			self.confocal_model = confocalmodel.ConfocalModel()
		if not hasattr(self.confocal_model, 'stitched_files'):
			self.confocal_model.setStitchedDir(confocal_stitched_dir)
			self.stitched_image_button.configure(text='Open Stitched Images')
		self._createStitchedProcWindow()
		
	def setConfocalImportFiles(self, data_file_entry):
		data_file_dir = data_file_entry.get()
		if data_file_dir == '':
			messagebox.showinfo('No Directory', 'Please select a directory for data files.')
			return(False)
		if not hasattr(self, 'confocal_model'):
			self.confocal_model = confocalmodel.ConfocalModel()
		self.confocal_model.setImportDirectory(data_file_dir)
		self.confocal_model.importModelData()
		self.confMeshButton.configure(state='normal', text='Generate Confocal Mesh')
	
	def setConfocalExportFiles(self, data_file_entry):
		data_file_dir = data_file_entry.get()
		if data_file_dir == '':
			messagebox.showinfo('No Directory', 'Please select a directory for data files.')
			return(False)
		if not hasattr(self, 'confocal_model'):
			messagebox.showinfo('No Model Data', 'No model data to export.')
			return(False)
		self.confocal_model.setExportDirectory(data_file_dir)
	
	def createConfocalMesh(self, num_rings_entry, elem_per_ring_entry, elem_thru_wall_entry, mesh_type_cbox=False):
		if not (num_rings_entry.get() == '' or elem_per_ring_entry.get() == '' or elem_thru_wall_entry.get() == ''):
			num_rings = int(num_rings_entry.get())
			elem_per_ring = int(elem_per_ring_entry.get())
			elem_in_wall = int(elem_thru_wall_entry.get())
		else:
			messagebox.showinfo('Mesh Settings', 'Mesh option left blank. Correct and try again.')
			return(False)
		
		self.confocal_model.prepDataForMesh()
		self.confocal_mesh = mesh.Mesh(num_rings, elem_per_ring, elem_in_wall)
		
		self.confocal_mesh.fitContours(self.confocal_model.full_endo_contour, self.confocal_model.full_epi_contour, self.confocal_model.apex_pt, self.confocal_model.basal_pt, self.confocal_model.septal_pinpoints[0], mesh_type_cbox.get())
		self.confocal_mesh.feMeshRender()
		self.confocal_mesh.nodeNum(self.confocal_mesh.meshCart[0], self.confocal_mesh.meshCart[1], self.confocal_mesh.meshCart[2])
		self.confocal_mesh.getElemConMatrix()
		self.conf_nodes_cbutton.configure(state='normal')
		self.plot_conf_model_button.configure(state='normal')
		self.plot_conf_mesh_button.configure(state='normal')
		self.confAlignButton.configure(state='normal')
	
	def alignConfMRI(self, mesh_type_cbox=False):
		time_point = int(self.cine_timepoint_cbox.current())
		conf_endo_nodes = self.confocal_mesh.endo_node_matrix
		conf_epi_nodes = self.confocal_mesh.epi_node_matrix
		print(self.confocal_mesh.origin)
		print(self.mri_mesh.origin)
		print(self.confocal_mesh.transform)
		print(self.mri_mesh.transform)
		self.confocal_mesh.fitContours(self.mri_model.cine_endo_rotate[time_point][:, :3], self.mri_model.cine_epi_rotate[time_point][:, :3], self.mri_model.cine_apex_pt, self.mri_model.cine_basal_pt, self.mri_model.cine_septal_pts, mesh_type_cbox.get(), self.confocal_mesh.endo_node_matrix, self.confocal_mesh.epi_node_matrix)
		#self.confocal_mesh.fitContours(self.confocal_model.full_endo_contour, self.confocal_model.full_epi_contour, self.confocal_model.apex_pt, self.confocal_model.basal_pt, self.confocal_model.septal_pinpoints[0], mesh_type_cbox.get(), self.mri_mesh.endo_node_matrix, self.mri_mesh.epi_node_matrix)
	
	def plotConfocalMesh(self):
		mesh_axes = displayhelper.surfaceRender(self.confocal_mesh.endo_node_matrix, self.confocal_mesh.focus)
		mesh_axes = displayhelper.surfaceRender(self.confocal_mesh.epi_node_matrix, self.confocal_mesh.focus, ax=mesh_axes)
		if self.conf_nodes_plot_bool.get():
			mesh_axes = displayhelper.nodeRender(self.confocal_mesh.nodes, ax=mesh_axes)
	
	def _createStitchedProcWindow(self, slice_folders, slice_positions, slice_widths):
		root = tk.Tk()
		screen_height = root.winfo_screenheight()
		root.update()
		root.destroy()
		self.stitched_proc_menu = tk.Toplevel(self.master)
		self.stitched_proc_menu.wm_title('Process Stitched Images')
		
		stitched_proc_canvas = tk.Canvas(self.stitched_proc_menu, borderwidth=0)
		stitched_proc_frame = tk.Frame(stitched_proc_canvas)
		stitched_proc_canvas.pack(side="left", fill="both", expand=True)
		stitched_proc_canvas.create_window((4,4), window=stitched_proc_frame, anchor="nw")
		
		stitched_image_options = slice_folders
		stitched_image_dict = dict()
		for image_ind, image_name in enumerate(stitched_image_options):
			stitched_image_dict[image_name] = image_ind
		
		def stitchedSliceChanged():
			slice_selected = self.mri_model.confocal_model.confocal_slices[stitched_image_dict[stitched_image_menu.get()]]
			num_slices = slice_selected.num_slices
			slice_options = list(range(num_slices))
			subslice_image_menu.configure(values=slice_options, state='readonly')
			subslice_image_menu.current(0)
		
		ttk.Label(stitched_proc_frame, text="Select Slice:").grid(column=0, row=1, sticky='W', padx=2, pady=2)
		stitched_image_menu = ttk.Combobox(stitched_proc_frame, values=stitched_image_options, state='readonly')
		stitched_image_menu.grid(column=1, row=1, sticky='W', padx=2, pady=2)
		stitched_image_menu.current(0)
		
		ttk.Label(stitched_proc_frame, text="Select Subslice:").grid(column=2, row=1, sticky='W', padx=2, pady=2)
		subslice_image_menu = ttk.Combobox(stitched_proc_frame, state='disabled', width=5)
		subslice_image_menu.grid(column=3, row=1, sticky='W', padx=2, pady=2)
		
		stitchedSliceChanged()
		stitched_image_menu.bind('<<ComboboxSelected>>', lambda _ : stitchedSliceChanged())
		
		stitched_image_button = ttk.Button(stitched_proc_frame, state='enabled', text='Open Image', command= lambda: self.__openStitchedImage(stitched_image_dict[stitched_image_menu.get()], subslice_image_menu, stitched_proc_canvas, stitched_proc_frame))
		#stitched_image_button = ttk.Button(stitched_proc_frame, state='enabled', text='Open Image', command= lambda: stitchedSliceChanged())
		stitched_image_button.grid(column=4, row=1, columnspan=2, padx=2, pady=2)
		
		stitched_proc_frame.update()
		stitched_proc_canvas.update()
		
		frame_height = np.min([int(screen_height * 5 / 8), stitched_proc_frame.winfo_height()])
		stitched_proc_canvas.config(width = stitched_proc_frame.winfo_width(), height=frame_height)
	
	def __openStitchedImage(self, slice_ind, subslice_menu, image_canvas, image_frame):
		# Set high-level variables and clear any conflicting bindings:
		endo_line_color = '#ff0000'
		epi_line_color = '#00ff00'
		pinpoint_line_color = '#0000ff'
		image_canvas.unbind("<Button-1>")
		image_canvas.unbind("<Button-3>")
		image_canvas.unbind("<Button-2>")
		image_canvas.unbind("<Control-z>")
		image_canvas.config(cursor="arrow")
		
		# Define functions needed for plotting
		# 	Function to add a new contour point to the models and displayed image
		def addContourPoint(event, contour_points, contour_name, subslice_ind=0, type='contour'):
			image_x_loc = event.x - 4
			image_y_loc = event.y - self.stitched_canvas_height
			if image_x_loc >= 0 and image_y_loc >= 0:
				true_contour_point = (image_x_loc/image_ratio, image_y_loc/image_ratio)
				if type == 'contour':
					contour_points.append((image_x_loc, image_y_loc))
					self.mri_model.confocal_model.confocal_slices[slice_ind].addContourPoint(contour_name, subslice_ind, true_contour_point)
					if contour_points[0] is None:
						del contour_points[0]
				elif type == 'pinpoints':
					if self.latest_pinpt == 0:
						self.latest_pinpt = 1
					else:
						self.latest_pinpt = 0
					contour_points[:, self.latest_pinpt] = (image_x_loc, image_y_loc)
					self.mri_model.confocal_model.confocal_slices[slice_ind].addSeptalPoint(true_contour_point, replace=self.latest_pinpt)
				__drawImageContours()
				image_canvas.update()
		
		# 	Function to complete the contour being plotted
		def completeContour(event, contour_points, contour_name, subslice_ind):
			contour_points.append(contour_points[0])
			self.mri_model.confocal_model.confocal_slices[slice_ind].closeContour(contour_name, subslice_ind)
			__drawImageContours()
			image_canvas.unbind("<Button-1>")
			image_canvas.unbind("<Button-3>")
			image_canvas.unbind("<Button-2>")
			image_canvas.unbind("<Control-z>")
			image_canvas.config(cursor="arrow")
			image_canvas.update()
		
		# 	Function to undo placement of the most recent contour point
		def removeContourPoint(contour_points, contour_name, subslice_ind=0, type='contour'):
			if type == 'contour':
				if len(contour_points) > 0:
					self.resized_subslice = image_subslice.resize(new_image_size, Image.ANTIALIAS).convert('RGB')
					self.conf_draw_image = ImageDraw.Draw(self.resized_subslice)
					del(contour_points[-1])
					self.mri_model.confocal_model.confocal_slices[slice_ind].deleteContourPoint(contour_name, subslice_ind)
			elif type == 'pinpoints':
				contour_points[:, self.latest_pinpt] = (0, 0)
				self.mri_model.confocal_model.confocal_slices[slice_ind].septal_pts[0, self.latest_pinpt] = 0
				self.mri_model.confocal_model.confocal_slices[slice_ind].septal_pts[1, self.latest_pinpt] = 0
				if self.latest_pinpt == 0:
					self.latest_pinpt = 1
				else:
					self.latest_pinpt = 0
			__drawImageContours()
			image_canvas.update()
		
		def unbindButtons():
			image_canvas.unbind("<Button-1>")
			image_canvas.unbind("<Button-3>")
			image_canvas.unbind("<Button-2>")
			image_canvas.unbind("<Control-z>")
			image_canvas.config(cursor="arrow")
		
		#	Function to display the updated contours onto the images
		def __drawImageContours():
			# Select contours based on known values
			endo_contour = self.endo_resized_contour if len(self.endo_resized_contour) != 0 else [None]
			endo_contour = [tuple(endo_contour_point) for endo_contour_point in endo_contour] if endo_contour[0] else []
			epi_contour = self.epi_resized_contour if len(self.epi_resized_contour) != 0 else [None]
			epi_contour = [tuple(epi_contour_point) for epi_contour_point in epi_contour] if epi_contour[0] else []
			pinpoint_placements = [tuple(self.pinpts_resized[:, pinpt_ind]) for pinpt_ind in range(self.pinpts_resized.shape[1])]
			if len(endo_contour) > 0:
				if len(endo_contour) == 1:
					self.conf_draw_image.point(endo_contour, fill=endo_line_color)
				else:
					self.conf_draw_image.line(endo_contour, fill=endo_line_color, width=1)
			if len(epi_contour) > 0:
				if len(epi_contour) == 1:
					self.conf_draw_image.point(epi_contour, fill=epi_line_color)
				else:
					self.conf_draw_image.line(epi_contour, fill=epi_line_color, width=1)
			if len(pinpoint_placements) > 0:
				for pinpoint_loc in pinpoint_placements:
					self.conf_draw_image.line([(pinpoint_loc[0]-3, pinpoint_loc[1]-3), (pinpoint_loc[0]+3, pinpoint_loc[1]+3)], fill=pinpoint_line_color)
					self.conf_draw_image.line([(pinpoint_loc[0]-3, pinpoint_loc[1]+3), (pinpoint_loc[0]+3, pinpoint_loc[1]-3)], fill=pinpoint_line_color)
			self.image_shown = ImageTk.PhotoImage(self.resized_subslice)
			self.displayed_image = image_canvas.create_image((4, self.stitched_canvas_height), anchor='nw', image=self.image_shown)
		
		# Get screen dimensions to set image bounds
		if not hasattr(self, 'stitched_canvas_height'):
			self.stitched_canvas_height = image_canvas.winfo_height()
		if not hasattr(self, 'stitched_canvas_width'):
			self.stitched_canvas_width = image_canvas.winfo_width()
		max_im_height = image_canvas.winfo_screenheight()*0.7 - self.stitched_canvas_height
		max_im_width = image_canvas.winfo_screenwidth()*0.95
		
		# Pull and resize image based on display values
		#test = self.confocal_model.slices[0].getStitchedSubslice(0)
		image_subslice = self.mri_model.confocal_model.confocal_slices[slice_ind].raw_images[0][int(subslice_menu.get())]
		height_ratio = max_im_height/image_subslice.shape[1]
		width_ratio = max_im_width/image_subslice.shape[0]
		image_ratio = height_ratio if height_ratio < width_ratio else width_ratio
		new_image_size = tuple([int(shape_val*image_ratio) for shape_val in image_subslice.shape])
		self.mri_model.confocal_model.confocal_slices[slice_ind].compressImages(compression_ratio=image_ratio)
		image_subslice = Image.fromarray(self.mri_model.confocal_model.confocal_slices[slice_ind].compressed_images[0][int(subslice_menu.get())].astype(int))
		self.resized_subslice = image_subslice.convert('RGB')
		
		# Set up displayed image and ImageDraw object
		self.image_shown = ImageTk.PhotoImage(self.resized_subslice)
		self.displayed_image = image_canvas.create_image((4,self.stitched_canvas_height), anchor='nw', image=self.image_shown)
		self.conf_draw_image = ImageDraw.Draw(self.resized_subslice)
		
		# Set up resized contours if natural contours already existed in the model (possibly from import)
		if len(self.mri_model.confocal_model.confocal_slices[slice_ind].endo_contour[int(subslice_menu.get())]) > 0:
			self.endo_resized_contour = [(contour_point[0]*image_ratio, contour_point[1]*image_ratio) for contour_point in self.mri_model.confocal_model.confocal_slices[slice_ind].endo_contour[int(subslice_menu.get())]]
		else:
			self.endo_resized_contour = []
		if len(self.mri_model.confocal_model.confocal_slices[slice_ind].epi_contour[int(subslice_menu.get())]) > 0:
			self.epi_resized_contour = [(contour_point[0]*image_ratio, contour_point[1]*image_ratio) for contour_point in self.mri_model.confocal_model.confocal_slices[slice_ind].epi_contour[int(subslice_menu.get())]]
		else:
			self.epi_resized_contour = []
		self.pinpts_resized = self.mri_model.confocal_model.confocal_slices[slice_ind].septal_pts * image_ratio
		if self.pinpts_resized[0, 0] == 0:
			self.latest_pinpt = 0
		else:
			self.latest_pinpt = 1
		
		# Draw any (resized) contours over the image that already exist in the model
		__drawImageContours()
		
		# Functions to initiate binding of mouse buttons to contour drawing (triggered by buttons on window)
		def drawEndo():
			image_canvas.focus_set()
			image_canvas.unbind("<Button-1>")
			image_canvas.unbind("<Button-3>")
			image_canvas.unbind("<Button-2>")
			image_canvas.unbind("<Control-z>")
			image_canvas.config(cursor="tcross")
			image_canvas.bind("<Button-1>", lambda event: addContourPoint(event, self.endo_resized_contour, 'endo', subslice_ind=int(subslice_menu.get())))
			image_canvas.bind("<Button-3>", lambda event: completeContour(event, self.endo_resized_contour, 'endo', subslice_ind=int(subslice_menu.get())))
			image_canvas.bind("<Button-2>", lambda event: removeContourPoint(self.endo_resized_contour, 'endo', subslice_ind=int(subslice_menu.get())))
			image_canvas.bind("<Control-z>", lambda event: removeContourPoint(self.endo_resized_contour, 'endo', subslice_ind=int(subslice_menu.get())))
			
		def drawEpi():
			image_canvas.focus_set()
			image_canvas.unbind("<Button-1>")
			image_canvas.unbind("<Button-3>")
			image_canvas.unbind("<Button-2>")
			image_canvas.unbind("<Control-z>")
			image_canvas.config(cursor="tcross")
			image_canvas.bind("<Button-1>", lambda event: addContourPoint(event, self.epi_resized_contour, 'epi', subslice_ind=int(subslice_menu.get())))
			image_canvas.bind("<Button-3>", lambda event: completeContour(event, self.epi_resized_contour, 'epi', subslice_ind=int(subslice_menu.get())))
			image_canvas.bind("<Button-2>", lambda event: removeContourPoint(self.epi_resized_contour, 'epi', subslice_ind=int(subslice_menu.get())))
			image_canvas.bind("<Control-z>", lambda event: removeContourPoint(self.epi_resized_contour, 'epi', subslice_ind=int(subslice_menu.get())))
		
		def drawPinpoints():
			image_canvas.focus_set()
			#image_canvas.focus_set()
			image_canvas.unbind("<Button-1>")
			image_canvas.unbind("<Button-3>")
			image_canvas.unbind("<Button-2>")
			image_canvas.unbind("<Control-z>")
			image_canvas.config(cursor="tcross")
			image_canvas.bind("<Button-1>", lambda event: addContourPoint(event, self.pinpts_resized, 'pinpts', type='pinpoints'))
			image_canvas.bind("<Button-3>", lambda event: unbindButtons())
			image_canvas.bind("<Button-2>", lambda event: removeContourPoint(self.pinpts_resized, 'pinpts', type='pinpoints'))
			image_canvas.bind("<Control-z>", lambda event: removeContourPoint(self.pinpts_resized, 'pinpts', type='pinpoints'))
			
		ttk.Button(image_frame, state='enabled', text='Draw Endo', command= lambda: drawEndo()).grid(column=6, row=1, columnspan=2, padx=2, pady=2)
		ttk.Button(image_frame, state='enabled', text='Draw Epi', command= lambda: drawEpi()).grid(column=8, row=1, columnspan=2, padx=2, pady=2)
		ttk.Button(image_frame, state='enabled', text='Add Pinpoints', command= lambda: drawPinpoints()).grid(column=10, row=1, columnspan=2, padx=2, pady=2)
		
		image_canvas.update()
		image_canvas.config(width=np.max([self.resized_subslice.width+4, self.stitched_canvas_width]), height=self.resized_subslice.height+self.stitched_canvas_height+4)
		
	def _createSubsliceWindow(self, conf_slices, slice_names, slice_positions, slice_widths):
		"""Create a window to select subslices and channels.
		"""
		# Calculate window height
		root = tk.Tk()
		screen_height = root.winfo_screenheight()
		root.destroy()
		
		self.slice_menu = tk.Toplevel(self.master)
		self.slice_menu.wm_title('Select Subslices and Channels')
		
		# Set up canvas to allow scrollable window
		slice_canvas = tk.Canvas(self.slice_menu, borderwidth=0)
		slice_frame = tk.Frame(slice_canvas)
		scroll_bar = tk.Scrollbar(self.slice_menu, orient="vertical", command=slice_canvas.yview)
		slice_canvas.configure(yscrollcommand=scroll_bar.set)
		
		scroll_bar.pack(side="right", fill="y")
		slice_canvas.pack(side="left", fill="both", expand=True)
		slice_canvas.create_window((4,4), window=slice_frame, anchor="nw")
		
		def onFrameConfigure(canvas):
			canvas.configure(scrollregion=canvas.bbox("all"))
		
		slice_canvas.bind_all("<MouseWheel>", lambda event: slice_canvas.yview_scroll(int(-1*(event.delta/40)), "units"))
		slice_frame.bind("<Configure>", lambda event, canvas=slice_canvas: onFrameConfigure(slice_canvas))
		
		# Set up lists to allow for multiple confocal models to be used for stitching purposes
		self.subslice_selections_models = [None]*len(model_list)
		self.subslice_buttons_models = [None]*len(model_list)
		self.select_buttons_models = [None]*len(model_list)
		self.select_all_models = [None]*len(model_list)
		cur_column = 0
		separator_cols = []
		for model_ind in range(len(model_list)):
			cur_slice_list = slice_list[model_ind]
			cur_subslice_list = subslice_list[model_ind]
			# Place the model name at the top of the section containing the model's slices.
			ttk.Label(slice_frame, text=model_list[model_ind].model_name).grid(row=0, column=cur_column, columnspan=len(cur_slice_list))
			# Set up sub-lists to track selected subslices.
			self.subslice_selections_models[model_ind] = {}
			self.subslice_buttons_models[model_ind] = [None]*len(cur_slice_list)
			self.select_buttons_models[model_ind] = [None]*len(cur_slice_list)
			self.select_all_models[model_ind] = [None]*len(cur_slice_list)
			
			for slice_ind, slice_name in enumerate(cur_slice_list):
				# Set up the columns, labels, and add a button to toggle all subslices on or off
				slice_frame.columnconfigure(cur_column, pad=10)
				ttk.Label(slice_frame, text=slice_name).grid(row=1, column=cur_column)
				self.select_buttons_models[model_ind][slice_ind] = ttk.Button(slice_frame, text="Deselect All", command= lambda m=model_ind,c=slice_ind: self.__selectAllFrames(m,c))
				self.select_buttons_models[model_ind][slice_ind].grid(row=2, column=cur_column)
				self.select_all_models[model_ind][slice_ind] = True
				self.subslice_buttons_models[model_ind][slice_ind] = [None]*len(subslice_list[model_ind][slice_ind])
				# Set up dictionary entries for each subslice, then store in top-level list
				for sub_ind, sub_name in enumerate(subslice_list[model_ind][slice_ind]):
					cur_string = slice_name + ' Frame ' + str(sub_name)
					self.subslice_buttons_models[model_ind][slice_ind][sub_ind] = cur_string
					self.subslice_selections_models[model_ind][cur_string] = tk.IntVar(value=1)
					ttk.Checkbutton(slice_frame, text="Frame " + str(sub_name), variable=self.subslice_selections_models[model_ind][cur_string]).grid(row=sub_ind+3, column=cur_column)
				cur_column += 1
			separator_cols.append(cur_column)
			cur_column += 1
		farthest_column, lowest_row = slice_frame.grid_size()
		# Place channel selections on the slice selection frame
		self.channel_selections_models = [None]*len(model_list)
		cur_column = 0
		for model_ind in range(len(model_list)):
			# Gather and set up lists to track channel selections
			cur_slice_list = slice_list[model_ind]
			cur_channel_list = channel_list[model_ind]
			self.channel_selections_models[model_ind] = {}
			for slice_ind, slice_name in enumerate(cur_slice_list):
				# Place channel labels and checkboxes for each channel option
				ttk.Label(slice_frame, text='Channels').grid(row=lowest_row, column=cur_column)
				for channel_ind, channel_name in enumerate(cur_channel_list[slice_ind]):
					# Add a dictionary key and value for each channel
					cur_string = slice_name + ' ' + channel_name
					self.channel_selections_models[model_ind][cur_string] = tk.IntVar(value=1)
					ttk.Checkbutton(slice_frame, text=channel_name, variable=self.channel_selections_models[model_ind][cur_string]).grid(row=lowest_row+channel_ind+1, column=cur_column)
				cur_column += 1
			cur_column += 1
		farthest_column, lowest_row = slice_frame.grid_size()
		# Add separators between models for visual clarity
		separator_cols = separator_cols[:-1]
		for separator in separator_cols:
			ttk.Separator(slice_frame, orient='vertical').grid(column=separator, row=0, rowspan=lowest_row, sticky='NESW')
		ttk.Separator(slice_frame, orient='horizontal').grid(column=0, row=lowest_row, columnspan=farthest_column, sticky='EW')
		lowest_row += 1
		# Add text box for mid-stitch resizing option and buttons to initiate stitching
		ttk.Label(slice_frame, text='Resizing Factor:').grid(row=lowest_row, column=0, columnspan=math.ceil(farthest_column/2), sticky='E')
		resize_entry = ttk.Entry(slice_frame, width=10)
		resize_entry.grid(row=lowest_row, column=math.ceil(farthest_column/2), columnspan=math.floor(farthest_column/2), sticky='W')
		resize_entry.insert(0, '1')
		resize_entry.configure(validate='key', validatecommand=(resize_entry.register(self.floatValidate), '%P'))
		#column_span = 2-((farthest_column - len(separator_cols)) % 2)
		ttk.Button(slice_frame, width=30, text='Generate Stitched Images', command= lambda: self._stitchSlices(slice_list, model_list, compress_ratio=resize_entry.get())).grid(row=lowest_row+1, column=0, columnspan=farthest_column)
		ttk.Button(slice_frame, width=30, text='Generate Stitch Grid Files', command= lambda: self._generateStitchFiles(slice_list, model_list)).grid(row=lowest_row+2, column=0, columnspan=farthest_column)
		# Set up padding for the button rows.
		slice_frame.rowconfigure(lowest_row, pad=4)
		slice_frame.rowconfigure(lowest_row+1, pad=2)
		slice_frame.rowconfigure(lowest_row+2, pad=4)
		# Resize the canvas to match the frame and set the correct frame size
		slice_frame.update()
		slice_canvas.update()
		frame_height = np.min([int(screen_height * 3 / 4), slice_frame.winfo_height()])
		slice_canvas.config(width = slice_frame.winfo_width(), height=frame_height)
	
	def _createSubsliceWindowDeprecated(self, slice_list, subslice_list, channel_list, model_list):
		"""Create a window to select subslices and channels.
		"""
		# Calculate window height
		root = tk.Tk()
		screen_height = root.winfo_screenheight()
		root.destroy()
		
		self.slice_menu = tk.Toplevel(self.master)
		self.slice_menu.wm_title('Select Subslices and Channels')
		
		# Set up canvas to allow scrollable window
		slice_canvas = tk.Canvas(self.slice_menu, borderwidth=0)
		slice_frame = tk.Frame(slice_canvas)
		scroll_bar = tk.Scrollbar(self.slice_menu, orient="vertical", command=slice_canvas.yview)
		slice_canvas.configure(yscrollcommand=scroll_bar.set)
		
		scroll_bar.pack(side="right", fill="y")
		slice_canvas.pack(side="left", fill="both", expand=True)
		slice_canvas.create_window((4,4), window=slice_frame, anchor="nw")
		
		def onFrameConfigure(canvas):
			canvas.configure(scrollregion=canvas.bbox("all"))
		
		slice_canvas.bind_all("<MouseWheel>", lambda event: slice_canvas.yview_scroll(int(-1*(event.delta/40)), "units"))
		slice_frame.bind("<Configure>", lambda event, canvas=slice_canvas: onFrameConfigure(slice_canvas))
		
		# Set up lists to allow for multiple confocal models to be used for stitching purposes
		self.subslice_selections_models = [None]*len(model_list)
		self.subslice_buttons_models = [None]*len(model_list)
		self.select_buttons_models = [None]*len(model_list)
		self.select_all_models = [None]*len(model_list)
		cur_column = 0
		separator_cols = []
		for model_ind in range(len(model_list)):
			cur_slice_list = slice_list[model_ind]
			cur_subslice_list = subslice_list[model_ind]
			# Place the model name at the top of the section containing the model's slices.
			ttk.Label(slice_frame, text=model_list[model_ind].model_name).grid(row=0, column=cur_column, columnspan=len(cur_slice_list))
			# Set up sub-lists to track selected subslices.
			self.subslice_selections_models[model_ind] = {}
			self.subslice_buttons_models[model_ind] = [None]*len(cur_slice_list)
			self.select_buttons_models[model_ind] = [None]*len(cur_slice_list)
			self.select_all_models[model_ind] = [None]*len(cur_slice_list)
			
			for slice_ind, slice_name in enumerate(cur_slice_list):
				# Set up the columns, labels, and add a button to toggle all subslices on or off
				slice_frame.columnconfigure(cur_column, pad=10)
				ttk.Label(slice_frame, text=slice_name).grid(row=1, column=cur_column)
				self.select_buttons_models[model_ind][slice_ind] = ttk.Button(slice_frame, text="Deselect All", command= lambda m=model_ind,c=slice_ind: self.__selectAllFrames(m,c))
				self.select_buttons_models[model_ind][slice_ind].grid(row=2, column=cur_column)
				self.select_all_models[model_ind][slice_ind] = True
				self.subslice_buttons_models[model_ind][slice_ind] = [None]*len(subslice_list[model_ind][slice_ind])
				# Set up dictionary entries for each subslice, then store in top-level list
				for sub_ind, sub_name in enumerate(subslice_list[model_ind][slice_ind]):
					cur_string = slice_name + ' Frame ' + str(sub_name)
					self.subslice_buttons_models[model_ind][slice_ind][sub_ind] = cur_string
					self.subslice_selections_models[model_ind][cur_string] = tk.IntVar(value=1)
					ttk.Checkbutton(slice_frame, text="Frame " + str(sub_name), variable=self.subslice_selections_models[model_ind][cur_string]).grid(row=sub_ind+3, column=cur_column)
				cur_column += 1
			separator_cols.append(cur_column)
			cur_column += 1
		farthest_column, lowest_row = slice_frame.grid_size()
		# Place channel selections on the slice selection frame
		self.channel_selections_models = [None]*len(model_list)
		cur_column = 0
		for model_ind in range(len(model_list)):
			# Gather and set up lists to track channel selections
			cur_slice_list = slice_list[model_ind]
			cur_channel_list = channel_list[model_ind]
			self.channel_selections_models[model_ind] = {}
			for slice_ind, slice_name in enumerate(cur_slice_list):
				# Place channel labels and checkboxes for each channel option
				ttk.Label(slice_frame, text='Channels').grid(row=lowest_row, column=cur_column)
				for channel_ind, channel_name in enumerate(cur_channel_list[slice_ind]):
					# Add a dictionary key and value for each channel
					cur_string = slice_name + ' ' + channel_name
					self.channel_selections_models[model_ind][cur_string] = tk.IntVar(value=1)
					ttk.Checkbutton(slice_frame, text=channel_name, variable=self.channel_selections_models[model_ind][cur_string]).grid(row=lowest_row+channel_ind+1, column=cur_column)
				cur_column += 1
			cur_column += 1
		farthest_column, lowest_row = slice_frame.grid_size()
		# Add separators between models for visual clarity
		separator_cols = separator_cols[:-1]
		for separator in separator_cols:
			ttk.Separator(slice_frame, orient='vertical').grid(column=separator, row=0, rowspan=lowest_row, sticky='NESW')
		ttk.Separator(slice_frame, orient='horizontal').grid(column=0, row=lowest_row, columnspan=farthest_column, sticky='EW')
		lowest_row += 1
		# Add text box for mid-stitch resizing option and buttons to initiate stitching
		ttk.Label(slice_frame, text='Resizing Factor:').grid(row=lowest_row, column=0, columnspan=math.ceil(farthest_column/2), sticky='E')
		resize_entry = ttk.Entry(slice_frame, width=10)
		resize_entry.grid(row=lowest_row, column=math.ceil(farthest_column/2), columnspan=math.floor(farthest_column/2), sticky='W')
		resize_entry.insert(0, '1')
		resize_entry.configure(validate='key', validatecommand=(resize_entry.register(self.floatValidate), '%P'))
		#column_span = 2-((farthest_column - len(separator_cols)) % 2)
		ttk.Button(slice_frame, width=30, text='Generate Stitched Images', command= lambda: self._stitchSlices(slice_list, model_list, compress_ratio=resize_entry.get())).grid(row=lowest_row+1, column=0, columnspan=farthest_column)
		ttk.Button(slice_frame, width=30, text='Generate Stitch Grid Files', command= lambda: self._generateStitchFiles(slice_list, model_list)).grid(row=lowest_row+2, column=0, columnspan=farthest_column)
		# Set up padding for the button rows.
		slice_frame.rowconfigure(lowest_row, pad=4)
		slice_frame.rowconfigure(lowest_row+1, pad=2)
		slice_frame.rowconfigure(lowest_row+2, pad=4)
		# Resize the canvas to match the frame and set the correct frame size
		slice_frame.update()
		slice_canvas.update()
		frame_height = np.min([int(screen_height * 3 / 4), slice_frame.winfo_height()])
		slice_canvas.config(width = slice_frame.winfo_width(), height=frame_height)
	
	def __selectAllFrames(self, model_ind, slice_ind):
		"""Function to check / uncheck all the subslice selection checkboxes in the popout window for stitching.
		"""
		if self.select_all_models[model_ind][slice_ind]:
			for subslice_string in self.subslice_buttons_models[model_ind][slice_ind]:
				self.subslice_selections_models[model_ind][subslice_string].set(0)
			self.select_buttons_models[model_ind][slice_ind].configure(text="Select All")
			self.select_all_models[model_ind][slice_ind] = False
		else:
			for subslice_string in self.subslice_buttons_models[model_ind][slice_ind]:
				self.subslice_selections_models[model_ind][subslice_string].set(1)
			self.select_buttons_models[model_ind][slice_ind].configure(text="Deselect All")
			self.select_all_models[model_ind][slice_ind] = True
	
	def _stitchSlices(self, slice_list, model_list, compress_ratio='1'):
		"""Actually iterate through and run the stitching process for each item selected by the user.
		"""
		# Get the selected channels and slices from the GUI window
		subslice_list, channel_list = self.__getSubsChannels(slice_list, model_list)
		# Verify that the resizing / compression ratio is within accepted range and of the correct format
		try:
			compress_ratio = float(compress_ratio)
			# If the value is beyond reasonable range for resizing, set to 1, throw a warning message, and continue
			if compress_ratio > 1 or compress_ratio <= 0:
				compress_ratio = 1
				messagebox.showwarning('Compression Invalid', 'Compression ratio must be between 0 and 1. Setting value to 1 and proceeding.')
		except:
			# If the format is incorrect, set to 1, throw a warning message, and continue
			compress_ratio = 1
			messagebox.showwarning('Compression Invalid', 'Compression ratio must be decimal value between 0 and 1. Setting value to 1 and proceeding.')
		# Iterate through the models to stitch each selected subslice & channel
		for model_ind, conf_model in enumerate(model_list):
			conf_model.generateStitchedImages(slice_list[model_ind], subslice_list[model_ind], channel_list[model_ind], compress_ratio=compress_ratio)
	
	def _generateStitchFiles(self, slice_list, model_list):
		"""Generate a text file in each folder that dictates the positioning of the files in the larger stitched image.
		"""
		for model_ind, conf_model in enumerate(model_list):
			conf_model.generateImageGridFiles(slice_list[model_ind])
	
	def __getSubsChannels(self, slice_list, model_list):
		"""Get the subslices and channels based on the selections made in the slice selection window.
		"""
		sub_slices_models = [None]*len(model_list)
		slice_chans_models = [None]*len(model_list)
		# Iterate through models to get selections made per model
		for model_ind, conf_model in enumerate(model_list):
			sub_slices = [None]*len(slice_list[model_ind])
			slice_chans = [None]*len(slice_list[model_ind])
			# Iterate through the slices in the current model
			for slice_num, slice_name in enumerate(slice_list[model_ind]):
				subslice_list = model_list[model_ind].getSubsliceList(slice_num)
				channel_list = model_list[model_ind].getChannelList(slice_num)
				subslice_selected = [False]*len(subslice_list)
				channel_selected = [False]*len(channel_list)
				for sub_num, sub_slice in enumerate(subslice_list):
					# Dictionary values used to match designated dict keys from window creation
					subslice_string = slice_name + " Frame " + str(sub_slice)
					subslice_selected[sub_num] = self.subslice_selections_models[model_ind][subslice_string].get()
				for channel_num, channel in enumerate(channel_list):
					# Dictionary setup is the same as subslices
					channel_string = slice_name + ' ' + channel
					channel_selected[channel_num] = self.channel_selections_models[model_ind][channel_string].get()
				# Append the lists for the subslices to the list for the full slice
				sub_slices[slice_num] = list(np.where(subslice_selected)[0])
				slice_chans[slice_num] = list(np.where(channel_selected)[0])
			# Append the full-slice list to the model list
			sub_slices_models[model_ind] = sub_slices
			slice_chans_models[model_ind] = slice_chans
		return([sub_slices_models, slice_chans_models])
	
root = tk.Tk()
gui = modelGUI(master=root)
gui.master.title('Cardiac Modeling Toolbox')
gui.mainloop()