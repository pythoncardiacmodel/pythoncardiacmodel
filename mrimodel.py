# -*- coding: utf-8 -*-
"""
Contains all class definitions and imports necessary to implement MRI segmentation imports
and alignments. Built based on MRI Processing MATLAB pipeline by Thien-Khoi Phung.

Created on Fri Jul 21 11:27:52 2017

@author: cdw2be
"""

# This is just a test comment to verify branch success.

# Imports
import math
import scipy as sp
import numpy as np
import matplotlib.pyplot as mplt
import confocalmodel
from mpl_toolkits.mplot3d import Axes3D
from cardiachelpers import importhelper
from cardiachelpers import stackhelper
from cardiachelpers import mathhelper
from cardiachelpers import displayhelper
# Call this to set appropriate printing to view all data from arrays
np.set_printoptions(threshold=np.inf)
np.core.arrayprint._line_width = 160

class MRIModel():

	"""Contains contours based on multiple MRI modalities.

	Class containing contours based on multiple MRI modalities. Can be used to
	import Cine Black-Blood stacks and LGE stacks. Use Long-Axis image to determine
	vertical slice orientation.
	
	Attributes:
		scar (bool): Whether or not the model has an LGE data set
		dense (bool): Whether or not the model has a DENSE data set
		*_endo (array): Endocardial contour for the data set indicated by *
		*_epi (array): Epicardial contour for the data set indicated by *
		*_apex_pt (array): Apex point for the data set indicated by *
		*_basal_pt (array): Basal point for the data set indicated by *
		*_septal_pts (array): RV Insertion points selected in segment for the data set indicated by *
		*_slices (list): List of slices that were traced in SEGMENT for the data set indicated by *
		scar_ratio (array): Ratio of scar versus wall thickness by angle bins
		aligned_scar (array): The scar contour mapped from LGE to cine stack
	"""
	
	def __init__(self, cine_file, la_file, sa_scar_file=None, la_scar_files=None, dense_file=None, twoxcrm_pd_file=None, twoxcrm_data_file=None, confocal_directory=None):
		"""Initialize new MRI model and select applicable files.
		
		Args:
			scar: Determines whether to import an LGE image stack.
			dense: Determines whether to import a DENSE image stack.
			
		Returns: New instance of MRIModel
		"""
		self.cine_file = cine_file
		self.long_axis_file = la_file
		if sa_scar_file:
			self.sa_scar_file = sa_scar_file
			self.scar = True
			if la_scar_files:
				self.la_scar_files = la_scar_files
				self.la_scar = True
			else:
				self.la_scar_files = None
				self.la_scar = False
		else:
			self.sa_scar_file = None
			self.scar = False
			self.la_scar = False
		
		if dense_file:
			self.dense_file = dense_file
			self.dense = True
		else:
			self.dense_file = None
			self.dense = False
		
		if twoxcrm_pd_file:
			self.twoxcrm_pd_file = twoxcrm_pd_file
			self.twoxcrm_data_file = twoxcrm_data_file
			self.twoxcrm = True
		else:
			self.twoxcrm_pd_file = None
			self.twoxcrm_data_file = None
			self.twoxcrm = False
		
		if confocal_directory:
			self.confocal_directory = confocal_directory
			self.confocal = True
		else:
			self.confocal_directory = None
			self.confocal = False
		
		# Define variables used in all models
		self.apex_base_pts = importhelper.importLongAxis(self.long_axis_file)
		
		self.cine_endo = []
		self.cine_epi = []
		self.cine_apex_pt = []
		self.cine_basal_pt = []
		self.cine_septal_pts = []
		self.cine_slices = []
		self.cine_timepts = []
		
		# Set up scar-specific variables
		self.lge_endo = []
		self.lge_epi = []
		self.scar_ratio = []
		self.lge_apex_pt = []
		self.lge_basal_pt = []
		self.lge_septal_pts = []
		self.scar_slices = []
		self.aligned_scar = []
		
		# Set up DENSE-specific variables
		self.dense_endo = []
		self.dense_epi = []
		self.dense_pts = []
		self.dense_displacement = []
		self.dense_slices = []
		self.dense_aligned_pts = []
		self.dense_aligned_displacement = []
		self.radial_strain = []
		self.circumferential_strain = []
		
		# Set up Confocal-specific variables
		self.conf_endo_all = []
		self.conf_epi_all = []
		self.conf_pts_all = []
		self.conf_pinpts_all = []
		self.conf_pts_vals = []
	
	def importCine(self, timepoint=0):
		"""Import the black-blood cine stack.

		Returns:
			boolean: True if import was successful.
		"""
		# Import the Black-Blood cine (short axis) stack
		endo_stack, epi_stack, self.rv_insertion_pts, sastruct, septal_slice = importhelper.importStack(self.cine_file, timepoint)
		self.cine_image_orientation = sastruct['ImageOrientation']
		self.cine_image_position = sastruct['ImagePosition']
		_, endo_for_slice, epi_for_slice, _ = stackhelper.getContourFromStack(endo_stack, epi_stack, sastruct, self.rv_insertion_pts, septal_slice, self.apex_base_pts)
		self.endo_slices = [np.unique(np.round(endo_for_slice[i][:, 2], 2))[0] for i in range(len(endo_for_slice))]
		self.epi_slices = [np.unique(np.round(epi_for_slice[i][:, 2], 2))[0] for i in range(len(epi_for_slice))]
		kept_slices = sastruct['KeptSlices']
		self.cine_slice_thickness = sastruct['SliceThickness']
		self.cine_slice_gap = sastruct['SliceGap']
		apex_pt = self.apex_base_pts[0, :]
		base_pt = self.apex_base_pts[1, :]
		center_septal_pt = np.expand_dims(self.rv_insertion_pts[2, :], 0)
		time_pts = np.unique(endo_stack[:, 3])
		endo = [None]*time_pts.size
		epi = [None]*time_pts.size
		self.cine_timepts = [None]*time_pts.size
		
		# Format endo and epi contour points
		for i, time_pt in enumerate(time_pts):
			endo_by_time = endo_stack[np.where(endo_stack[:, 3] == time_pt)[0], :]
			epi_by_time = epi_stack[np.where(epi_stack[:, 3] == time_pt)[0], :]
			endo_time_by_slice = [None]*len(kept_slices)
			epi_time_by_slice = [None]*len(kept_slices)
			for j, slice_num in enumerate(kept_slices):
				endo_time_by_slice[j] = endo_by_time[np.where(endo_by_time[:, 4] == slice_num)[0], :3]
				endo_slice_column = endo_by_time[np.where(endo_by_time[:, 4] == slice_num)[0], 4]
				epi_time_by_slice[j] = epi_by_time[np.where(epi_by_time[:, 4] == slice_num)[0], :3]
				epi_slice_column = epi_by_time[np.where(epi_by_time[:, 4] == slice_num)[0], 4]
				endo_time_by_slice[j] = np.column_stack((endo_time_by_slice[j], endo_slice_column))
				epi_time_by_slice[j] = np.column_stack((epi_time_by_slice[j], epi_slice_column))
			endo[i] = endo_time_by_slice
			epi[i] = epi_time_by_slice
			self.cine_timepts[i] = int(time_pt)
		
		cine_endo_rotate = [None]*len(endo)
		cine_epi_rotate = [None]*len(epi)
		
		# Rotate endo and epicardial points and reformat the rotated points.
		for time_pt in range(len(cine_endo_rotate)):
			endo_rotate_timepts = [None]*len(endo[time_pt])
			epi_rotate_timepts = [None]*len(epi[time_pt])
			for slice_num in range(len(endo_rotate_timepts)):
				endo_rotate_timepts[slice_num], self.focus, self.transform_basis, _ = stackhelper.rotateDataCoordinates(endo[time_pt][slice_num][:, :3], apex_pt, base_pt, center_septal_pt)
				epi_rotate_timepts[slice_num], _, self.transform_basis, _ = stackhelper.rotateDataCoordinates(epi[time_pt][slice_num][:, :3], apex_pt, base_pt, center_septal_pt)
				endo_rotate_timepts[slice_num] = np.column_stack((endo_rotate_timepts[slice_num], endo[time_pt][slice_num][:, 3]))
				epi_rotate_timepts[slice_num] = np.column_stack((epi_rotate_timepts[slice_num], epi[time_pt][slice_num][:, 3]))
			cine_endo_rotate[time_pt] = endo_rotate_timepts
			cine_epi_rotate[time_pt] = epi_rotate_timepts
		self.rv_insertion_pts_rot, _, _, _ = stackhelper.rotateDataCoordinates(self.rv_insertion_pts, apex_pt, base_pt, center_septal_pt)
		self.abs_pts_rot = stackhelper.rotateDataCoordinates(self.apex_base_pts, apex_pt, base_pt, center_septal_pt)[0]

		cine_endo_arrs = [None]*len(endo)
		cine_epi_arrs = [None]*len(epi)
		cine_endo_rotate_arrs = [None]*len(cine_endo_rotate)
		cine_epi_rotate_arrs = [None]*len(cine_epi_rotate)
		
		# Establish list of arrays to format
		for time_ind, endo_timept in enumerate(endo):
			epi_timept = epi[time_ind]
			endo_rotate_timept = cine_endo_rotate[time_ind]
			epi_rotate_timept = cine_epi_rotate[time_ind]
			cine_endo_arrs[time_ind] = np.vstack(endo_timept)
			cine_endo_rotate_arrs[time_ind] = np.vstack(endo_rotate_timept)
			cine_epi_arrs[time_ind] = np.vstack(epi_timept)
			cine_epi_rotate_arrs[time_ind] = np.vstack(epi_rotate_timept)
		
		# Store class fields based on calculated values:
		self.cine_endo_rotate = cine_endo_rotate_arrs
		self.cine_epi_rotate = cine_epi_rotate_arrs
		self.cine_endo = cine_endo_arrs
		self.cine_epi = cine_epi_arrs
		self.cine_apex_pt = self.abs_pts_rot[0]
		self.cine_basal_pt = self.abs_pts_rot[1]
		self.cine_septal_pts = self.rv_insertion_pts_rot
		return(True)
		
	def importLGE(self):
		"""Import the LGE MRI stack.
		
		Create an endocardial and epicardial contour based on the LGE file stack.
		Additionally imports scar traces and stores them as scar ratio.
		Scar ratio is the ratio of the scar contour edges compared to wall thickness.
			
		Returns:
			boolean: True if the import was successful.
		"""
		scar_endo_stack, scar_epi_stack, scar_insertion_pts, scarstruct, scar_septal_slice = importhelper.importStack(self.sa_scar_file)
		
		# Prepare variables imported from file.
		scar_auto = np.array(scarstruct['Scar']['Auto'])
		scar_manual = np.array(scarstruct['Scar']['Manual'])
		
		# Form a combination array that combines the automatic scar recognition with manual adjustments (including manual erasing)
		scar_combined = np.add(scar_auto, scar_manual) > 0
		
		# The new array needs to have axes adjusted to align with the format (z, x, y) allowing list[n] to return a full slice
		scar_combined = np.swapaxes(np.swapaxes(scar_combined, 1, 2), 0, 1)
		scar_slices = np.where([np.any(scar_combined[slice_num, :, :]) for slice_num in range(scar_combined.shape[0])])[0]
		self.scar_combined = scar_combined
		
		scar_x, scar_y = stackhelper.getMaskXY(scar_combined, scarstruct['KeptSlices'])
		scarstruct['mask_x'] = scar_x
		scarstruct['mask_y'] = scar_y
		scar_pt_stack, scar_m = stackhelper.rotateStack(scarstruct, scar_slices+1, layer='mask')

		apex_pt = self.apex_base_pts[0, :]
		base_pt = self.apex_base_pts[1, :]
		center_septal_pt = np.expand_dims(self.rv_insertion_pts[2, :], 0)
		#center_septal_pt = np.expand_dims(scar_insertion_pts[2, :], 0)
		
		scar_endo = [None]*len(scar_slices)
		scar_epi = [None]*len(scar_slices)
		scar_pts = [None]*len(scar_slices)
		self.lge_endo_rotate = [None]*len(scar_endo)
		self.lge_epi_rotate = [None]*len(scar_epi)
		self.lge_pts_rotate = [None]*len(scar_pts)
		
		for i, slice_num in enumerate(scar_slices):
			scar_endo[i] = scar_endo_stack[np.where(scar_endo_stack[:, 4] == slice_num+1)[0], :3]
			scar_epi[i] = scar_epi_stack[np.where(scar_epi_stack[:, 4] == slice_num+1)[0], :3]
			scar_pts[i] = scar_pt_stack[np.where(scar_pt_stack[:, 4] == slice_num+1)[0], :3]
			self.lge_endo_rotate[i], _, self.lge_transform_basis, self.lge_origin = stackhelper.rotateDataCoordinates(scar_endo[i], apex_pt, base_pt, center_septal_pt)
			self.lge_epi_rotate[i], _, self.lge_transform_basis, _ = stackhelper.rotateDataCoordinates(scar_epi[i], apex_pt, base_pt, center_septal_pt)
			self.lge_pts_rotate[i], _, _, _ = stackhelper.rotateDataCoordinates(scar_pts[i], apex_pt, base_pt, center_septal_pt)
			#Shift points by endocardial centroid in slice.
			# This corrects an error in import that was causing scar to not correctly fill the model.
			'''lge_endo_center = np.mean(self.lge_endo_rotate[i], axis=0)
			self.lge_endo_rotate[i][:, 1] -= lge_endo_center[1]
			self.lge_endo_rotate[i][:, 2] -= lge_endo_center[2]
			self.lge_epi_rotate[i][:, 1] -= lge_endo_center[1]
			self.lge_epi_rotate[i][:, 2] -= lge_endo_center[2]
			self.lge_pts_rotate[i][:, 1] -= lge_endo_center[1]
			self.lge_pts_rotate[i][:, 2] -= lge_endo_center[2]'''
		
		# Store instance fields
		self.lge_septal_pts = scar_insertion_pts
		self.lge_endo = scar_endo
		self.lge_epi = scar_epi
		self.scar_slices = scar_slices
		
		return(True)
		
	def importScarLA(self):
		"""Import Long-Axis LGE Images and contours.
		"""
		# Set up import variables based on number of long-axis files.
		scar_la_endo = [None]*len(self.la_scar_files)
		scar_la_epi = [None]*len(self.la_scar_files)
		scar_la_pinpts = [None]*len(self.la_scar_files)
		scar_la_struct = [None]*len(self.la_scar_files)
		scar_pt_stack = [None]*len(self.la_scar_files)

		for i, la_scar_file in enumerate(self.la_scar_files):
			# Import the long-axis-based LGE information
			scar_la_endo[i], scar_la_epi[i], scar_la_pinpts[i], scar_la_struct[i] = importhelper.importStack(la_scar_file, ignore_pinpts=True)
			
			# Define scar combination
			scar_auto = np.array(scar_la_struct[i]['Scar']['Auto'])
			scar_manual = np.array(scar_la_struct[i]['Scar']['Manual'])
			scar_combined_full = np.expand_dims(np.add(scar_auto, scar_manual) > 0, axis=0)
			
			scar_x, scar_y = stackhelper.getMaskXY(scar_combined_full, [0])
			
			scar_la_struct[i]['mask_x'] = scar_x
			scar_la_struct[i]['mask_y'] = scar_y
			
			scar_pt_stack[i], _ = stackhelper.rotateStack(scar_la_struct[i], [1], layer='mask')
		
		self.lge_la_endo_rotate = [None]*len(self.la_scar_files)
		self.lge_la_epi_rotate = [None]*len(self.la_scar_files)
		self.lge_la_pts_rotate = [None]*len(self.la_scar_files)
		
		apex_pt = self.apex_base_pts[0, :]
		base_pt = self.apex_base_pts[1, :]
		center_septal_pt = np.expand_dims(self.rv_insertion_pts[2, :], 0)
		
		# Rotate and save long-axis lge contours
		for slice_num in range(len(self.la_scar_files)):
			scar_endo = scar_la_endo[slice_num][:, :3]
			scar_epi = scar_la_epi[slice_num][:, :3]
			scar_pts = scar_pt_stack[slice_num][:, :3]
			
			self.lge_la_endo_rotate[slice_num] = stackhelper.rotateDataCoordinates(scar_endo, apex_pt, base_pt, center_septal_pt)[0]
			self.lge_la_epi_rotate[slice_num] = stackhelper.rotateDataCoordinates(scar_epi, apex_pt, base_pt, center_septal_pt)[0]
			self.lge_la_pts_rotate[slice_num] = stackhelper.rotateDataCoordinates(scar_pts, apex_pt, base_pt, center_septal_pt)[0]
		
	def importDense(self):
		"""Imports DENSE MR data from the file established at initialization.
		"""
		dense_endo = [None]*len(self.dense_file)
		dense_epi = [None]*len(self.dense_file)
		dense_pts = [None]*len(self.dense_file)
		slice_locations = [None]*len(self.dense_file)
		dense_displacement = False
		radial_strain = False
		circumferential_strain = False
		apex_pt = self.apex_base_pts[0, :]
		base_pt = self.apex_base_pts[1, :]
		center_septal_pt = np.expand_dims(self.rv_insertion_pts[2, :], 0)
		height_vals = [3, 2, 1]
		self.dense_image_position = [None for file_ind in range(len(self.dense_file))]
		self.dense_image_orientation = [None for file_ind in range(len(self.dense_file))]
		self.dense_image_resolution = [None for file_ind in range(len(self.dense_file))]
		for file_num in range(len(self.dense_file)):
			dense_file = self.dense_file[file_num]
			# Extract Contour Information from DENSE Mat file
			dense_data = importhelper.loadmat(dense_file)
			slice_location = dense_data['SequenceInfo'][0, 0].SliceLocation
			slice_locations[file_num] = slice_location
			image_orientation = dense_data['SequenceInfo'][0, 0].ImageOrientationPatient
			image_position = dense_data['SequenceInfo'][0, 0].ImagePositionPatient
			self.dense_image_position[file_num] = image_position
			self.dense_image_orientation[file_num] = image_orientation
			x_resolution = dense_data['SequenceInfo'][0, 0].PixelSpacing[0]
			y_resolution = dense_data['SequenceInfo'][0, 0].PixelSpacing[1]
			self.dense_image_resolution[file_num] = x_resolution
			epi_dense = np.array(dense_data['ROIInfo']['RestingContour'][0])
			endo_dense = np.array(dense_data['ROIInfo']['RestingContour'][1])
			m_arr = stackhelper.generateDenseTransformMatrix(x_resolution, y_resolution, image_position, image_orientation, slice_thickness=self.cine_slice_thickness, slice_gap=self.cine_slice_gap)
			endo_slice_diff = [abs(endo_slice_i - slice_location) for endo_slice_i in self.endo_slices]
			endo_slice_index = np.argmin(endo_slice_diff)-height_vals[file_num]
			#endo_slice_match = np.where(self.cine_endo_rotate[0][:, 3] == endo_slice_index)[0]
			#endo_slice = self.cine_endo_rotate[0][endo_slice_match, 0]
			#slice_z = np.mean(endo_slice)
			#rot_slice_z = -1*(endo_slice_index-1)
			rot_slice_z = 0
			
			# Append slice location to DENSE contour data
			endo_slice_col = np.array([rot_slice_z] * endo_dense.shape[0]).reshape([endo_dense.shape[0], 1])
			epi_slice_col = np.array([rot_slice_z] * epi_dense.shape[0]).reshape([epi_dense.shape[0], 1])
			endo_dense = np.append(endo_dense, endo_slice_col, axis=1)
			epi_dense = np.append(epi_dense, epi_slice_col, axis=1)
			# Interpolate data to get an equal number of points for each contour
			#endo_interp_func = sp.interpolate.interp1d(np.arange(0, 1+1/(endo_dense.shape[0]-1), 1/(endo_dense.shape[0]-1)), endo_dense, axis=0, kind='cubic')
			endo_interp_func = sp.interpolate.interp1d(np.linspace(0, 1, endo_dense.shape[0]), endo_dense, axis=0, kind='cubic')
			#epi_interp_func = sp.interpolate.interp1d(np.arange(0, 1+1/(epi_dense.shape[0]-1), 1/(epi_dense.shape[0]-1)), epi_dense, axis=0, kind='cubic')
			epi_interp_func = sp.interpolate.interp1d(np.linspace(0, 1, epi_dense.shape[0]), epi_dense, axis=0, kind='cubic')
			#endo_interp = endo_interp_func(np.arange(0, 80/79, 1/79))
			endo_interp = endo_interp_func(np.linspace(0, 1, 80))
			#epi_interp = epi_interp_func(np.arange(0, 80/79, 1/79))
			epi_interp = epi_interp_func(np.linspace(0, 1, 80))
			
			dense_x = dense_data['DisplacementInfo']['X']
			dense_y = dense_data['DisplacementInfo']['Y']
			dense_z = [rot_slice_z]*len(dense_x)
			dense_pts_for_rot = np.column_stack((dense_x, dense_y, dense_z))
			
			mult_arr = np.transpose(np.append(endo_interp, np.ones([endo_interp.shape[0], 1]), axis=1))
			X = np.transpose(m_arr@mult_arr)
			endo_interp_rot = X[:,0:3]
			mult_arr = np.transpose(np.append(epi_interp, np.ones([epi_interp.shape[0], 1]), axis=1))
			X = np.transpose(m_arr@mult_arr)
			epi_interp_rot = X[:, 0:3]
			pts_mult_arr = np.transpose(np.append(dense_pts_for_rot, np.ones([dense_pts_for_rot.shape[0], 1]), axis=1))
			X = np.transpose(m_arr@pts_mult_arr)
			dense_pts_rot = X[:, 0:3]
			
			dense_endo_rotate, _, dense_transform_basis, dense_origin = stackhelper.rotateDataCoordinates(endo_interp_rot, apex_pt, base_pt, center_septal_pt)
			dense_epi_rotate, _, _, _ = stackhelper.rotateDataCoordinates(epi_interp_rot, apex_pt, base_pt, center_septal_pt)
			dense_pts_rotate = stackhelper.rotateDataCoordinates(dense_pts_rot, apex_pt, base_pt, center_septal_pt)[0]
			dense_endo[file_num] = dense_endo_rotate
			dense_epi[file_num] = dense_epi_rotate
			dense_pts[file_num] = dense_pts_rotate
			
			# Pull timepoints from DENSE
			dense_timepoints = len(dense_data['DisplacementInfo']['dX'][0])
			
			# Shift the DENSE endo and epi contours by the epicardial mean
			'''endo_shift = endo_interp[:, :2] - np.mean(endo_interp[:, :2], axis=0)
			epi_shift = epi_interp[:, :2] - np.mean(endo_interp[:, :2], axis=0)
			endo_shift = np.column_stack((endo_shift, endo_interp[:, 2]))
			epi_shift = np.column_stack((epi_shift, epi_interp[:, 2]))
			#dense_endo[i] = endo_shift
			#dense_epi[i] = epi_shift
			
			# Scale DENSE endo and epi contour radius
			cine_endo_polar = self.cine_endo_rotate[0][endo_slice_match, 1:3]
			cine_epi_polar = self.cine_epi_rotate[0][endo_slice_match, 1:3]
			cine_endo_radius = np.mean(mathhelper.cart2pol(cine_endo_polar[:, 0], cine_endo_polar[:, 1])[1])
			cine_epi_radius = np.mean(mathhelper.cart2pol(cine_epi_polar[:, 0], cine_epi_polar[:, 1])[1])
			dense_endo_polar = np.column_stack(tuple(mathhelper.cart2pol(endo_shift[:, 0], endo_shift[:, 1])))
			dense_epi_polar = np.column_stack(tuple(mathhelper.cart2pol(epi_shift[:, 0], epi_shift[:, 1])))
			dense_endo_radius = np.mean(dense_endo_polar[:, 1])
			dense_epi_radius = np.mean(dense_epi_polar[:, 1])
			average_radius_ratio = np.mean([dense_endo_radius/cine_endo_radius, dense_epi_radius/cine_epi_radius])
			dense_endo_polar[:, 1] = dense_endo_polar[:, 1]/average_radius_ratio
			dense_epi_polar[:, 1] = dense_epi_polar[:, 1]/average_radius_ratio
			endo_scaled = np.column_stack(tuple(mathhelper.pol2cart(dense_endo_polar[:, 0], dense_endo_polar[:, 1])))
			epi_scaled = np.column_stack(tuple(mathhelper.pol2cart(dense_epi_polar[:, 0], dense_epi_polar[:, 1])))
			endo_scaled = np.column_stack((endo_scaled, endo_shift[:, 2]))
			epi_scaled = np.column_stack((epi_scaled, epi_shift[:, 2]))
			dense_endo[file_num] = endo_scaled
			dense_epi[file_num] = epi_scaled
			
			# Shift the entire pixel array by the same epicardial mean
			dense_x = dense_data['DisplacementInfo']['X'] - np.mean(endo_interp[:, 0])
			dense_y = dense_data['DisplacementInfo']['Y'] - np.mean(endo_interp[:, 1])
			dense_z = [slice_z]*len(dense_x)
			
			# Scale the dense pixel array by the same ratio, as well
			dense_polar = np.column_stack(tuple(mathhelper.cart2pol(dense_x, dense_y)))
			dense_polar[:, 1] = dense_polar[:, 1]/average_radius_ratio
			dense_x, dense_y = mathhelper.pol2cart(dense_polar[:, 0], dense_polar[:, 1])
			dense_pts[file_num] = np.column_stack((dense_x, dense_y, dense_z))
			
			all_dense_theta, all_dense_rho = mathhelper.cart2pol(dense_x, dense_y)'''
			
			# Get displacement / strain info and store as a 2-D array
			dense_dx = np.array(dense_data['DisplacementInfo']['dX'])
			dense_dy = np.array(dense_data['DisplacementInfo']['dY'])
			dense_dz = np.array(dense_data['DisplacementInfo']['dZ'])
			dense_radial = np.array(dense_data['StrainInfo']['RR'])
			peak_strain_ind = np.where(dense_radial[:, 11] == max(dense_radial[:, 11]))[0][0]
			dense_circumferential = np.array(dense_data['StrainInfo']['CC'])
			if dense_pts_for_rot.shape[0] != dense_radial.shape[0]:
				x_points = dense_data['DisplacementInfo']['X']
				y_points = dense_data['DisplacementInfo']['Y']
				mask_points = np.asarray(dense_data['StrainInfo']['Mask'])
				is_valid = [mask_points[y_points[cur_point]-1,x_points[cur_point]-1]==0 for cur_point in range(len(x_points))]
				skipped_index = np.where(is_valid)
				dense_x = np.delete(dense_x, skipped_index)
				dense_y = np.delete(dense_y, skipped_index)
				dense_dx = np.delete(dense_dx, skipped_index, axis=0)
				dense_dy = np.delete(dense_dy, skipped_index, axis=0)
				dense_dz = np.delete(dense_dz, skipped_index, axis=0)
				#dense_polar = np.delete(dense_polar, skipped_index, axis=0)
				dense_pts[file_num] = np.delete(dense_pts[file_num], skipped_index, axis=0)
				print('Removed index:')
				print(skipped_index)
			
			# Add DENSE displacement and strain slices by time
			if not dense_displacement:
				dense_time_pts = dense_radial.shape[1]
				dense_displacement = [None] * dense_time_pts
				radial_strain = [None] * dense_time_pts
				circumferential_strain = [None]*dense_time_pts
				for time_ind in range(dense_time_pts):
					dense_displacement[time_ind] = [np.column_stack((dense_dx[:, time_ind], dense_dy[:, time_ind], dense_dz[:, time_ind]))]
					radial_strain[time_ind] = [dense_radial[:, time_ind]]
					circumferential_strain[time_ind] = [dense_circumferential[:, time_ind]]
			else:
				for time_ind in range(dense_time_pts):
					cur_disp = np.column_stack((dense_dx[:, time_ind], dense_dy[:, time_ind], dense_dz[:, time_ind]))
					dense_displacement[time_ind].append(cur_disp)
					cur_rad_strain = dense_radial[:, time_ind]
					radial_strain[time_ind].append(cur_rad_strain)
					cur_circ_strain = dense_circumferential[:, time_ind]
					circumferential_strain[time_ind].append(cur_circ_strain)
		self.dense_endo = dense_endo
		self.dense_epi = dense_epi
		self.dense_pts = dense_pts
		self.dense_displacement = dense_displacement
		self.dense_slices = slice_locations
		self.radial_strain = radial_strain
		self.circumferential_strain = circumferential_strain
		return(True)
		
	def import2XCRM(self):
		twoxcrm_endo_stack, twoxcrm_epi_stack, twoxcrm_insertion_pts, pd_struct, twoxcrm_septal_slice = importhelper.importStack(self.twoxcrm_pd_file, is_twoxcrm=True)
		# Assign mid-ventricular slice position until a method of identifying slice location is fixed.
		twoxcrm_endo_stack[:, 4] = np.median(range(len(self.endo_slices)))
		twoxcrm_epi_stack[:, 4] = np.median(range(len(self.endo_slices)))
		twoxcrm_septal_slice = twoxcrm_endo_stack[0, 4]
		
		# Organize ROIs for processing.
		endo_roi = np.column_stack((pd_struct['EndoX'], pd_struct['EndoY']))
		epi_roi = np.column_stack((pd_struct['EpiX'], pd_struct['EpiY']))
		blood_roi = np.column_stack((pd_struct['Roi']['X'], pd_struct['Roi']['Y']))
		
		# Import data from contrast files.
		twoxcrm_data_struct = importhelper.loadmat(self.twoxcrm_data_file)['setstruct']
		twoxcrm_data_arr = np.multiply(twoxcrm_data_struct['IM'], twoxcrm_data_struct['IntensityScaling'])
		twoxcrm_pd_arr = np.multiply(pd_struct['IM'], pd_struct['IntensityScaling'])
		
		# Calculate pixel intensities in the ROIs
		twoxcrm_data_myocardium, myocardium_data_pixels = stackhelper.getPixelIntensities(epi_roi, twoxcrm_data_arr, endo_roi, sum_pixels=False)
		twoxcrm_data_blood = stackhelper.getPixelIntensities(blood_roi, twoxcrm_data_arr, sum_pixels=True)[0]
		twoxcrm_pd_myocardium = stackhelper.getPixelIntensities(epi_roi, twoxcrm_pd_arr, endo_roi, sum_pixels=True)[0]
		twoxcrm_pd_blood = stackhelper.getPixelIntensities(blood_roi, twoxcrm_pd_arr, sum_pixels=True)[0]
		twoxcrm_data_struct['mask_x'] = [np.expand_dims(myocardium_data_pixels[0], axis=0)]
		twoxcrm_data_struct['mask_y'] = [np.expand_dims(myocardium_data_pixels[1], axis=0)]
		print(twoxcrm_data_struct['mask_x'][0].shape)
		twoxcrm_pts_stack, twoxcrm_m = stackhelper.rotateStack(twoxcrm_data_struct, [1], layer='mask')
		print(twoxcrm_pts_stack)
		twoxcrm_data_myo_arr = np.array(twoxcrm_data_myocardium)
		twoxcrm_pd_myo_arr = np.array(twoxcrm_pd_myocardium)
		twoxcrm_data_pixel_list = [twoxcrm_data_myo_arr[:, pixel_ind] for pixel_ind in range(twoxcrm_data_myo_arr.shape[1])]
		#twoxcrm_pd_pixel_list = [twoxcrm_pd_myo_arr[:, pixel_ind] for pixel_ind in range(twoxcrm_pd_myo_arr.shape[1])]
		#normalized_myocardium = twoxcrm_data_myocardium/twoxcrm_pd_myocardium[0]*np.sin(math.radians(5))/np.sin(math.radians(15))
		normalized_blood = twoxcrm_data_blood/twoxcrm_pd_blood[0]*np.sin(math.radians(5))/np.sin(math.radians(15))
		t1_results_arr = np.empty((0, 5))
		t1_fit_list = []
		for pixel_ind in range(len(twoxcrm_data_pixel_list)):
			twoxcrm_data_myocardium = twoxcrm_data_pixel_list[pixel_ind]
			normalized_myocardium = twoxcrm_data_myocardium/twoxcrm_pd_myocardium[0]*np.sin(math.radians(5))/np.sin(math.radians(15))
			t1, aifgd_conc, t1_tf, tfgd_conc = mathhelper.twoxcrmNorm2gd(normalized_myocardium[:-1], normalized_blood[:-1], [1, 2, 3, 4, 6, 7, 8, 9], twoxcrm_data_struct, pd_struct)
			t1_decayrate, t1_decayslope = mathhelper.getDecayRates(tfgd_conc, twoxcrm_data_struct['TimeVector'], len(t1))
			t1_results, t1_fitted_models = mathhelper.modToftsKetyModel(twoxcrm_data_struct['TimeVector'], aifgd_conc, tfgd_conc)
			#results_plot = displayhelper.plotListData(t1_fitted_models[:, 3])[0]
			#results_plot = displayhelper.plotListData(t1_fitted_models[:, 4], fig=results_plot)[0]
			t1_results_arr = np.vstack((t1_results_arr, t1_results))
			t1_fit_list.append(t1_fitted_models)
		print(t1_results_arr)
		return(True)
	
	def importConfocal(self, slice_widths=0.25, compressed=False):
		"""Imports data gathered from confocal microscopy using the same principles as the DENSE Data import.
		"""
		self.confocal_model = confocalmodel.ConfocalModel(self.confocal_directory, slice_width=0.25)
		if self.confocal_model.data_imported:
			# Determine and apply compression for images
			if compressed == 'dense':
				for confocal_slice in self.confocal_model.confocal_slices:
					dense_compression_ratio = confocal_slice.image_resolution / self.dense_image_resolution[0]
					confocal_slice.compressImages(compression_ratio = dense_compression_ratio)
			elif compressed:
				for confocal_slice in self.confocal_model.confocal_slices:
					confocal_slice.compressImages(compression_ratio = compressed)
			# Pull relevant positioning information, calculate z orientation for image shifting.
			apex_pt = self.apex_base_pts[0, :]
			base_pt = self.apex_base_pts[1, :]
			cine_center_septal_pt = np.expand_dims(self.rv_insertion_pts[2, :], 0)
			conf_slice_positions = [slice_position + self.endo_slices[-1] for slice_position in self.confocal_model.slice_positions]
			#conf_image_orientation = self.cine_image_orientation
			conf_image_orientation = self.dense_image_orientation[len(self.dense_image_orientation) // 2]
			#base_image_position = self.cine_image_position
			base_image_position = self.dense_image_position[len(self.dense_image_position) // 2]
			z_image_orientation = np.cross(conf_image_orientation[0:3], conf_image_orientation[3:6])
			# Prepare lists to store contours
			self.conf_endo_all = [[None for subslice_num in range(len(self.confocal_model.confocal_slices[slice_num].endo_contour))] for slice_num in range(len(self.confocal_model.confocal_slices))]
			self.conf_epi_all = [[None for subslice_num in range(len(self.confocal_model.confocal_slices[slice_num].epi_contour))] for slice_num in range(len(self.confocal_model.confocal_slices))]
			self.conf_pts_all = [[None for subslice_num in range(len(self.confocal_model.confocal_slices[slice_num].epi_contour))] for slice_num in range(len(self.confocal_model.confocal_slices))]
			self.conf_pinpts_all = [[None for subslice_num in range(len(self.confocal_model.confocal_slices[slice_num].epi_contour))] for slice_num in range(len(self.confocal_model.confocal_slices))]
			self.conf_pts_vals = [[None for subslice_num in range(len(self.confocal_model.confocal_slices[slice_num].epi_contour))] for slice_num in range(len(self.confocal_model.confocal_slices))]
			# Iterate through each ConfocalSlice object
			for slice_num in range(len(self.confocal_model.confocal_slices)):
				if self.confocal_model.slices_imported[slice_num]:
					# Pull ConfocalSlice object and image resolution for the slice.
					conf_slice = self.confocal_model.confocal_slices[slice_num]
					image_resolution = conf_slice.compressed_image_resolution if compressed else conf_slice.image_resolution
					# Iterate through each frame of the confocal slice.
					for contour_ind in range(len(conf_slice.endo_contour)):
						# Calculate subslice position based on slice position, maximal endo slice position, and the slice separation provided for the slice.
						#subslice_position = self.endo_slices[-1] + contour_ind*conf_slice.slice_separation + self.confocal_model.slice_positions[slice_num]
						subslice_position = len(self.endo_slices) - (contour_ind*conf_slice.slice_separation + self.confocal_model.slice_positions[slice_num])
						#image_position_shift = subslice_position - self.endo_slices[0]
						#image_position_shift = math.ceil(len(self.endo_slices) / 2) + subslice_position
						image_position_shift = subslice_position
						conf_image_position = base_image_position - (z_image_orientation * image_position_shift)
						# Generate m_arr necessary for contour rotation
						m_arr = stackhelper.generateDenseTransformMatrix(image_resolution, image_resolution, conf_image_position, conf_image_orientation)
						if len(conf_slice.endo_contour[contour_ind]):
							# Pull endo, epi, points, and pinpoints for interpolation and rotation into the common coordinate system
							conf_endo = np.array(conf_slice.compressed_endo_contour[contour_ind])[:-1, :] if compressed else np.array(conf_slice.endo_contour[contour_ind])[:-1, :]
							conf_endo_interp, conf_endo_interp_rot = stackhelper.prepConfocalContour(conf_endo, m_arr, 0)
							conf_epi = np.array(conf_slice.compressed_epi_contour[contour_ind])[:-1, :] if compressed else np.array(conf_slice.epi_contour[contour_ind])[:-1, :]
							conf_epi_interp, conf_epi_interp_rot = stackhelper.prepConfocalContour(conf_epi, m_arr, 0)
							pixel_locs, pixel_vals = conf_slice.getPixelData(contour_ind, conf_epi_interp, conf_endo_interp, compressed=compressed)
							conf_pixel_channels = np.column_stack(tuple(pixel_vals))
							conf_pts_for_rot = np.column_stack((pixel_locs, np.zeros(pixel_locs.shape[0])))
							conf_pts_mult_arr = np.transpose(np.append(conf_pts_for_rot, np.ones([conf_pts_for_rot.shape[0], 1]), axis=1))
							X = np.transpose(m_arr@conf_pts_mult_arr)
							conf_pts_rot = X[:, 0:3]
							# Format RV Insertion Points and rotate in the same manner as the contours
							pts_for_midpt = conf_slice.septal_pts[:2, :2] if not compressed else conf_slice.compressed_septal_pts[:2, :2]
							conf_septal_pts = mathhelper.findConfMidPt(np.transpose(pts_for_midpt), conf_endo)
							z_pix = 0 * np.ones(conf_septal_pts.shape[0])
							conf_septal_pts = np.transpose(np.column_stack((conf_septal_pts, z_pix, np.ones(conf_septal_pts.shape[0]))))
							pp = np.transpose(m_arr@conf_septal_pts)
							conf_septal_pts = pp[:,0:3]
							conf_center_septal_pt = np.expand_dims(conf_septal_pts[2, :], 0)
							# Perform rotation step on each set of points. Rotation incorporates pinpoint rotation, so angular alignment should occur in this step.
							conf_endo_rotate, _, conf_transform_basis, conf_origin = stackhelper.rotateDataCoordinates(conf_endo_interp_rot, apex_pt, base_pt, conf_center_septal_pt)
							conf_epi_rotate, _, _, _ = stackhelper.rotateDataCoordinates(conf_epi_interp_rot, apex_pt, base_pt, conf_center_septal_pt)
							conf_pts_rotate, _, _, _ = stackhelper.rotateDataCoordinates(conf_pts_rot, apex_pt, base_pt, conf_center_septal_pt)
							conf_pinpts_rotate, _, _, _ = stackhelper.rotateDataCoordinates(conf_septal_pts, apex_pt, base_pt, conf_center_septal_pt)
							# Store rotated pixels and contours for later access
							self.conf_endo_all[slice_num][contour_ind] = conf_endo_rotate
							self.conf_epi_all[slice_num][contour_ind] = conf_epi_rotate
							self.conf_pts_all[slice_num][contour_ind] = conf_pts_rotate
							self.conf_pinpts_all[slice_num][contour_ind] = conf_pinpts_rotate
							self.conf_pts_vals[slice_num][contour_ind] = conf_pixel_channels
		return(True)
	
	def alignScar(self, timepoint=0):
		"""Scar alignment designed to include long-axis scar data.
		"""
		#Interpolate the scar contours circumferentially using 50 evenly-spaced angle bins
		self.interp_epi_surf, self.wall_scar, scar_pts_combined = stackhelper.interpShortScar(50, self.lge_epi_prol, self.lge_endo_prol, self.lge_pts_prol, self.lge_epi_rotate, self.lge_endo_rotate, self.lge_pts_rotate)
		#print(self.wall_scar)
		# Remove nan values from the list of scar transmuralities
		for slice_num in range(len(self.wall_scar)):
			temp_slice = self.wall_scar[slice_num]
			temp_slice[np.isnan(temp_slice[:, 1]), 1] = 0
			self.wall_scar[slice_num] = temp_slice
		# Interpolate scar contours based on the long-axis LGE contours
		if hasattr(self, 'lge_la_epi_prol'):
			self.interp_epi_la_surf, self.wall_scar_la, scar_pts_combined_la = stackhelper.interpLongScar(20, self.lge_la_epi_prol, self.lge_la_endo_prol, self.lge_la_pts_prol, self.lge_la_epi_rotate, self.lge_la_endo_rotate, self.lge_la_pts_rotate)
			# Remove nan values from the list of long-axis LGE transmuralities
			for slice_num in range(len(self.wall_scar_la)):
				temp_slice = self.wall_scar_la[slice_num]
				temp_slice[np.isnan(temp_slice[:, 1]), 1] = 0
				self.wall_scar_la[slice_num] = temp_slice
		temp_data_arr = np.empty([0, 6])
		# Stack the slices into single arrays
		for slice_num in range(len(self.wall_scar)):
			temp_slice_arr = np.column_stack((self.interp_epi_surf[slice_num], self.wall_scar[slice_num]))
			temp_data_arr = np.vstack((temp_data_arr, temp_slice_arr))
		if hasattr(self, 'lge_la_epi_prol'):
			for slice_num in range(len(self.wall_scar_la)):
				temp_slice_arr = np.column_stack((self.interp_epi_la_surf[slice_num], self.wall_scar_la[slice_num]))
				temp_data_arr = np.vstack((temp_data_arr, temp_slice_arr))
		nan_rows = ~np.isnan(temp_data_arr[:, 1])
		interp_data = temp_data_arr[nan_rows, :]
		epi_mu = interp_data[:, 0]
		wall_thickness = interp_data[:, 3]
		interp_data = interp_data[:, [2, 1, 4, 5]]
		interp_data_inc = np.column_stack((interp_data[:, 0]+2*math.pi, interp_data[:, 1:]))
		interp_data_dec = np.column_stack((interp_data[:, 0]-2*math.pi, interp_data[:, 1:]))
		interp_data_complete = np.vstack((interp_data, interp_data_inc, interp_data_dec))
		#self.interp_scar = interp_data_complete
		self.interp_scar = interp_data
		#ax = displayhelper.plot3d(np.column_stack((interp_data[:, :2], interp_data[:, 2])), type='scatter')
		self.interp_scar_trace = self.__convertInterpScar(scar_pts_combined)
		if hasattr(self, 'lge_la_epi_prol'):
			self.interp_scar_la_trace = self.__convertInterpScar(scar_pts_combined_la, long_axis=True)
		return(self.interp_scar)
	
	def convertDataProlate(self, focus=False):
		"""Convert all data from a rotated axis into prolate spheroid coordinates for further alignment.
		"""
		if not focus:
			focus = self.focus
		
		# Convert cine endocardial and epicardial traces
		cine_endo_prol = [None]*len(self.cine_endo_rotate)
		cine_epi_prol = [None]*len(self.cine_epi_rotate)
		
		for time_pt in range(len(self.cine_endo_rotate)):
			cine_endo_rot_time = self.cine_endo_rotate[time_pt]
			cine_epi_rot_time = self.cine_epi_rotate[time_pt]
			cine_endo_prol[time_pt] = np.column_stack(tuple(mathhelper.cart2prolate(cine_endo_rot_time[:, 0], cine_endo_rot_time[:, 1], cine_endo_rot_time[:, 2], focus)))
			cine_epi_prol[time_pt] = np.column_stack(tuple(mathhelper.cart2prolate(cine_epi_rot_time[:, 0], cine_epi_rot_time[:, 1], cine_epi_rot_time[:, 2], focus)))
		
		self.cine_pinpts_prol = np.column_stack(tuple(mathhelper.cart2prolate(self.cine_septal_pts[:, 0], self.cine_septal_pts[:, 1], self.cine_septal_pts[:, 2], focus)))
		self.cine_endo_prol = cine_endo_prol
		self.cine_epi_prol = cine_epi_prol

		# Convert Short-Axis LGE endocardial, epicardial, and scar-point traces
		if self.scar:
			lge_endo_prol = [None]*len(self.lge_endo_rotate)
			lge_epi_prol = [None]*len(self.lge_epi_rotate)
			lge_pts_prol = [None]*len(self.lge_pts_rotate)
		
			for i in range(len(self.lge_endo_rotate)):
				lge_endo_prol_list = mathhelper.cart2prolate(self.lge_endo_rotate[i][:, 0], self.lge_endo_rotate[i][:, 1], self.lge_endo_rotate[i][:, 2], focus)
				lge_epi_prol_list = mathhelper.cart2prolate(self.lge_epi_rotate[i][:, 0], self.lge_epi_rotate[i][:, 1], self.lge_epi_rotate[i][:, 2], focus)
				lge_pts_prol_list = mathhelper.cart2prolate(self.lge_pts_rotate[i][:, 0], self.lge_pts_rotate[i][:, 1], self.lge_pts_rotate[i][:, 2], focus)
				
				lge_endo_prol[i] = np.column_stack(tuple(lge_endo_prol_list))
				lge_epi_prol[i] = np.column_stack(tuple(lge_epi_prol_list))
				lge_pts_prol[i] = np.column_stack(tuple(lge_pts_prol_list))
			
			self.lge_endo_prol = lge_endo_prol
			self.lge_epi_prol = lge_epi_prol
			self.lge_pts_prol = lge_pts_prol
		# Convert long-axis LGE endocardial, epicardial, and scar-point traces
		if self.la_scar:
			lge_la_endo_prol = [None]*len(self.lge_la_endo_rotate)
			lge_la_epi_prol = [None]*len(self.lge_la_epi_rotate)
			lge_la_pts_prol = [None]*len(self.lge_la_pts_rotate)
			
			for i in range(len(self.lge_la_endo_rotate)):
				lge_la_endo_prol_list = mathhelper.cart2prolate(self.lge_la_endo_rotate[i][:, 0], self.lge_la_endo_rotate[i][:, 1], self.lge_la_endo_rotate[i][:, 2], focus)
				lge_la_epi_prol_list = mathhelper.cart2prolate(self.lge_la_epi_rotate[i][:, 0], self.lge_la_epi_rotate[i][:, 1], self.lge_la_epi_rotate[i][:, 2], focus)
				lge_la_pts_prol_list = mathhelper.cart2prolate(self.lge_la_pts_rotate[i][:, 0], self.lge_la_pts_rotate[i][:, 1], self.lge_la_pts_rotate[i][:, 2], focus)
				
				lge_la_endo_prol[i] = np.column_stack(tuple(lge_la_endo_prol_list))
				lge_la_epi_prol[i] = np.column_stack(tuple(lge_la_epi_prol_list))
				lge_la_pts_prol[i] = np.column_stack(tuple(lge_la_pts_prol_list))
				
			self.lge_la_endo_prol = lge_la_endo_prol
			self.lge_la_epi_prol = lge_la_epi_prol
			self.lge_la_pts_prol = lge_la_pts_prol
	
		if self.dense:
			self.dense_endo_prol = [None]*len(self.dense_endo)
			self.dense_epi_prol = [None]*len(self.dense_epi)
			self.dense_pts_prol = [None]*len(self.dense_pts)
			
			for i in range(len(self.dense_endo)):
				dense_slice = np.zeros((self.dense_endo[i].shape[0], 1))
				dense_slice.fill(self.dense_slices[i])
				dense_endo_prol_list = mathhelper.cart2prolate(self.dense_endo[i][:, 0], self.dense_endo[i][:, 1], self.dense_endo[i][:, 2], focus)
				dense_epi_prol_list = mathhelper.cart2prolate(self.dense_epi[i][:, 0], self.dense_epi[i][:, 1], self.dense_epi[i][:, 2], focus)
				dense_pts_prol_list = mathhelper.cart2prolate(self.dense_pts[i][:, 0], self.dense_pts[i][:, 1], self.dense_pts[i][:, 2], focus)
				
				self.dense_endo_prol[i] = np.column_stack(tuple(dense_endo_prol_list))
				self.dense_epi_prol[i] = np.column_stack(tuple(dense_epi_prol_list))
				self.dense_pts_prol[i] = np.column_stack(tuple(dense_pts_prol_list))
		
		if self.confocal:
			self.conf_endo_prol = [[None for i in range(len(self.conf_endo_all[slice_num]))] for slice_num in range(len(self.conf_endo_all))]
			self.conf_epi_prol = [[None for i in range(len(self.conf_epi_all[slice_num]))] for slice_num in range(len(self.conf_epi_all))]
			self.conf_pts_prol = [[None for i in range(len(self.conf_pts_all[slice_num]))] for slice_num in range(len(self.conf_pts_all))]
			self.conf_pinpts_prol = [[None for i in range(len(self.conf_pinpts_all[slice_num]))] for slice_num in range(len(self.conf_pinpts_all))]
			for slice_num in range(len(self.conf_endo_all)):
				conf_endo_slice = self.conf_endo_all[slice_num]
				conf_epi_slice = self.conf_epi_all[slice_num]
				conf_pts_slice = self.conf_pts_all[slice_num]
				conf_pinpts_slice = self.conf_pinpts_all[slice_num]
				for subslice_num in range(len(conf_endo_slice)):
					if conf_endo_slice[subslice_num] is not None:
						conf_endo_prol_list = mathhelper.cart2prolate(conf_endo_slice[subslice_num][:, 0], conf_endo_slice[subslice_num][:, 1], conf_endo_slice[subslice_num][:, 2], focus)
						conf_epi_prol_list = mathhelper.cart2prolate(conf_epi_slice[subslice_num][:, 0], conf_epi_slice[subslice_num][:, 1], conf_epi_slice[subslice_num][:, 2], focus)
						conf_pts_prol_list = mathhelper.cart2prolate(conf_pts_slice[subslice_num][:, 0], conf_pts_slice[subslice_num][:, 1], conf_pts_slice[subslice_num][:, 2], focus)
						conf_pinpts_prol_list = mathhelper.cart2prolate(conf_pinpts_slice[subslice_num][:, 0], conf_pinpts_slice[subslice_num][:, 1], conf_pinpts_slice[subslice_num][:, 2], focus)
						self.conf_endo_prol[slice_num][subslice_num] = np.column_stack(tuple(conf_endo_prol_list))
						self.conf_epi_prol[slice_num][subslice_num] = np.column_stack(tuple(conf_epi_prol_list))
						self.conf_pts_prol[slice_num][subslice_num] = np.column_stack(tuple(conf_pts_prol_list))
						self.conf_pinpts_prol[slice_num][subslice_num] = np.column_stack(tuple(conf_pinpts_prol_list))
	
	def alignScarCine(self, timepoint=0):
		"""Deprecated.
		"""
		# If a timepoint is passed, pull the cine from that point
		cine_endo = self.cine_endo[timepoint]
		cine_epi = self.cine_epi[timepoint]
		# Get slice values to section the endo / epi array by slice
		slice_indices = sorted(np.unique(cine_endo[:, 2], return_index=True)[1])
		slice_vals = cine_endo[slice_indices, 2]
		# Set up angle bins
		num_bins = self.scar_ratio.shape[1] + 1
		angles = np.linspace(-math.pi, math.pi, num_bins)
		angles2 = angles[1:]
		angles2 = np.append(angles2, angles[0])
		angles = np.column_stack((angles, angles2))[:-1]
		# Iterate through slices and convert to polar
		full_scar_contour = []
		for i in range(len(slice_vals)):
			# Get indices for the current slice
			cur_slice_ind = np.where(cine_endo[:, 2] == slice_vals[i])[0]
			# Pull current slice endocardial and epicardial cartesian contours
			cur_slice_endo = cine_endo[cur_slice_ind, :]
			cur_slice_epi = cine_epi[cur_slice_ind, :]
			
			# Get the slice center and shift by that value (center slices at 0)
			slice_center = np.mean(cur_slice_epi, axis=0)
			endo_x = cur_slice_endo[:, 0] - slice_center[0]
			endo_y = cur_slice_endo[:, 1] - slice_center[1]
			epi_x = cur_slice_epi[:, 0] - slice_center[0]
			epi_y = cur_slice_epi[:, 1] - slice_center[1]
			
			# Convert the cartesian contours to polar
			endo_theta, endo_rho = mathhelper.cart2pol(endo_x, endo_y)
			epi_theta, epi_rho = mathhelper.cart2pol(epi_x, epi_y)
			endo_theta = [cur_slice_theta_i - 2*np.pi if cur_slice_theta_i > np.pi else cur_slice_theta_i for cur_slice_theta_i in endo_theta]
			epi_theta = [cur_slice_theta_i - 2*np.pi if cur_slice_theta_i > np.pi else cur_slice_theta_i for cur_slice_theta_i in epi_theta]
			# Get rho values for each angle bin based on theta
			endo_bin_inds = [np.where((endo_theta > angles[i, 0]) & (endo_theta <= angles[i, 1]))[0].tolist() for i in range(angles.shape[0])]
			epi_bin_inds = [np.where((epi_theta > angles[i, 0]) & (epi_theta <= angles[i, 1]))[0].tolist() for i in range(angles.shape[0])]
			endo_rho_mean = [np.mean(endo_rho[endo_bin_inds_i]) for endo_bin_inds_i in endo_bin_inds]
			epi_rho_mean = [np.mean(epi_rho[epi_bin_inds_i]) for epi_bin_inds_i in epi_bin_inds]
			
			# Get the current scar slice
			cur_scar = self.scar_ratio[i, :, :]
			
			# Adjust any values less than 0 or greater than 1 in the ratio
			with np.errstate(invalid='ignore'):
				nonan_inds = np.where(~np.isnan(cur_scar[:, 1]))[0].tolist()
				for j in range(len(nonan_inds)):
					# Check if the value is less than 0
					if cur_scar[nonan_inds[j], 1] <= 0:
						# Set it equal to the average of the adjacent values (to create a smooth scar trace)
						if j == 0:
							cur_scar[nonan_inds[j], 1] = np.mean([cur_scar[nonan_inds[j], 2], cur_scar[nonan_inds[j+1], 1]])
						elif j == len(nonan_inds) - 1:
							cur_scar[nonan_inds[j], 1] = np.mean([cur_scar[nonan_inds[j-1], 1], cur_scar[nonan_inds[j], 2]])
						else:
							cur_scar[nonan_inds[j], 1] = np.mean([cur_scar[nonan_inds[j-1], 1], cur_scar[nonan_inds[(j+1) % (len(nonan_inds)-1)], 1]])
					# Check if the value is greater than 1
					if cur_scar[nonan_inds[j], 2] >= 1:
						# Set it equal to the average of the adjacent values (to create a smooth scar trace)
						if j == 0:
							cur_scar[nonan_inds[j], 2] = np.mean([cur_scar[nonan_inds[j], 1], cur_scar[nonan_inds[j+1], 2]])
						elif j == len(nonan_inds) - 1:
							cur_scar[nonan_inds[j], 2] = np.mean([cur_scar[nonan_inds[j-1], 2], cur_scar[nonan_inds[j], 1]])
						else:
							cur_scar[nonan_inds[j], 2] = np.mean([cur_scar[nonan_inds[j-1], 2], cur_scar[nonan_inds[(j+1) % (len(nonan_inds)-1)], 2]])
			
			# Get the scar inner and outer rho values based on endo and epi rho values
			scar_inner_rho = [endo_rho_mean[j] + cur_scar[j, 1] * (epi_rho_mean[j] - endo_rho_mean[j]) for j in range(cur_scar.shape[0])]
			scar_outer_rho = [endo_rho_mean[j] + cur_scar[j, 2] * (epi_rho_mean[j] - endo_rho_mean[j]) for j in range(cur_scar.shape[0])]
			# Convert the scar values to cartesian
			scar_inner_x, scar_inner_y = mathhelper.pol2cart(cur_scar[:, 0], scar_inner_rho)
			scar_outer_x, scar_outer_y = mathhelper.pol2cart(cur_scar[:, 0], scar_outer_rho)
			# If there is no scar trace here, just move to the next slice and append an empty array for the current contour
			if np.all(np.isnan(scar_inner_x)):
				full_scar_contour.append(np.array([]))
				continue
			
			# Extract the contour and remove NaN values
			# Roll the array so that the first point of the contour is the non-nan value immediately after nan
			nan_boundary = np.where([np.isnan(scar_inner_x[j]) & ~np.isnan(scar_inner_x[j+1]) for j in range(scar_inner_x.size - 1)])[0]
			scar_inner_x = np.roll(scar_inner_x, -nan_boundary[0]-1)
			# Find the end of the contour and slice, removing NaNs
			num_boundary = np.where([~np.isnan(scar_inner_x[j]) & np.isnan(scar_inner_x[j+1]) for j in range(scar_inner_x.size - 1)])[0]
			scar_inner_x = scar_inner_x[:num_boundary[0]+1]
			# Roll each other contour
			scar_outer_x = np.roll(scar_outer_x, -nan_boundary[0]-1)[:num_boundary[0]+1]
			scar_inner_y = np.roll(scar_inner_y, -nan_boundary[0]-1)[:num_boundary[0]+1]
			scar_outer_y = np.roll(scar_outer_y, -nan_boundary[0]-1)[:num_boundary[0]+1]
			# Construct combined arrays in trace order, inner -> outer in a loop
			scar_x = np.append(scar_inner_x, scar_outer_x[::-1])
			scar_y = np.append(scar_inner_y, scar_outer_y[::-1])
			# Re-shift based on center of slice
			scar_x += slice_center[0]
			scar_y += slice_center[1]
			# Stack the x, y, and z values into a slice of the full contour
			full_scar_contour.append(np.column_stack((scar_x, scar_y, [slice_vals[i]]*scar_x.size)))
		
		# Get equal number of points per slice
		for i in range(len(full_scar_contour)):
			scar_layer = full_scar_contour[i]
			if scar_layer.size == 0: continue
			scar_xy_pts = scar_layer[:, :2]
			scar_range_pts = np.linspace(0, 1, scar_xy_pts.shape[0])
			interp_func = sp.interpolate.interp1d(scar_range_pts, scar_xy_pts, kind='cubic', axis=0)
			new_scar_numpts = np.linspace(0, 1, 80)
			new_xy_pts = interp_func(new_scar_numpts)
			new_xy_pts = np.column_stack((new_xy_pts, [scar_layer[0, 2]] * 80))
			full_scar_contour[i] = new_xy_pts
		
		# Interpolate additional scar slices
		interp_scar_contour = []
		for i in range(len(full_scar_contour) - 1):
			# Get scar slices to use for interpolation
			scar_layer = full_scar_contour[i]
			scar_adj_layer = full_scar_contour[i+1]
			# If the layers are edge layers, or non-scar, don't bother
			if scar_layer.size == 0 or scar_adj_layer.size == 0: continue
			# Use the two slices as the edge values for the interpolation function
			interp_arr = [0, 1]
			x_interp_func = sp.interpolate.interp1d(interp_arr, np.column_stack((scar_layer[:, 0], scar_adj_layer[:, 0])))
			y_interp_func = sp.interpolate.interp1d(interp_arr, np.column_stack((scar_layer[:, 1], scar_adj_layer[:, 1])))
			z_interp_func = sp.interpolate.interp1d(interp_arr, np.column_stack((scar_layer[:, 2], scar_adj_layer[:, 2])))
			# Interpolate a number of intermediate slices
			new_interp_arr = np.linspace(0, 1, 5)
			new_x_vals = x_interp_func(new_interp_arr)
			new_y_vals = y_interp_func(new_interp_arr)
			new_z_vals = z_interp_func(new_interp_arr)
			# Reconstruct each slice back together, removing duplicate slices
			for j in range(new_x_vals.shape[1]):
				cur_slice = np.column_stack((new_x_vals[:, j], new_y_vals[:, j], new_z_vals[:, j]))
				slice_stored = any((np.around(cur_slice, 5) == np.around(slice_arr, 5)).all() for slice_arr in interp_scar_contour)
				if not slice_stored: interp_scar_contour.append(np.column_stack((new_x_vals[:, j], new_y_vals[:, j], new_z_vals[:, j])))
		full_scar_contour = interp_scar_contour
		# Append the contour to the total aligned scar data (essentially tracks timepoints)
		if len(self.aligned_scar) == 0:
			self.aligned_scar = [None] * len(self.cine_endo)
		self.aligned_scar[timepoint] = full_scar_contour
		return(full_scar_contour)

	def alignDense(self, cine_timepoint=0):
		"""Method to align DENSE points and contours to the cine-based model coordinates.
		"""
		# Convert prolate variables to arrays
		dense_endo_prol = np.vstack(tuple(self.dense_endo_prol))
		dense_epi_prol = np.vstack(tuple(self.dense_epi_prol))
		dense_pts_prol = np.vstack(tuple(self.dense_pts_prol))
		cine_endo_prol = np.vstack(tuple(self.cine_endo_prol[0]))
		cine_epi_prol = np.vstack(tuple(self.cine_epi_prol[0]))
		
		dense_epi_prol_shifted, epi_prol_shift_diff = self.__shiftProlateVals(dense_epi_prol, cine_epi_prol)
		dense_endo_prol_shifted, _ = self.__shiftProlateVals(dense_endo_prol, shift_diff=epi_prol_shift_diff)
		dense_pts_prol_shifted, _ = self.__shiftProlateVals(dense_pts_prol, shift_diff=epi_prol_shift_diff)
		#print(np.vstack(tuple(self.dense_endo)))
		
		# Interpolate dense endo and epi contours
		dense_endo_interp_mu = sp.interpolate.griddata(cine_endo_prol[:, 1:3], cine_endo_prol[:, 0], dense_endo_prol_shifted[:, 1:3], method='linear')
		dense_endo_interp_mu_nearest = sp.interpolate.griddata(cine_endo_prol[:, 1:3], cine_endo_prol[:, 0], dense_endo_prol_shifted[:, 1:3], method='nearest')
		dense_endo_interp_mu[np.where(np.isnan(dense_endo_interp_mu))[0]] = dense_endo_interp_mu_nearest[np.where(np.isnan(dense_endo_interp_mu))[0]]
		dense_endo_mu_ratio = dense_endo_interp_mu / dense_endo_prol_shifted[:, 0]
		
		dense_epi_interp_mu = sp.interpolate.griddata(cine_epi_prol[:, 1:3], cine_epi_prol[:, 0], dense_epi_prol_shifted[:, 1:3], method='linear')
		dense_epi_interp_mu_nearest = sp.interpolate.griddata(cine_epi_prol[:, 1:3], cine_epi_prol[:, 0], dense_epi_prol_shifted[:, 1:3], method='nearest')
		dense_epi_interp_mu[np.where(np.isnan(dense_epi_interp_mu))[0]] = dense_epi_interp_mu_nearest[np.where(np.isnan(dense_epi_interp_mu))[0]]
		dense_epi_mu_ratio = dense_epi_interp_mu / dense_epi_prol_shifted[:, 0]
		
		dense_full_interp_stack = np.vstack((dense_endo_prol_shifted[:, :3], dense_epi_prol_shifted[:, :3]))
		dense_interp_mu_vals = dense_endo_mu_ratio.tolist()
		dense_interp_mu_vals.extend(dense_epi_mu_ratio)
		dense_pts_interp_linear = sp.interpolate.LinearNDInterpolator(dense_full_interp_stack, dense_interp_mu_vals)
		dense_pts_interp_nearest = sp.interpolate.NearestNDInterpolator(dense_full_interp_stack, dense_interp_mu_vals)
		dense_pts_interp = dense_pts_interp_linear(dense_pts_prol_shifted)
		dense_pts_nearest = dense_pts_interp_nearest(dense_pts_prol_shifted)
		dense_pts_interp[np.where(np.isnan(dense_pts_interp))] = dense_pts_nearest[np.where(np.isnan(dense_pts_interp))]
		dense_pts_prol_adjusted = dense_pts_prol_shifted[:, 0]*dense_pts_interp
		
		# Convert interpolated & shifted contours back to cartesian coordinates.
		dense_endo_cart = np.column_stack(tuple(mathhelper.prolate2cart(dense_endo_interp_mu, dense_endo_prol_shifted[:, 1], dense_endo_prol_shifted[:, 2], self.focus)))
		dense_epi_cart = np.column_stack(tuple(mathhelper.prolate2cart(dense_epi_interp_mu, dense_epi_prol_shifted[:, 1], dense_epi_prol_shifted[:, 2], self.focus)))
		dense_pts_cart = np.column_stack(tuple(mathhelper.prolate2cart(dense_pts_prol_adjusted, dense_pts_prol_shifted[:, 1], dense_pts_prol_shifted[:, 2], self.focus)))
		
		dense_endo_cart_old = np.column_stack(tuple(mathhelper.prolate2cart(dense_endo_prol_shifted[:, 0], dense_endo_prol_shifted[:, 1], dense_endo_prol_shifted[:, 2], self.focus)))
		dense_epi_cart_old = np.column_stack(tuple(mathhelper.prolate2cart(dense_epi_prol_shifted[:, 0], dense_epi_prol_shifted[:, 1], dense_epi_prol_shifted[:, 2], self.focus)))
		
		# Convert cartesian slices into polar slices.
		'''dense_endo_polar = np.transpose(mathhelper.cart2pol(dense_endo_cart[:, 1], dense_endo_cart[:, 2]))
		dense_epi_polar = np.transpose(mathhelper.cart2pol(dense_epi_cart[:, 1], dense_epi_cart[:, 2]))
		dense_pts_polar = np.transpose(mathhelper.cart2pol(dense_pts_cart[:, 1], dense_pts_cart[:, 2]))
		
		dense_endo_polar_old = np.transpose(mathhelper.cart2pol(dense_endo_cart_old[:, 1], dense_endo_cart_old[:, 2]))
		dense_epi_polar_old = np.transpose(mathhelper.cart2pol(dense_epi_cart_old[:, 1], dense_epi_cart_old[:, 2]))'''
		dense_slice_z = np.unique(np.round(dense_endo_cart_old[:, 0], 2))
		
		# Split polar dense contours into separate slices.
		dense_endo_polar_old_split = [None]*dense_slice_z.size
		dense_epi_polar_old_split = [None]*dense_slice_z.size
		dense_pts_polar_old_split = [None]*dense_slice_z.size
		dense_endo_polar_split = [None]*dense_slice_z.size
		dense_epi_polar_split = [None]*dense_slice_z.size
		dense_endo_cart_split = [None]*dense_slice_z.size
		dense_epi_cart_split = [None]*dense_slice_z.size
		dense_pts_cart_split = [None]*dense_slice_z.size
		
		for slice_ind, slice_val in enumerate(dense_slice_z):
			#dense_endo_polar_old_split[slice_ind] = dense_endo_polar_old[np.where(np.round(dense_endo_cart_old[:, 0], 2) == slice_val)[0], :]
			#dense_epi_polar_old_split[slice_ind] = dense_epi_polar_old[np.where(np.round(dense_endo_cart_old[:, 0], 2) == slice_val)[0], :]
			#dense_pts_polar_old_split[slice_ind] = dense_pts_polar[np.where(np.round(dense_pts_cart[:, 0], 2) == slice_val)[0], :]
			#dense_endo_polar_split[slice_ind] = dense_endo_polar[np.where(np.round(dense_endo_cart_old[:, 0], 2) == slice_val)[0], :]
			#dense_epi_polar_split[slice_ind] = dense_epi_polar[np.where(np.round(dense_endo_cart_old[:, 0], 2) == slice_val)[0], :]
			dense_endo_cart_split[slice_ind] = dense_endo_cart[np.where(np.round(dense_endo_cart_old[:, 0], 2) == slice_val)[0], :]
			dense_epi_cart_split[slice_ind] = dense_epi_cart[np.where(np.round(dense_endo_cart_old[:, 0], 2) == slice_val)[0], :]
			dense_pts_cart_split[slice_ind] = dense_pts_cart[np.where(np.round(dense_pts_cart[:, 0], 2) == slice_val)[0], :]
		
		#dense_pts_polar_split = [None]*dense_slice_z.size
		
		# Interpolate points along the angles.
		'''for slice_ind, slice_val in enumerate(dense_slice_z):
			# Calculate boundary ratios from epicardial / endocardial contour adjustments
			dense_endo_ratio = np.divide(dense_endo_polar_split[slice_ind][:, 1], dense_endo_polar_old_split[slice_ind][:, 1])
			dense_epi_ratio = np.divide(dense_epi_polar_split[slice_ind][:, 1], dense_epi_polar_old_split[slice_ind][:, 1])
			dense_stacked_ratios = np.vstack(((np.column_stack((dense_endo_polar_old_split[slice_ind][:, 0], dense_endo_polar_old_split[slice_ind][:, 1], dense_endo_ratio)), np.column_stack((dense_epi_polar_old_split[slice_ind][:, 0], dense_epi_polar_old_split[slice_ind][:, 1], dense_epi_ratio)))))
			# Interpolate ratios based on angle and radius, then apply to the pts in the middle
			dense_rho_griddata_interp = sp.interpolate.griddata(dense_stacked_ratios[:, 0:2], dense_stacked_ratios[:, 2], dense_pts_polar_old_split[slice_ind][:, 0:2], method='linear')
			# Interpolate after performing a 180-degree rotation to account for edge points outside of the convex hull.
			dense_shifted_angles = np.column_stack(([dense_stacked_ratios[i, 0] - np.pi if dense_stacked_ratios[i, 0] >= np.pi else dense_stacked_ratios[i, 0] + np.pi for i in range(dense_stacked_ratios.shape[0])], dense_stacked_ratios[:, 1]))
			dense_shifted_old_pts = np.column_stack(([dense_pts_polar_old_split[slice_ind][i, 0] - np.pi if dense_pts_polar_old_split[slice_ind][i, 0] >= np.pi else dense_pts_polar_old_split[slice_ind][i, 0] + np.pi for i in range(dense_pts_polar_old_split[slice_ind].shape[0])], dense_pts_polar_old_split[slice_ind][:, 1]))
			dense_rho_griddata_shifted = sp.interpolate.griddata(dense_shifted_angles, dense_stacked_ratios[:, 2], dense_shifted_old_pts, method='linear')
			dense_rho_griddata_nearest = sp.interpolate.griddata(dense_shifted_angles, dense_stacked_ratios[:, 2], dense_shifted_old_pts, method='nearest')
			dense_rho_griddata_shifted[np.isnan(dense_rho_griddata_shifted)] = dense_rho_griddata_nearest[np.isnan(dense_rho_griddata_shifted)]
			# Fill nan values from original interpolation with appropriate values from rotated interpolation.
			dense_rho_griddata_interp[np.isnan(dense_rho_griddata_interp)] = dense_rho_griddata_shifted[np.isnan(dense_rho_griddata_interp)]
			# Calculate new points based on interpolated conversion values.
			dense_pts_rho = np.multiply(dense_pts_polar_old_split[slice_ind][:, 1], dense_rho_griddata_interp)
			dense_pts_polar_split[slice_ind] = np.column_stack(([slice_val]*dense_pts_polar_old_split[slice_ind].shape[0], dense_pts_polar_old_split[slice_ind][:, 0], dense_pts_rho))
			# Convert back to cartesian coordinates
			dense_pts_cart_temp = np.column_stack(tuple(mathhelper.pol2cart(dense_pts_polar_split[slice_ind][:, 1], dense_pts_polar_split[slice_ind][:, 2])))
			dense_pts_cart_split[slice_ind] = np.column_stack(([slice_val]*dense_pts_polar_split[slice_ind].shape[0], dense_pts_cart_temp))'''
		# Store values as class member variables.
		self.dense_slice_shifted = dense_endo_cart_old[[np.where(np.round(dense_endo_cart_old[:, 0], 2) == slice_val)[0][0] for slice_val in dense_slice_z], 0]
		self.dense_aligned_endo = dense_endo_cart
		self.dense_aligned_epi = dense_epi_cart
		self.dense_aligned_pts = dense_pts_cart
		return(True)
	
	def alignConfocal(self, cine_timepoint=0):
		"""Method to align confocal microscopy results to the cine-based "ground truth" anatomy.
		"""
		# Create a unified array for all endo and epi contours from confocal contours.
		conf_endo_prol_list = []
		conf_endo_slice_list = []
		conf_epi_prol_list = []
		conf_epi_slice_list = []
		conf_pts_prol_list = []
		conf_pts_slice_list = []
		conf_pts_vals_list = []
		conf_pinpts_list = []
		conf_pinpts_slice_list = []
		for slice_num in range(len(self.conf_endo_prol)):
			for subslice_num in range(len(self.conf_endo_prol[slice_num])):
				if self.conf_endo_prol[slice_num][subslice_num] is not None:
					conf_endo_slice_list.extend([slice_num for ind in range(len(self.conf_endo_prol[slice_num][subslice_num]))])
					conf_endo_prol_list.append(self.conf_endo_prol[slice_num][subslice_num])
					conf_epi_slice_list.extend([slice_num for ind in range(len(self.conf_epi_prol[slice_num][subslice_num]))])
					conf_epi_prol_list.append(self.conf_epi_prol[slice_num][subslice_num])
					conf_pts_slice_list.extend([slice_num for ind in range(len(self.conf_pts_prol[slice_num][subslice_num]))])
					conf_pts_prol_list.append(self.conf_pts_prol[slice_num][subslice_num])
					conf_pts_vals_list.append(self.conf_pts_vals[slice_num][subslice_num])
					conf_pinpts_list.append(self.conf_pinpts_prol[slice_num][subslice_num])
					conf_pinpts_slice_list.extend([slice_num for ind in range(len(self.conf_pinpts_prol[slice_num][subslice_num]))])
		conf_pinpts_prol = np.vstack(tuple(conf_pinpts_list))
		conf_pinpts_slice = np.vstack(tuple(conf_pinpts_slice_list))
		conf_endo_prol = np.vstack(tuple(conf_endo_prol_list))
		conf_endo_slice = np.vstack(tuple(conf_endo_slice_list))
		conf_epi_prol = np.vstack(tuple(conf_epi_prol_list))
		conf_epi_slice = np.vstack(tuple(conf_epi_slice_list))
		conf_pts_prol = np.vstack(tuple(conf_pts_prol_list))
		conf_pts_slice = np.vstack(tuple(conf_pts_slice_list))
		conf_pts_vals = np.vstack(tuple(conf_pts_vals_list))
		cine_endo_prol = np.vstack(tuple(self.cine_endo_prol[0]))
		cine_epi_prol = np.vstack(tuple(self.cine_epi_prol[0]))
		# Shift contours to align with cine contours by center point.
		conf_epi_prol_shifted, epi_prol_shift_diff = self.__shiftProlateVals(conf_epi_prol, cine_epi_prol)
		conf_endo_prol_shifted, _ = self.__shiftProlateVals(conf_endo_prol, shift_diff=epi_prol_shift_diff)
		conf_pts_prol_shifted, _ = self.__shiftProlateVals(conf_pts_prol, shift_diff=epi_prol_shift_diff)
		conf_pinpts_prol_shifted, _ = self.__shiftProlateVals(conf_pinpts_prol, shift_diff=epi_prol_shift_diff)
		
		# Rotate shifted points based on slice-by-slice septal pts.
		#print(conf_pinpts_prol_shifted[:, 2])
		conf_endo_prol_shifted[:, 2] = mathhelper.rotateProlatePhi(conf_endo_prol_shifted[:, 2], conf_endo_slice, conf_pinpts_prol_shifted, self.cine_septal_pts[-1, -1])
		conf_epi_prol_shifted[:, 2] = mathhelper.rotateProlatePhi(conf_epi_prol_shifted[:, 2], conf_epi_slice, conf_pinpts_prol_shifted, self.cine_septal_pts[-1, -1])
		conf_pts_prol_shifted[:, 2] = mathhelper.rotateProlatePhi(conf_pts_prol_shifted[:, 2], conf_pts_slice, conf_pinpts_prol_shifted, self.cine_septal_pts[-1, -1])
		conf_pinpts_prol_shifted[:, 2] = mathhelper.rotateProlatePhi(conf_pinpts_prol_shifted[:, 2], conf_pinpts_slice, conf_pinpts_prol_shifted, self.cine_septal_pts[-1, -1])
		
		# Interpolate mu values for endo and epi contours
		conf_endo_interp_mu = sp.interpolate.griddata(cine_endo_prol[:, 1:3], cine_endo_prol[:, 0], conf_endo_prol_shifted[:, 1:3], method='linear')
		conf_endo_interp_mu_nearest = sp.interpolate.griddata(cine_endo_prol[:, 1:3], cine_endo_prol[:, 0], conf_endo_prol_shifted[:, 1:3], method='nearest')
		conf_endo_interp_mu[np.where(np.isnan(conf_endo_interp_mu))[0]] = conf_endo_interp_mu_nearest[np.where(np.isnan(conf_endo_interp_mu))[0]]
		conf_endo_mu_ratio = conf_endo_interp_mu / conf_endo_prol_shifted[:, 0]
		
		conf_epi_interp_mu = sp.interpolate.griddata(cine_epi_prol[:, 1:3], cine_epi_prol[:, 0], conf_epi_prol_shifted[:, 1:3], method='linear')
		conf_epi_interp_mu_nearest = sp.interpolate.griddata(cine_epi_prol[:, 1:3], cine_epi_prol[:, 0], conf_epi_prol_shifted[:, 1:3], method='nearest')
		conf_epi_interp_mu[np.where(np.isnan(conf_epi_interp_mu))[0]] = conf_epi_interp_mu_nearest[np.where(np.isnan(conf_epi_interp_mu))[0]]
		conf_epi_mu_ratio = conf_epi_interp_mu / conf_epi_prol_shifted[:, 0]
		
		conf_full_interp_stack = np.vstack((conf_epi_prol_shifted[:, :3], conf_endo_prol_shifted[:, :3]))
		conf_interp_mu_vals = conf_epi_mu_ratio.tolist()
		conf_interp_mu_vals.extend(conf_endo_mu_ratio)
		conf_pts_interp_linear = sp.interpolate.LinearNDInterpolator(conf_full_interp_stack, conf_interp_mu_vals)
		conf_pts_interp_nearest = sp.interpolate.NearestNDInterpolator(conf_full_interp_stack, conf_interp_mu_vals)
		conf_pts_interp = conf_pts_interp_linear(conf_pts_prol_shifted)
		conf_pts_nearest = conf_pts_interp_nearest(conf_pts_prol_shifted)
		conf_pts_interp[np.where(np.isnan(conf_pts_interp))] = conf_pts_nearest[np.where(np.isnan(conf_pts_interp))]
		conf_pts_prol_adjusted = conf_pts_prol_shifted[:, 0]*conf_pts_interp
		
		# Convert contours back to cartesian coordinates
		conf_endo_cart = np.column_stack(tuple(mathhelper.prolate2cart(conf_endo_interp_mu, conf_endo_prol_shifted[:, 1], conf_endo_prol_shifted[:, 2], self.focus)))
		conf_epi_cart = np.column_stack(tuple(mathhelper.prolate2cart(conf_epi_interp_mu, conf_epi_prol_shifted[:, 1], conf_epi_prol_shifted[:, 2], self.focus)))
		conf_pts_cart = np.column_stack(tuple(mathhelper.prolate2cart(conf_pts_prol_adjusted, conf_pts_prol_shifted[:, 1], conf_pts_prol_shifted[:, 2], self.focus)))
		conf_pinpts_cart = np.column_stack(tuple(mathhelper.prolate2cart(conf_pinpts_prol_shifted[:, 0], conf_pinpts_prol_shifted[:, 1], conf_pinpts_prol_shifted[:, 2], self.focus)))
		cine_pinpts_cart = self.cine_septal_pts
		conf_epi_polar = np.column_stack(tuple(mathhelper.cart2pol(conf_epi_cart[:, 1], conf_epi_cart[:, 2])))
		conf_pts_polar = np.column_stack(tuple(mathhelper.cart2pol(conf_pts_cart[:, 1], conf_pts_cart[:, 2])))
		self.conf_shifted_endo = np.column_stack(tuple(mathhelper.prolate2cart(conf_endo_prol_shifted[:, 0], conf_endo_prol_shifted[:, 1], conf_endo_prol_shifted[:, 2], self.focus)))
		self.conf_shifted_epi = np.column_stack(tuple(mathhelper.prolate2cart(conf_epi_prol_shifted[:, 0], conf_epi_prol_shifted[:, 1], conf_epi_prol_shifted[:, 2], self.focus)))
		self.conf_aligned_endo = conf_endo_cart
		self.conf_aligned_epi = conf_epi_cart
		self.conf_aligned_pts = conf_pts_cart
		self.conf_aligned_vals = conf_pts_vals
		self.conf_pinpts_shifted = conf_pinpts_cart
		return(True)
	
	def __shiftProlateVals(self, adj_prol_pts, ref_prol_pts=np.empty([]), shift_diff=np.empty([0])):
		"""Shift a set of prolate points to align with a central axis in cartesian coordinates.
		"""
		adj_cart_pts = np.column_stack(tuple(mathhelper.prolate2cart(adj_prol_pts[:, 0], adj_prol_pts[:, 1], adj_prol_pts[:, 2], self.focus)))
		if (shift_diff.size == 0):
			if ref_prol_pts.size:
				ref_cart_pts = np.column_stack(tuple(mathhelper.prolate2cart(ref_prol_pts[:, 0], ref_prol_pts[:, 1], ref_prol_pts[:, 2], self.focus)))
				shift_diff = np.mean(adj_cart_pts, axis=0) - np.mean(ref_cart_pts, axis=0)
				shift_diff[0] = 0
			else:
				return(False)
		adj_cart_pts = adj_cart_pts - shift_diff
		adj_prol_pts_post = np.column_stack(tuple(mathhelper.cart2prolate(adj_cart_pts[:, 0], adj_cart_pts[:, 1], adj_cart_pts[:, 2], self.focus)))
		return(adj_prol_pts_post, shift_diff)
		
	def __convertInterpScar(self, scar_pts_combined, timepoint=0, long_axis=False):
		"""Converts interpolated scar trace into a plottable list of arrays, based on displayhelper.plotScarTrace.
		"""
		# Initialize list to store arrays for scar tracing.
		interp_scar_trace_inner = [np.empty([0, 3])]*len(scar_pts_combined)
		interp_scar_trace_outer = [np.empty([0, 3])]*len(scar_pts_combined)
		
		if long_axis:
			scar_pts_adj = [np.empty([0, 3])]*len(scar_pts_combined)
			for scar_ind, scar_pts in enumerate(scar_pts_combined):
				cine_epi_vals = self.lge_la_epi_prol[scar_ind]
				cine_epi_interp = sp.interpolate.interp1d(cine_epi_vals[:, 1], cine_epi_vals[:, 0], fill_value='extrapolate')
				scar_pt_rads = cine_epi_interp(scar_pts[:, 1])
				scar_adj_rads = scar_pts[:, 3]*scar_pt_rads
				scar_pts_adj_cart = np.multiply(np.column_stack(tuple(mathhelper.prolate2cart(scar_pt_rads, scar_pts[:, 1], scar_pts[:, 2], focus=self.focus))), [1, -1, 1])
				scar_pts_adj_rot = stackhelper.rotateDataCoordinates(scar_pts_adj_cart, self.apex_base_pts[0, :], self.apex_base_pts[1, :], np.expand_dims(self.rv_insertion_pts[2, :], 0))[0]
				scar_pts_adj[scar_ind] = np.flip(scar_pts_adj_rot, axis=1)
			return(scar_pts_adj)
		
		for scar_ind, scar_pts in enumerate(scar_pts_combined):
			cine_epi_vals = self.cine_epi_rotate[timepoint][np.where(self.cine_epi_rotate[timepoint][:, 3] == self.scar_slices[scar_ind]+1), 1:3].squeeze()
			cine_endo_vals = self.cine_endo_rotate[timepoint][np.where(self.cine_endo_rotate[timepoint][:, 3] == self.scar_slices[scar_ind]+1), 1:3].squeeze()
			cine_epi_polar_slice = np.column_stack((mathhelper.cart2pol(cine_epi_vals[:, 1], cine_epi_vals[:, 0])))
			cine_endo_polar_slice = np.column_stack((mathhelper.cart2pol(cine_endo_vals[:, 1], cine_endo_vals[:, 0])))
			cine_epi_polar_rho_interp = sp.interpolate.interp1d(cine_epi_polar_slice[:, 0], cine_epi_polar_slice[:, 1], fill_value='extrapolate')
			cine_endo_polar_rho_interp = sp.interpolate.interp1d(cine_endo_polar_slice[:, 0], cine_endo_polar_slice[:, 1], fill_value='extrapolate')
			cine_epi_inner_rho_vals = cine_epi_polar_rho_interp(scar_pts[:, 2])
			cine_epi_outer_rho_vals = cine_epi_polar_rho_interp(scar_pts[:, 3])
			cine_endo_inner_rho_vals = cine_endo_polar_rho_interp(scar_pts[:, 2])
			cine_endo_outer_rho_vals = cine_endo_polar_rho_interp(scar_pts[:, 3])
			scar_pts_rho = np.multiply(scar_pts[:, :2], np.column_stack((cine_epi_inner_rho_vals - cine_endo_inner_rho_vals, cine_epi_outer_rho_vals - cine_endo_outer_rho_vals))) + np.column_stack((cine_endo_inner_rho_vals, cine_endo_outer_rho_vals))
			scar_outer_pt_cine = np.column_stack((mathhelper.pol2cart(scar_pts[:, 3], scar_pts_rho[:, 1])))
			scar_inner_pt_cine = np.column_stack((mathhelper.pol2cart(scar_pts[:, 2], scar_pts_rho[:, 0])))
			interp_scar_trace_inner[scar_ind] = np.vstack((interp_scar_trace_inner[scar_ind], np.column_stack(tuple([scar_inner_pt_cine, scar_pts[:, 4]]))))
			interp_scar_trace_outer[scar_ind] = np.vstack((interp_scar_trace_outer[scar_ind], np.column_stack(tuple([scar_outer_pt_cine, scar_pts[:, 4]]))))
		
		interp_scar_trace = [None]*len(scar_pts_combined)
		for scar_ind in range(len(interp_scar_trace_inner)):
			inner_nonnans = np.where(~np.isnan(interp_scar_trace_inner[scar_ind][:, 0]))[0]
			outer_nonnans = np.where(~np.isnan(interp_scar_trace_outer[scar_ind][:, 0]))[0]
			inner_pts = interp_scar_trace_inner[scar_ind][inner_nonnans, :]
			outer_pts = interp_scar_trace_outer[scar_ind][outer_nonnans, :]
			wrapped_pts = np.vstack(tuple([inner_pts, np.flip(outer_pts, axis=0)]))
			interp_scar_trace[scar_ind] = wrapped_pts
		#print(scar_pts)
			# Stack outer and inner scar arrays vertically
			#ordered_scar_pts = np.vstack((scar_pts[:, 0:3], np.flip(scar_pts[:, 3:], axis=0)))
			# Remove nan values from the array and append the first value to the end to close the loop
			#nan_inds = np.where(np.isnan(ordered_scar_pts[:, 0]))
			#non_nan_scar = np.delete(ordered_scar_pts, nan_inds, axis=0)
			#interp_scar_trace[scar_ind] = np.vstack((non_nan_scar, non_nan_scar[0, :]))
		return(interp_scar_trace)